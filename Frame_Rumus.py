# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import ConfigParser


###########################################################################
## Class Frame_RumSet
###########################################################################

class Frame_RumSet(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition,
                          size=wx.Size(492, 398), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHTTEXT))

        fgSizer1 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer1.SetFlexibleDirection(wx.BOTH)
        fgSizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_panel22424 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel22424.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_INACTIVEBORDER))

        gSizer1 = wx.GridSizer(1, 1, 0, 0)

        self.m_staticText22 = wx.StaticText(self.m_panel22424, wx.ID_ANY, u"SETTING RUMUS CALCULATE ENCODER",
                                            wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText22.Wrap(-1)
        gSizer1.Add(self.m_staticText22, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.m_panel22424.SetSizer(gSizer1)
        self.m_panel22424.Layout()
        gSizer1.Fit(self.m_panel22424)
        fgSizer1.Add(self.m_panel22424, 1, wx.EXPAND | wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL,
                     5)

        self.ss22232 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.ss22232.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_INACTIVECAPTION))

        fgSizer2 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer2.SetFlexibleDirection(wx.BOTH)
        fgSizer2.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_listbook1 = wx.Listbook(self.ss22232, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LB_DEFAULT)
        self.Pnl_RumSet_X = wx.Panel(self.m_listbook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Pnl_RumSet_X.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_INACTIVEBORDER))

        fgSizer3 = wx.FlexGridSizer(4, 1, 0, 0)
        fgSizer3.SetFlexibleDirection(wx.BOTH)
        fgSizer3.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText24 = wx.StaticText(self.Pnl_RumSet_X, wx.ID_ANY, u"MOTOR X", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText24.Wrap(-1)
        fgSizer3.Add(self.m_staticText24, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer15 = wx.FlexGridSizer(0, 2, 0, 0)
        fgSizer15.SetFlexibleDirection(wx.BOTH)
        fgSizer15.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Chk_RumSet_ISUSEDX = wx.CheckBox(self.Pnl_RumSet_X, wx.ID_ANY, u"USE NEW SETTING", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        fgSizer15.Add(self.Chk_RumSet_ISUSEDX, 0, wx.ALL, 5)

        self.Chk_RumSet_OLDUSEX = wx.CheckBox(self.Pnl_RumSet_X, wx.ID_ANY, u"USE OLD SETTINGS", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        fgSizer15.Add(self.Chk_RumSet_OLDUSEX, 0, wx.ALL, 5)

        fgSizer3.Add(fgSizer15, 1, wx.EXPAND, 5)

        self.Pnl_RumSet_X_SUB = wx.Panel(self.Pnl_RumSet_X, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                         wx.TAB_TRAVERSAL)
        # self.Pnl_RumSet_X_SUB.Hide()

        fgSizer8 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer8.SetFlexibleDirection(wx.BOTH)
        fgSizer8.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer8.SetMinSize(wx.Size(300, 200))
        gbSizer1 = wx.GridBagSizer(0, 0)
        gbSizer1.SetFlexibleDirection(wx.BOTH)
        gbSizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        gbSizer1.SetMinSize(wx.Size(350, -1))
        self.Lbl_Constant_Label = wx.StaticText(self.Pnl_RumSet_X_SUB, wx.ID_ANY, u"Constant", wx.DefaultPosition,
                                                wx.DefaultSize, 0)
        self.Lbl_Constant_Label.Wrap(-1)
        gbSizer1.Add(self.Lbl_Constant_Label, wx.GBPosition(0, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                     5)

        self.Txt_RumSet_ConstantX = wx.TextCtrl(self.Pnl_RumSet_X_SUB, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                wx.DefaultSize, 0)
        gbSizer1.Add(self.Txt_RumSet_ConstantX, wx.GBPosition(0, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                     5)

        self.Lbl_Plus_Label = wx.StaticText(self.Pnl_RumSet_X_SUB, wx.ID_ANY, u"Plus", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.Lbl_Plus_Label.Wrap(-1)
        gbSizer1.Add(self.Lbl_Plus_Label, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_RumSet_PlusX = wx.TextCtrl(self.Pnl_RumSet_X_SUB, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        gbSizer1.Add(self.Txt_RumSet_PlusX, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer8.Add(gbSizer1, 1, wx.EXPAND, 5)

        self.nobodycaresX = wx.StaticText(self.Pnl_RumSet_X_SUB, wx.ID_ANY, u"*Rumus View", wx.DefaultPosition,
                                          wx.DefaultSize, 0)
        self.nobodycaresX.Wrap(-1)
        fgSizer8.Add(self.nobodycaresX, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Txt_RumSet_ViewX = wx.StaticText(self.Pnl_RumSet_X_SUB, wx.ID_ANY, u"-", wx.DefaultPosition,
                                              wx.Size(320, -1), wx.ALIGN_CENTRE)
        self.Txt_RumSet_ViewX.Wrap(-1)
        fgSizer8.Add(self.Txt_RumSet_ViewX, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Pnl_RumSet_X_SUB.SetSizer(fgSizer8)
        self.Pnl_RumSet_X_SUB.Layout()
        fgSizer8.Fit(self.Pnl_RumSet_X_SUB)
        fgSizer3.Add(self.Pnl_RumSet_X_SUB, 1, wx.EXPAND | wx.ALL, 5)

        self.Pnl_RumSet_X_SUB1 = wx.Panel(self.Pnl_RumSet_X, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer84 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer84.SetFlexibleDirection(wx.BOTH)
        fgSizer84.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer84.SetMinSize(wx.Size(300, 200))
        gbSizer14 = wx.GridBagSizer(0, 0)
        gbSizer14.SetFlexibleDirection(wx.BOTH)
        gbSizer14.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        gbSizer14.SetMinSize(wx.Size(350, -1))
        self.Lbl_Constant_Label4 = wx.StaticText(self.Pnl_RumSet_X_SUB1, wx.ID_ANY, u"Offset", wx.DefaultPosition,
                                                 wx.DefaultSize, 0)
        self.Lbl_Constant_Label4.Wrap(-1)
        gbSizer14.Add(self.Lbl_Constant_Label4, wx.GBPosition(0, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                      5)

        self.Txt_RumSet_OffsetX = wx.TextCtrl(self.Pnl_RumSet_X_SUB1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        gbSizer14.Add(self.Txt_RumSet_OffsetX, wx.GBPosition(0, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                      5)

        self.Lbl_Plus_Label4 = wx.StaticText(self.Pnl_RumSet_X_SUB1, wx.ID_ANY, u"Ratio", wx.DefaultPosition, wx.DefaultSize,
                                             0)
        self.Lbl_Plus_Label4.Wrap(-1)
        gbSizer14.Add(self.Lbl_Plus_Label4, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_RumSet_RatioX = wx.TextCtrl(self.Pnl_RumSet_X_SUB1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        gbSizer14.Add(self.Txt_RumSet_RatioX, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                      5)

        fgSizer84.Add(gbSizer14, 1, wx.EXPAND, 5)

        self.nobodycaresX4 = wx.StaticText(self.Pnl_RumSet_X_SUB1, wx.ID_ANY, u"*Rumus View", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.nobodycaresX4.Wrap(-1)
        fgSizer84.Add(self.nobodycaresX4, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Txt_RumSet_ViewX1 = wx.StaticText(self.Pnl_RumSet_X_SUB1, wx.ID_ANY, u"-", wx.DefaultPosition, wx.Size( 320,-1 ), wx.ALIGN_CENTRE)
        self.Txt_RumSet_ViewX1.Wrap(-1)
        fgSizer84.Add(self.Txt_RumSet_ViewX1, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Pnl_RumSet_X_SUB1.SetSizer(fgSizer84)
        self.Pnl_RumSet_X_SUB1.Layout()
        fgSizer84.Fit(self.Pnl_RumSet_X_SUB1)
        fgSizer3.Add(self.Pnl_RumSet_X_SUB1, 1, wx.EXPAND | wx.ALL, 5)

        self.Pnl_RumSet_X.SetSizer(fgSizer3)
        self.Pnl_RumSet_X.Layout()
        fgSizer3.Fit(self.Pnl_RumSet_X)
        self.m_listbook1.AddPage(self.Pnl_RumSet_X, u"X", False)
        self.Pnl_RumSet_Y = wx.Panel(self.m_listbook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Pnl_RumSet_Y.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_INACTIVEBORDER))

        fgSizer31 = wx.FlexGridSizer(4, 1, 0, 0)
        fgSizer31.SetFlexibleDirection(wx.BOTH)
        fgSizer31.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText241 = wx.StaticText(self.Pnl_RumSet_Y, wx.ID_ANY, u"MOTOR Y", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText241.Wrap(-1)
        fgSizer31.Add(self.m_staticText241, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer14 = wx.FlexGridSizer(0, 2, 0, 0)
        fgSizer14.SetFlexibleDirection(wx.BOTH)
        fgSizer14.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Chk_RumSet_ISUSEDY = wx.CheckBox(self.Pnl_RumSet_Y, wx.ID_ANY, u"USE NEW SETTING", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        fgSizer14.Add(self.Chk_RumSet_ISUSEDY, 0, wx.ALL, 5)

        self.Chk_RumSet_OLDUSEY = wx.CheckBox(self.Pnl_RumSet_Y, wx.ID_ANY, u"USE OLD SETTINGS", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        fgSizer14.Add(self.Chk_RumSet_OLDUSEY, 0, wx.ALL, 5)

        fgSizer31.Add(fgSizer14, 1, wx.EXPAND, 5)

        self.Pnl_RumSet_Y_SUB = wx.Panel(self.Pnl_RumSet_Y, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                         wx.TAB_TRAVERSAL)
        # self.Pnl_RumSet_Y_SUB.Hide()

        fgSizer81 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer81.SetFlexibleDirection(wx.BOTH)
        fgSizer81.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer81.SetMinSize(wx.Size(300, 200))
        gbSizer11 = wx.GridBagSizer(0, 0)
        gbSizer11.SetFlexibleDirection(wx.BOTH)
        gbSizer11.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        gbSizer11.SetMinSize(wx.Size(350, -1))
        self.Lbl_Constant_Label1 = wx.StaticText(self.Pnl_RumSet_Y_SUB, wx.ID_ANY, u"Constant", wx.DefaultPosition,
                                                 wx.DefaultSize, 0)
        self.Lbl_Constant_Label1.Wrap(-1)
        gbSizer11.Add(self.Lbl_Constant_Label1, wx.GBPosition(0, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                      5)

        self.Txt_RumSet_ConstantY = wx.TextCtrl(self.Pnl_RumSet_Y_SUB, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                wx.DefaultSize, 0)
        gbSizer11.Add(self.Txt_RumSet_ConstantY, wx.GBPosition(0, 1), wx.GBSpan(1, 1),
                      wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Lbl_Plus_Label1 = wx.StaticText(self.Pnl_RumSet_Y_SUB, wx.ID_ANY, u"Plus", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.Lbl_Plus_Label1.Wrap(-1)
        gbSizer11.Add(self.Lbl_Plus_Label1, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_RumSet_PlusY = wx.TextCtrl(self.Pnl_RumSet_Y_SUB, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        gbSizer11.Add(self.Txt_RumSet_PlusY, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer81.Add(gbSizer11, 1, wx.EXPAND, 5)

        self.nobodycaresX1 = wx.StaticText(self.Pnl_RumSet_Y_SUB, wx.ID_ANY, u"*Rumus View", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.nobodycaresX1.Wrap(-1)
        fgSizer81.Add(self.nobodycaresX1, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Txt_RumSet_ViewY = wx.StaticText(self.Pnl_RumSet_Y_SUB, wx.ID_ANY, u"-", wx.DefaultPosition,
                                              wx.Size(320, -1), wx.ALIGN_CENTRE)
        self.Txt_RumSet_ViewY.Wrap(-1)
        fgSizer81.Add(self.Txt_RumSet_ViewY, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Pnl_RumSet_Y_SUB.SetSizer(fgSizer81)
        self.Pnl_RumSet_Y_SUB.Layout()
        fgSizer81.Fit(self.Pnl_RumSet_Y_SUB)
        fgSizer31.Add(self.Pnl_RumSet_Y_SUB, 1, wx.EXPAND | wx.ALL, 5)

        self.Pnl_RumSet_Y_SUB1 = wx.Panel(self.Pnl_RumSet_Y, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                          wx.TAB_TRAVERSAL)
        fgSizer811 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer811.SetFlexibleDirection(wx.BOTH)
        fgSizer811.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer811.SetMinSize(wx.Size(300, 200))
        gbSizer111 = wx.GridBagSizer(0, 0)
        gbSizer111.SetFlexibleDirection(wx.BOTH)
        gbSizer111.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        gbSizer111.SetMinSize(wx.Size(350, -1))
        self.Lbl_Constant_Label11 = wx.StaticText(self.Pnl_RumSet_Y_SUB1, wx.ID_ANY, u"Offset", wx.DefaultPosition,
                                                  wx.DefaultSize, 0)
        self.Lbl_Constant_Label11.Wrap(-1)
        gbSizer111.Add(self.Lbl_Constant_Label11, wx.GBPosition(0, 0), wx.GBSpan(1, 1),
                       wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_RumSet_OffsetY = wx.TextCtrl(self.Pnl_RumSet_Y_SUB1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        gbSizer111.Add(self.Txt_RumSet_OffsetY, wx.GBPosition(0, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                       5)

        self.Lbl_Plus_Label11 = wx.StaticText(self.Pnl_RumSet_Y_SUB1, wx.ID_ANY, u"Ratio", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.Lbl_Plus_Label11.Wrap(-1)
        gbSizer111.Add(self.Lbl_Plus_Label11, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                       5)

        self.Txt_RumSet_RatioY = wx.TextCtrl(self.Pnl_RumSet_Y_SUB1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        gbSizer111.Add(self.Txt_RumSet_RatioY, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                       5)

        fgSizer811.Add(gbSizer111, 1, wx.EXPAND, 5)

        self.nobodycaresX11 = wx.StaticText(self.Pnl_RumSet_Y_SUB1, wx.ID_ANY, u"*Rumus View", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.nobodycaresX11.Wrap(-1)
        fgSizer811.Add(self.nobodycaresX11, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Txt_RumSet_ViewY1 = wx.StaticText(self.Pnl_RumSet_Y_SUB1, wx.ID_ANY, u"-", wx.DefaultPosition,
                                               wx.Size(320, -1), wx.ALIGN_CENTRE)
        self.Txt_RumSet_ViewY1.Wrap(-1)
        fgSizer811.Add(self.Txt_RumSet_ViewY1, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Pnl_RumSet_Y_SUB1.SetSizer(fgSizer811)
        self.Pnl_RumSet_Y_SUB1.Layout()
        fgSizer811.Fit(self.Pnl_RumSet_Y_SUB1)
        fgSizer31.Add(self.Pnl_RumSet_Y_SUB1, 1, wx.EXPAND | wx.ALL, 5)

        self.Pnl_RumSet_Y.SetSizer(fgSizer31)
        self.Pnl_RumSet_Y.Layout()
        fgSizer31.Fit(self.Pnl_RumSet_Y)
        self.m_listbook1.AddPage(self.Pnl_RumSet_Y, u"Y", False)
        self.Pnl_RumSet_Z = wx.Panel(self.m_listbook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Pnl_RumSet_Z.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_INACTIVEBORDER))

        fgSizer32 = wx.FlexGridSizer(4, 1, 0, 0)
        fgSizer32.SetFlexibleDirection(wx.BOTH)
        fgSizer32.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText242 = wx.StaticText(self.Pnl_RumSet_Z, wx.ID_ANY, u"MOTOR Z", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.m_staticText242.Wrap(-1)
        fgSizer32.Add(self.m_staticText242, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer13 = wx.FlexGridSizer(0, 2, 0, 0)
        fgSizer13.SetFlexibleDirection(wx.BOTH)
        fgSizer13.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Chk_RumSet_ISUSEDZ = wx.CheckBox(self.Pnl_RumSet_Z, wx.ID_ANY, u"USE NEW SETTING", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        fgSizer13.Add(self.Chk_RumSet_ISUSEDZ, 0, wx.ALL, 5)

        self.Chk_RumSet_OLDUSEZ = wx.CheckBox(self.Pnl_RumSet_Z, wx.ID_ANY, u"USE OLD SETTINGS", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        fgSizer13.Add(self.Chk_RumSet_OLDUSEZ, 0, wx.ALL, 5)

        fgSizer32.Add(fgSizer13, 1, wx.EXPAND, 5)

        self.Pnl_RumSet_Z_SUB = wx.Panel(self.Pnl_RumSet_Z, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                         wx.TAB_TRAVERSAL)
        # self.Pnl_RumSet_Z_SUB.Hide()

        fgSizer82 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer82.SetFlexibleDirection(wx.BOTH)
        fgSizer82.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer82.SetMinSize(wx.Size(300, 200))
        gbSizer12 = wx.GridBagSizer(0, 0)
        gbSizer12.SetFlexibleDirection(wx.BOTH)
        gbSizer12.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        gbSizer12.SetMinSize(wx.Size(350, -1))
        self.Lbl_Constant_Label2 = wx.StaticText(self.Pnl_RumSet_Z_SUB, wx.ID_ANY, u"Constant", wx.DefaultPosition,
                                                 wx.DefaultSize, 0)
        self.Lbl_Constant_Label2.Wrap(-1)
        gbSizer12.Add(self.Lbl_Constant_Label2, wx.GBPosition(0, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                      5)

        self.Txt_RumSet_ConstantZ = wx.TextCtrl(self.Pnl_RumSet_Z_SUB, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                wx.DefaultSize, 0)
        gbSizer12.Add(self.Txt_RumSet_ConstantZ, wx.GBPosition(0, 1), wx.GBSpan(1, 1),
                      wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Lbl_Plus_Label2 = wx.StaticText(self.Pnl_RumSet_Z_SUB, wx.ID_ANY, u"Plus", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.Lbl_Plus_Label2.Wrap(-1)
        gbSizer12.Add(self.Lbl_Plus_Label2, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_RumSet_PlusZ = wx.TextCtrl(self.Pnl_RumSet_Z_SUB, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        gbSizer12.Add(self.Txt_RumSet_PlusZ, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer82.Add(gbSizer12, 1, wx.EXPAND, 5)

        self.nobodycaresX2 = wx.StaticText(self.Pnl_RumSet_Z_SUB, wx.ID_ANY, u"*Rumus View", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.nobodycaresX2.Wrap(-1)
        fgSizer82.Add(self.nobodycaresX2, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Txt_RumSet_ViewZ = wx.StaticText(self.Pnl_RumSet_Z_SUB, wx.ID_ANY, u"-", wx.DefaultPosition,
                                              wx.Size(320, -1), wx.ALIGN_CENTRE)
        self.Txt_RumSet_ViewZ.Wrap(-1)
        fgSizer82.Add(self.Txt_RumSet_ViewZ, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Pnl_RumSet_Z_SUB.SetSizer(fgSizer82)
        self.Pnl_RumSet_Z_SUB.Layout()
        fgSizer82.Fit(self.Pnl_RumSet_Z_SUB)
        fgSizer32.Add(self.Pnl_RumSet_Z_SUB, 1, wx.EXPAND | wx.ALL, 5)

        self.Pnl_RumSet_Z_SUB1 = wx.Panel(self.Pnl_RumSet_Z, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                          wx.TAB_TRAVERSAL)
        fgSizer821 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer821.SetFlexibleDirection(wx.BOTH)
        fgSizer821.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer821.SetMinSize(wx.Size(300, 200))
        gbSizer121 = wx.GridBagSizer(0, 0)
        gbSizer121.SetFlexibleDirection(wx.BOTH)
        gbSizer121.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        gbSizer121.SetMinSize(wx.Size(350, -1))
        self.Lbl_Constant_Label21 = wx.StaticText(self.Pnl_RumSet_Z_SUB1, wx.ID_ANY, u"Offset", wx.DefaultPosition,
                                                  wx.DefaultSize, 0)
        self.Lbl_Constant_Label21.Wrap(-1)
        gbSizer121.Add(self.Lbl_Constant_Label21, wx.GBPosition(0, 0), wx.GBSpan(1, 1),
                       wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_RumSet_OffsetZ = wx.TextCtrl(self.Pnl_RumSet_Z_SUB1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        gbSizer121.Add(self.Txt_RumSet_OffsetZ, wx.GBPosition(0, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                       5)

        self.Lbl_Plus_Label21 = wx.StaticText(self.Pnl_RumSet_Z_SUB1, wx.ID_ANY, u"Ratio", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.Lbl_Plus_Label21.Wrap(-1)
        gbSizer121.Add(self.Lbl_Plus_Label21, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                       5)

        self.Txt_RumSet_RatioZ = wx.TextCtrl(self.Pnl_RumSet_Z_SUB1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        gbSizer121.Add(self.Txt_RumSet_RatioZ, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                       5)

        fgSizer821.Add(gbSizer121, 1, wx.EXPAND, 5)

        self.nobodycaresX21 = wx.StaticText(self.Pnl_RumSet_Z_SUB1, wx.ID_ANY, u"*Rumus View", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.nobodycaresX21.Wrap(-1)
        fgSizer821.Add(self.nobodycaresX21, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Txt_RumSet_ViewZ1 = wx.StaticText(self.Pnl_RumSet_Z_SUB1, wx.ID_ANY, u"-", wx.DefaultPosition,
                                               wx.Size(320, -1), wx.ALIGN_CENTRE)
        self.Txt_RumSet_ViewZ1.Wrap(-1)
        fgSizer821.Add(self.Txt_RumSet_ViewZ1, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Pnl_RumSet_Z_SUB1.SetSizer(fgSizer821)
        self.Pnl_RumSet_Z_SUB1.Layout()
        fgSizer821.Fit(self.Pnl_RumSet_Z_SUB1)
        fgSizer32.Add(self.Pnl_RumSet_Z_SUB1, 1, wx.EXPAND | wx.ALL, 5)

        self.Pnl_RumSet_Z.SetSizer(fgSizer32)
        self.Pnl_RumSet_Z.Layout()
        fgSizer32.Fit(self.Pnl_RumSet_Z)
        self.m_listbook1.AddPage(self.Pnl_RumSet_Z, u"Z", True)
        self.Pnl_RumSet_C = wx.Panel(self.m_listbook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.Pnl_RumSet_C.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_INACTIVEBORDER))

        fgSizer33 = wx.FlexGridSizer(4, 1, 0, 0)
        fgSizer33.SetFlexibleDirection(wx.BOTH)
        fgSizer33.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText2421 = wx.StaticText(self.Pnl_RumSet_C, wx.ID_ANY, u"MOTOR C", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.m_staticText2421.Wrap(-1)
        fgSizer33.Add(self.m_staticText2421, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        fgSizer121 = wx.FlexGridSizer(0, 2, 0, 0)
        fgSizer121.SetFlexibleDirection(wx.BOTH)
        fgSizer121.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Chk_RumSet_ISUSEDC = wx.CheckBox(self.Pnl_RumSet_C, wx.ID_ANY, u"USE NEW SETTING", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        fgSizer121.Add(self.Chk_RumSet_ISUSEDC, 0, wx.ALL, 5)

        self.Chk_RumSet_OLDUSEC = wx.CheckBox(self.Pnl_RumSet_C, wx.ID_ANY, u"USE OLD SETTINGS", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        fgSizer121.Add(self.Chk_RumSet_OLDUSEC, 0, wx.ALL, 5)

        fgSizer33.Add(fgSizer121, 1, wx.EXPAND, 5)

        self.Pnl_RumSet_C_SUB = wx.Panel(self.Pnl_RumSet_C, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                         wx.TAB_TRAVERSAL)
        # self.Pnl_RumSet_C_SUB.Hide()

        fgSizer83 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer83.SetFlexibleDirection(wx.BOTH)
        fgSizer83.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer83.SetMinSize(wx.Size(300, 200))
        gbSizer13 = wx.GridBagSizer(0, 0)
        gbSizer13.SetFlexibleDirection(wx.BOTH)
        gbSizer13.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        gbSizer13.SetMinSize(wx.Size(350, -1))
        self.Lbl_Constant_Label3 = wx.StaticText(self.Pnl_RumSet_C_SUB, wx.ID_ANY, u"Constant", wx.DefaultPosition,
                                                 wx.DefaultSize, 0)
        self.Lbl_Constant_Label3.Wrap(-1)
        gbSizer13.Add(self.Lbl_Constant_Label3, wx.GBPosition(0, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                      5)

        self.Txt_RumSet_ConstantC = wx.TextCtrl(self.Pnl_RumSet_C_SUB, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                wx.DefaultSize, 0)
        gbSizer13.Add(self.Txt_RumSet_ConstantC, wx.GBPosition(0, 1), wx.GBSpan(1, 1),
                      wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Lbl_Plus_Label3 = wx.StaticText(self.Pnl_RumSet_C_SUB, wx.ID_ANY, u"Plus", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.Lbl_Plus_Label3.Wrap(-1)
        gbSizer13.Add(self.Lbl_Plus_Label3, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_RumSet_PlusC = wx.TextCtrl(self.Pnl_RumSet_C_SUB, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        gbSizer13.Add(self.Txt_RumSet_PlusC, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer83.Add(gbSizer13, 1, wx.EXPAND, 5)

        self.nobodycaresX3 = wx.StaticText(self.Pnl_RumSet_C_SUB, wx.ID_ANY, u"*Rumus View", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.nobodycaresX3.Wrap(-1)
        fgSizer83.Add(self.nobodycaresX3, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Txt_RumSet_ViewC = wx.StaticText(self.Pnl_RumSet_C_SUB, wx.ID_ANY, u"-", wx.DefaultPosition,
                                              wx.Size(320, -1), wx.ALIGN_CENTRE)
        self.Txt_RumSet_ViewC.Wrap(-1)
        fgSizer83.Add(self.Txt_RumSet_ViewC, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Pnl_RumSet_C_SUB.SetSizer(fgSizer83)
        self.Pnl_RumSet_C_SUB.Layout()
        fgSizer83.Fit(self.Pnl_RumSet_C_SUB)
        fgSizer33.Add(self.Pnl_RumSet_C_SUB, 1, wx.EXPAND | wx.ALL, 5)

        self.Pnl_RumSet_C_SUB1 = wx.Panel(self.Pnl_RumSet_C, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                          wx.TAB_TRAVERSAL)
        fgSizer831 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer831.SetFlexibleDirection(wx.BOTH)
        fgSizer831.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer831.SetMinSize(wx.Size(300, 200))
        gbSizer131 = wx.GridBagSizer(0, 0)
        gbSizer131.SetFlexibleDirection(wx.BOTH)
        gbSizer131.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        gbSizer131.SetMinSize(wx.Size(350, -1))
        self.Lbl_Constant_Label31 = wx.StaticText(self.Pnl_RumSet_C_SUB1, wx.ID_ANY, u"Offset", wx.DefaultPosition,
                                                  wx.DefaultSize, 0)
        self.Lbl_Constant_Label31.Wrap(-1)
        gbSizer131.Add(self.Lbl_Constant_Label31, wx.GBPosition(0, 0), wx.GBSpan(1, 1),
                       wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_RumSet_OffsetC = wx.TextCtrl(self.Pnl_RumSet_C_SUB1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        gbSizer131.Add(self.Txt_RumSet_OffsetC, wx.GBPosition(0, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                       5)

        self.Lbl_Plus_Label31 = wx.StaticText(self.Pnl_RumSet_C_SUB1, wx.ID_ANY, u"Ratio", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.Lbl_Plus_Label31.Wrap(-1)
        gbSizer131.Add(self.Lbl_Plus_Label31, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                       5)

        self.Txt_RumSet_RatioC = wx.TextCtrl(self.Pnl_RumSet_C_SUB1, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        gbSizer131.Add(self.Txt_RumSet_RatioC, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                       5)

        fgSizer831.Add(gbSizer131, 1, wx.EXPAND, 5)

        self.nobodycaresX31 = wx.StaticText(self.Pnl_RumSet_C_SUB1, wx.ID_ANY, u"*Rumus View", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.nobodycaresX31.Wrap(-1)
        fgSizer831.Add(self.nobodycaresX31, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Txt_RumSet_ViewC1 = wx.StaticText(self.Pnl_RumSet_C_SUB1, wx.ID_ANY, u"-", wx.DefaultPosition,
                                               wx.Size(320, -1), wx.ALIGN_CENTRE)
        self.Txt_RumSet_ViewC1.Wrap(-1)
        fgSizer831.Add(self.Txt_RumSet_ViewC1, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Pnl_RumSet_C_SUB1.SetSizer(fgSizer831)
        self.Pnl_RumSet_C_SUB1.Layout()
        fgSizer831.Fit(self.Pnl_RumSet_C_SUB1)
        fgSizer33.Add(self.Pnl_RumSet_C_SUB1, 1, wx.EXPAND | wx.ALL, 5)

        self.Pnl_RumSet_C.SetSizer(fgSizer33)
        self.Pnl_RumSet_C.Layout()
        fgSizer33.Fit(self.Pnl_RumSet_C)
        self.m_listbook1.AddPage(self.Pnl_RumSet_C, u"C", False)

        fgSizer2.Add(self.m_listbook1, 1, wx.EXPAND | wx.ALL, 5)

        self.ss22232.SetSizer(fgSizer2)
        self.ss22232.Layout()
        fgSizer2.Fit(self.ss22232)
        fgSizer1.Add(self.ss22232, 1, wx.EXPAND | wx.ALL, 5)

        self.m_paldklnel7 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_paldklnel7.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_INACTIVEBORDER))

        gSizer2 = wx.GridSizer(1, 2, 0, 0)

        self.m_staticText23434 = wx.StaticText(self.m_paldklnel7, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                               wx.DefaultSize, 0)
        self.m_staticText23434.Wrap(-1)
        gSizer2.Add(self.m_staticText23434, 0, wx.ALL, 5)

        fgSizer12 = wx.FlexGridSizer(0, 2, 0, 0)
        fgSizer12.SetFlexibleDirection(wx.BOTH)
        fgSizer12.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Cmd_RumSet_Apply = wx.Button(self.m_paldklnel7, wx.ID_ANY, u"Apply", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer12.Add(self.Cmd_RumSet_Apply, 0, wx.ALL, 5)

        self.Cmd_RumSet_Exit = wx.Button(self.m_paldklnel7, wx.ID_ANY, u"Exit", wx.DefaultPosition, wx.DefaultSize,
                                         wx.NO_BORDER)
        fgSizer12.Add(self.Cmd_RumSet_Exit, 0, wx.ALL, 5)

        gSizer2.Add(fgSizer12, 1, wx.EXPAND | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL | wx.LEFT, 5)

        self.m_paldklnel7.SetSizer(gSizer2)
        self.m_paldklnel7.Layout()
        gSizer2.Fit(self.m_paldklnel7)
        fgSizer1.Add(self.m_paldklnel7, 1, wx.EXPAND | wx.ALL, 5)

        self.SetSizer(fgSizer1)
        self.Layout()

        self.StarterRum()

        self.Centre(wx.BOTH)
        self.Show()



        # Connect Events
        self.Chk_RumSet_ISUSEDX.Bind(wx.EVT_CHECKBOX, lambda x:self.ShowFrams(mode=1, val=self.Chk_RumSet_ISUSEDX.GetValue()))
        self.Txt_RumSet_ConstantX.Bind(wx.EVT_TEXT, lambda x:self.ChangeRumus(motor='X',types='constant'))
        self.Txt_RumSet_PlusX.Bind(wx.EVT_TEXT, lambda x:self.ChangeRumus(motor='X',types='plus'))
        self.Chk_RumSet_ISUSEDY.Bind(wx.EVT_CHECKBOX, lambda x:self.ShowFrams(mode=2, val=self.Chk_RumSet_ISUSEDY.GetValue()))
        self.Txt_RumSet_ConstantY.Bind(wx.EVT_TEXT, lambda x:self.ChangeRumus(motor='Y',types='constant'))
        self.Txt_RumSet_PlusY.Bind(wx.EVT_TEXT, lambda x:self.ChangeRumus(motor='Y',types='plus'))
        self.Chk_RumSet_ISUSEDZ.Bind(wx.EVT_CHECKBOX, lambda x:self.ShowFrams(mode=3, val=self.Chk_RumSet_ISUSEDZ.GetValue()))
        self.Txt_RumSet_ConstantZ.Bind(wx.EVT_TEXT, lambda x:self.ChangeRumus(motor='Z',types='constant'))
        self.Txt_RumSet_PlusZ.Bind(wx.EVT_TEXT, lambda x:self.ChangeRumus(motor='Z',types='plus'))
        self.Chk_RumSet_ISUSEDC.Bind(wx.EVT_CHECKBOX, lambda x:self.ShowFrams(mode=4, val=self.Chk_RumSet_ISUSEDC.GetValue()))
        self.Txt_RumSet_ConstantC.Bind(wx.EVT_TEXT, lambda x:self.ChangeRumus(motor='C',types='constant'))
        self.Txt_RumSet_PlusC.Bind(wx.EVT_TEXT, lambda x:self.ChangeRumus(motor='C',types='plus'))
        self.Cmd_RumSet_Apply.Bind(wx.EVT_BUTTON, lambda x:self.ApplySet())
        self.Cmd_RumSet_Exit.Bind(wx.EVT_BUTTON, lambda x:self.Exit())


        # OLD RUMUS
        self.Chk_RumSet_OLDUSEX.Bind(wx.EVT_CHECKBOX, lambda x:self.ShowFrams2(mode=1, val=self.Chk_RumSet_OLDUSEX.GetValue()))
        self.Txt_RumSet_OffsetX.Bind(wx.EVT_TEXT, lambda x:self.ChangeRumus2(motor='X',types='constant'))
        self.Txt_RumSet_RatioX.Bind(wx.EVT_TEXT, lambda x:self.ChangeRumus2(motor='X',types='plus'))
        self.Chk_RumSet_OLDUSEY.Bind(wx.EVT_CHECKBOX, lambda x:self.ShowFrams2(mode=2, val=self.Chk_RumSet_OLDUSEY.GetValue()))
        self.Txt_RumSet_OffsetY.Bind(wx.EVT_TEXT, lambda x:self.ChangeRumus2(motor='Y',types='constant'))
        self.Txt_RumSet_RatioY.Bind(wx.EVT_TEXT, lambda x:self.ChangeRumus2(motor='Y',types='plus'))
        self.Chk_RumSet_OLDUSEZ.Bind(wx.EVT_CHECKBOX, lambda x:self.ShowFrams2(mode=3, val=self.Chk_RumSet_OLDUSEZ.GetValue()))
        self.Txt_RumSet_OffsetZ.Bind(wx.EVT_TEXT, lambda x:self.ChangeRumus2(motor='Z',types='constant'))
        self.Txt_RumSet_RatioZ.Bind(wx.EVT_TEXT, lambda x:self.ChangeRumus2(motor='Z',types='plus'))
        self.Chk_RumSet_OLDUSEC.Bind(wx.EVT_CHECKBOX, lambda x:self.ShowFrams2(mode=4, val=self.Chk_RumSet_OLDUSEC.GetValue()))
        self.Txt_RumSet_OffsetC.Bind(wx.EVT_TEXT, lambda x:self.ChangeRumus2(motor='C',types='constant'))
        self.Txt_RumSet_RatioC.Bind(wx.EVT_TEXT, lambda x:self.ChangeRumus2(motor='C',types='plus'))

        self.StarterRum2()

    def __del__(self):
        pass

    def StarterRum(self):


        # NEW RUMUS

        config = ConfigParser.ConfigParser()
        config.readfp(open(r'config.txt'))

        x_using = float(config.get('CalculateEncoder Data', 'X_USING'))
        y_using = float(config.get('CalculateEncoder Data', 'Y_USING'))
        z_using = float(config.get('CalculateEncoder Data', 'Z_USING'))
        c_using = float(config.get('CalculateEncoder Data', 'C_USING'))

        x_const = float(config.get('CalculateEncoder Data', 'X_Constant'))
        y_const = float(config.get('CalculateEncoder Data', 'Y_Constant'))
        z_const = float(config.get('CalculateEncoder Data', 'Z_Constant'))
        c_const = float(config.get('CalculateEncoder Data', 'C_Constant'))

        x_plus = float(config.get('CalculateEncoder Data', 'X_Plus'))
        y_plus = float(config.get('CalculateEncoder Data', 'Y_Plus'))
        z_plus = float(config.get('CalculateEncoder Data', 'Z_Plus'))
        c_plus = float(config.get('CalculateEncoder Data', 'C_Plus'))

        # self._RumData['C']['use']
        x_dat = {'use': x_using,
                 'constant': x_const,
                 'plus': x_plus}
        y_dat = {'use': y_using,
                 'constant': y_const,
                 'plus': y_plus}
        z_dat = {'use': z_using,
                 'constant': z_const,
                 'plus': z_plus}
        c_dat = {'use': c_using,
                 'constant': c_const,
                 'plus': c_plus}

        self._RumData = {'X': x_dat,
                         'Y': y_dat,
                         'Z': z_dat,
                         'C': c_dat}

        if self._RumData['X']['use'] == False:
            self.Pnl_RumSet_X_SUB.Hide()
            self.Pnl_RumSet_X_SUB.SetMaxSize(wx.Size(1, 1))
        elif self._RumData['X']['use'] == True:
            self.Pnl_RumSet_X_SUB.Show()
            self.Pnl_RumSet_X_SUB.SetMaxSize(wx.Size(-1, -1))
            self.Chk_RumSet_ISUSEDX.SetValue(True)
            rumus = str('= (' + str(self._RumData['X']['constant']) + ' * x) + (' + str(self._RumData['X']['plus']) + ')')
            self.Txt_RumSet_ViewX.SetLabel(rumus)

        if self._RumData['Y']['use'] == False:
            self.Pnl_RumSet_Y_SUB.Hide()
            self.Pnl_RumSet_Y_SUB.SetMaxSize(wx.Size(1, 1))
        elif self._RumData['Y']['use'] == True:
            self.Pnl_RumSet_Y_SUB.Show()
            self.Pnl_RumSet_Y_SUB.SetMaxSize(wx.Size(-1, -1))
            self.Chk_RumSet_ISUSEDY.SetValue(True)
            rumus = str('= (' + str(self._RumData['Y']['constant']) + ' * x) + (' + str(self._RumData['Y']['plus']) + ')')
            self.Txt_RumSet_ViewY.SetLabel(rumus)

        if self._RumData['Z']['use'] == False:
            self.Pnl_RumSet_Z_SUB.Hide()
            self.Pnl_RumSet_Z_SUB1.Show()
            self.Pnl_RumSet_Z_SUB.SetMaxSize(wx.Size(1, 1))
        elif self._RumData['Z']['use'] == True:
            self.Pnl_RumSet_Z_SUB.Show()
            self.Pnl_RumSet_Z_SUB1.Hide()
            self.Pnl_RumSet_Z_SUB.SetMaxSize(wx.Size(-1, -1))
            self.Chk_RumSet_ISUSEDZ.SetValue(True)
            rumus = str('= (' + str(self._RumData['Z']['constant']) + ' * x) + (' + str(self._RumData['Z']['plus']) + ')')
            self.Txt_RumSet_ViewZ.SetLabel(rumus)

        if self._RumData['C']['use'] == False:
            self.Pnl_RumSet_C_SUB.Hide()
            self.Pnl_RumSet_C_SUB.SetMaxSize(wx.Size(1, 1))
        elif self._RumData['C']['use'] == True:
            self.Pnl_RumSet_C_SUB.Show()
            self.Pnl_RumSet_C_SUB.SetMaxSize(wx.Size(-1, -1))
            self.Chk_RumSet_ISUSEDC.SetValue(True)
            rumus = str('= (' + str(self._RumData['C']['constant']) + ' * x) + (' + str(self._RumData['C']['plus']) + ')')
            self.Txt_RumSet_ViewC.SetLabel(rumus)


        #SET TEXT
        self.Txt_RumSet_ConstantX.SetValue(str(self._RumData['X']['constant']))
        self.Txt_RumSet_ConstantY.SetValue(str(self._RumData['Y']['constant']))
        self.Txt_RumSet_ConstantZ.SetValue(str(self._RumData['Z']['constant']))
        self.Txt_RumSet_ConstantC.SetValue(str(self._RumData['C']['constant']))

        self.Txt_RumSet_PlusX.SetValue(str(self._RumData['X']['plus']))
        self.Txt_RumSet_PlusY.SetValue(str(self._RumData['Y']['plus']))
        self.Txt_RumSet_PlusZ.SetValue(str(self._RumData['Z']['plus']))
        self.Txt_RumSet_PlusC.SetValue(str(self._RumData['C']['plus']))

        # SET TEXT OLD
        self.Txt_RumSet_OffsetX.SetValue(str(self.Encoder_List[6]['offset']))
        self.Txt_RumSet_OffsetY.SetValue(str(self.Encoder_List[4]['offset']))
        self.Txt_RumSet_OffsetZ.SetValue(str(self.Encoder_List[1]['offset']))
        self.Txt_RumSet_OffsetC.SetValue(str(self.Encoder_List[5]['offset']))

        self.Txt_RumSet_RatioX.SetValue(str(self.Encoder_List[6]['ratio']))
        self.Txt_RumSet_RatioY.SetValue(str(self.Encoder_List[4]['ratio']))
        self.Txt_RumSet_RatioZ.SetValue(str(self.Encoder_List[1]['ratio']))
        self.Txt_RumSet_RatioC.SetValue(str(self.Encoder_List[5]['ratio']))


        #OLD Rumus
        x_using2 = float(config.get('Old Rumus', 'X_USING'))
        y_using2 = float(config.get('Old Rumus', 'Y_USING'))
        z_using2 = float(config.get('Old Rumus', 'Z_USING'))
        c_using2 = float(config.get('Old Rumus', 'C_USING'))
        self._OldRum = {'X':1,
                       'Y':1,
                       'Z':1,
                       'C':1}

        if self._OldRum['X'] == False:
            self.Pnl_RumSet_X_SUB1.Hide()
        elif self._OldRum['X'] == True:

            #PREVENT MULTI CHECK
            if self._RumData['X']['use'] == True:
                self.Pnl_RumSet_X_SUB.Hide()
                self._RumData['X']['use'] = False
                self.Chk_RumSet_ISUSEDX.SetValue(False)

            self.Pnl_RumSet_X_SUB1.Show()
            self.Chk_RumSet_OLDUSEX.SetValue(True)
            ratio = self.Encoder_List[6]['ratio']
            offset = self.Encoder_List[6]['offset']
            rumus2 = str('= (' + str('x') + ' + (' + str(offset) + ' * ' + str(-1) + '))' + ' / ' + str(ratio))
            self.Txt_RumSet_ViewX1.SetLabel(rumus2)

        if self._OldRum['Y'] == False:
            self.Pnl_RumSet_Y_SUB1.Hide()
        elif self._OldRum['Y'] == True:

            # PREVENT MULTI CHECK
            if self._RumData['Y']['use'] == True:
                self.Pnl_RumSet_Y_SUB.Hide()
                self._RumData['Y']['use'] = False
                self.Chk_RumSet_ISUSEDY.SetValue(False)

            self.Pnl_RumSet_Y_SUB1.Show()
            self.Chk_RumSet_OLDUSEY.SetValue(True)
            ratio = self.Encoder_List[4]['ratio']
            offset = self.Encoder_List[4]['offset']
            rumus2 = str('= (' + str('x') + ' + (' + str(offset) + ' * ' + str(-1) + '))' + ' / ' + str(ratio))
            self.Txt_RumSet_ViewY1.SetLabel(rumus2)

        if self._OldRum['Z'] == False:
            self.Pnl_RumSet_Z_SUB1.Hide()
        elif self._OldRum['Z'] == True:

            # PREVENT MULTI CHECK
            if self._RumData['Z']['use'] == True:
                self.Pnl_RumSet_Z_SUB.Hide()
                self._RumData['Z']['use'] = False
                self.Chk_RumSet_ISUSEDZ.SetValue(False)


            self.Pnl_RumSet_Z_SUB1.Show()
            self.Chk_RumSet_OLDUSEZ.SetValue(True)
            ratio = self.Encoder_List[1]['ratio']
            offset = self.Encoder_List[1]['offset']
            rumus2 = str('= (' + str('x') + ' + (' + str(offset) + ' * ' + str(-1) + '))' + ' / ' + str(ratio))
            self.Txt_RumSet_ViewZ1.SetLabel(rumus2)

        if self._OldRum['C'] == False:
            self.Pnl_RumSet_C_SUB1.Hide()
        elif self._OldRum['C'] == True:

            # PREVENT MULTI CHECK
            if self._RumData['C']['use'] == True:
                self.Pnl_RumSet_C_SUB.Hide()
                self._RumData['C']['use'] = False
                self.Chk_RumSet_ISUSEDC.SetValue(False)

            self.Pnl_RumSet_C_SUB1.Show()
            self.Chk_RumSet_OLDUSEC.SetValue(True)
            ratio = self.Encoder_List[5]['ratio']
            offset = self.Encoder_List[5]['offset']
            rumus2 = str('= (' + str('x') + ' + (' + str(offset) + ' * ' +  str(-1) + '))' + ' / ' + str(ratio))
            self.Txt_RumSet_ViewC1.SetLabel(rumus2)

        # dict_name = {1: 'Z', 4: 'Y', 5: 'C', 6: 'X'}

        self.Refresh()

    def StarterRum2(self):


        # NEW RUMUS

        config = ConfigParser.ConfigParser()
        config.readfp(open(r'config.txt'))

        x_using = float(config.get('CalculateEncoder Data', 'X_USING'))
        y_using = float(config.get('CalculateEncoder Data', 'Y_USING'))
        z_using = float(config.get('CalculateEncoder Data', 'Z_USING'))
        c_using = float(config.get('CalculateEncoder Data', 'C_USING'))

        x_const = float(config.get('CalculateEncoder Data', 'X_Constant'))
        y_const = float(config.get('CalculateEncoder Data', 'Y_Constant'))
        z_const = float(config.get('CalculateEncoder Data', 'Z_Constant'))
        c_const = float(config.get('CalculateEncoder Data', 'C_Constant'))

        x_plus = float(config.get('CalculateEncoder Data', 'X_Plus'))
        y_plus = float(config.get('CalculateEncoder Data', 'Y_Plus'))
        z_plus = float(config.get('CalculateEncoder Data', 'Z_Plus'))
        c_plus = float(config.get('CalculateEncoder Data', 'C_Plus'))

        # self._RumData['C']['use']
        x_dat = {'use': x_using,
                 'constant': x_const,
                 'plus': x_plus}
        y_dat = {'use': y_using,
                 'constant': y_const,
                 'plus': y_plus}
        z_dat = {'use': z_using,
                 'constant': z_const,
                 'plus': z_plus}
        c_dat = {'use': c_using,
                 'constant': c_const,
                 'plus': c_plus}

        self._RumData = {'X': x_dat,
                         'Y': y_dat,
                         'Z': z_dat,
                         'C': c_dat}

        if self._RumData['X']['use'] == False:
            self.Chk_RumSet_ISUSEDX.SetValue(False)
        elif self._RumData['X']['use'] == True:
            self.Chk_RumSet_ISUSEDX.SetValue(True)
        self.ShowFrams(mode=1, val=self.Chk_RumSet_ISUSEDX.GetValue())

        if self._RumData['Y']['use'] == False:
            self.Chk_RumSet_ISUSEDY.SetValue(False)
        elif self._RumData['Y']['use'] == True:
            self.Chk_RumSet_ISUSEDY.SetValue(True)
        self.ShowFrams(mode=2, val=self.Chk_RumSet_ISUSEDY.GetValue())

        if self._RumData['Z']['use'] == False:
            self.Chk_RumSet_ISUSEDZ.SetValue(False)
        elif self._RumData['Z']['use'] == True:
            self.Chk_RumSet_ISUSEDZ.SetValue(True)
        self.ShowFrams(mode=3, val=self.Chk_RumSet_ISUSEDZ.GetValue())

        if self._RumData['C']['use'] == False:
            self.Chk_RumSet_ISUSEDC.SetValue(False)
        elif self._RumData['C']['use'] == True:
            self.Chk_RumSet_ISUSEDC.SetValue(True)
        self.ShowFrams(mode=4, val=self.Chk_RumSet_ISUSEDC.GetValue())


        #SET TEXT
        self.Txt_RumSet_ConstantX.SetValue(str(self._RumData['X']['constant']))
        self.Txt_RumSet_ConstantY.SetValue(str(self._RumData['Y']['constant']))
        self.Txt_RumSet_ConstantZ.SetValue(str(self._RumData['Z']['constant']))
        self.Txt_RumSet_ConstantC.SetValue(str(self._RumData['C']['constant']))

        self.Txt_RumSet_PlusX.SetValue(str(self._RumData['X']['plus']))
        self.Txt_RumSet_PlusY.SetValue(str(self._RumData['Y']['plus']))
        self.Txt_RumSet_PlusZ.SetValue(str(self._RumData['Z']['plus']))
        self.Txt_RumSet_PlusC.SetValue(str(self._RumData['C']['plus']))

        # SET TEXT OLD
        self.Txt_RumSet_OffsetX.SetValue(str(self.Encoder_List[6]['offset']))
        self.Txt_RumSet_OffsetY.SetValue(str(self.Encoder_List[4]['offset']))
        self.Txt_RumSet_OffsetZ.SetValue(str(self.Encoder_List[1]['offset']))
        self.Txt_RumSet_OffsetC.SetValue(str(self.Encoder_List[5]['offset']))

        self.Txt_RumSet_RatioX.SetValue(str(self.Encoder_List[6]['ratio']))
        self.Txt_RumSet_RatioY.SetValue(str(self.Encoder_List[4]['ratio']))
        self.Txt_RumSet_RatioZ.SetValue(str(self.Encoder_List[1]['ratio']))
        self.Txt_RumSet_RatioC.SetValue(str(self.Encoder_List[5]['ratio']))


        #OLD Rumus
        x_using2 = float(config.get('Old Rumus', 'X_USING'))
        y_using2 = float(config.get('Old Rumus', 'Y_USING'))
        z_using2 = float(config.get('Old Rumus', 'Z_USING'))
        c_using2 = float(config.get('Old Rumus', 'C_USING'))
        self._OldRum = {'X':x_using2,
                       'Y':y_using2,
                       'Z':z_using2,
                       'C':c_using2}

        if self._OldRum['X'] == False:
            self.Chk_RumSet_OLDUSEX.SetValue(False)
        elif self._OldRum['X'] == True:

            #PREVENT MULTI CHECK
            if self._RumData['X']['use'] == True:
                self.Chk_RumSet_ISUSEDX.SetValue(False)

            self.Chk_RumSet_OLDUSEX.SetValue(True)
        self.ShowFrams2(mode=1, val=self.Chk_RumSet_OLDUSEX.GetValue())

        if self._OldRum['Y'] == False:
            self.Chk_RumSet_OLDUSEY.SetValue(False)
        elif self._OldRum['Y'] == True:

            # PREVENT MULTI CHECK
            if self._RumData['Y']['use'] == True:
                self.Chk_RumSet_ISUSEDY.SetValue(False)

            self.Chk_RumSet_OLDUSEY.SetValue(True)
        self.ShowFrams2(mode=2, val=self.Chk_RumSet_OLDUSEY.GetValue())

        if self._OldRum['Z'] == False:
            self.Chk_RumSet_OLDUSEZ.SetValue(False)
        elif self._OldRum['Z'] == True:

            # PREVENT MULTI CHECK
            if self._RumData['Z']['use'] == True:
                self.Chk_RumSet_ISUSEDZ.SetValue(False)

            self.Chk_RumSet_OLDUSEZ.SetValue(True)
        self.ShowFrams2(mode=3, val=self.Chk_RumSet_OLDUSEZ.GetValue())

        if self._OldRum['C'] == False:
            self.Chk_RumSet_OLDUSEC.SetValue(False)
        elif self._OldRum['C'] == True:

            # PREVENT MULTI CHECK
            if self._RumData['C']['use'] == True:
                self.Chk_RumSet_ISUSEDC.SetValue(False)

            self.Chk_RumSet_OLDUSEC.SetValue(True)
        self.ShowFrams2(mode=4, val=self.Chk_RumSet_OLDUSEC.GetValue())

    # Virtual event handlers, overide them in your derived class
    def ShowFrams(self, mode, val):
        if mode == 1:
            other_val = self.Chk_RumSet_OLDUSEX.GetValue()
            if val == True:
                if other_val != False:
                    self.Chk_RumSet_OLDUSEX.SetValue(False)
                # self._OldRum['X'] = False

                self.Pnl_RumSet_X_SUB1.Hide()
                self.Pnl_RumSet_X_SUB.Show()
                self.Pnl_RumSet_X_SUB.SetMaxSize(wx.Size(-1, -1))
                rumus = str(
                    '= (' + str(self._RumData['X']['constant']) + ' * x) + (' + str(self._RumData['X']['plus']) + ')')
                self.Txt_RumSet_ViewX.SetLabel(rumus)
            elif val == False:
                if other_val != True:
                    self.Chk_RumSet_OLDUSEX.SetValue(True)
                # self._OldRum['X'] = True

                self.Pnl_RumSet_X_SUB1.Show()
                self.Pnl_RumSet_X_SUB.Hide()
                self.Pnl_RumSet_X_SUB.SetMaxSize(wx.Size(1, 1))

            self._RumData['X']['use'] = val
        elif mode == 2:
            other_val = self.Chk_RumSet_OLDUSEY.GetValue()
            if val == True:
                if other_val != False:
                    self.Chk_RumSet_OLDUSEY.SetValue(False)
                # self._OldRum['Y'] = False

                self.Pnl_RumSet_Y_SUB1.Hide()
                self.Pnl_RumSet_Y_SUB.Show()
                self.Pnl_RumSet_Y_SUB.SetMaxSize(wx.Size(-1, -1))
                rumus = str(
                    '= (' + str(self._RumData['Y']['constant']) + ' * x) + (' + str(self._RumData['Y']['plus']) + ')')
                self.Txt_RumSet_ViewY.SetLabel(rumus)
            elif val == False:
                if other_val != True:
                    self.Chk_RumSet_OLDUSEY.SetValue(True)
                # self._OldRum['Y'] = True


                self.Pnl_RumSet_Y_SUB.Hide()
                self.Pnl_RumSet_Y_SUB.SetMaxSize(wx.Size(1, 1))
                self.Pnl_RumSet_Y_SUB1.Show()

            self._RumData['Y']['use'] = val
        elif mode == 3:
            other_val = self.Chk_RumSet_OLDUSEZ.GetValue()
            if val == True:
                if other_val != False:
                    self.Chk_RumSet_OLDUSEZ.SetValue(False)
                # self._OldRum['Z'] = False

                self.Pnl_RumSet_Z_SUB1.Hide()
                self.Pnl_RumSet_Z_SUB.SetMaxSize(wx.Size(-1, -1))
                self.Pnl_RumSet_Z_SUB.Show()
                rumus = str(
                    '= (' + str(self._RumData['Z']['constant']) + ' * x) + (' + str(self._RumData['Z']['plus']) + ')')
                self.Txt_RumSet_ViewZ.SetLabel(rumus)
            elif val == False:
                if other_val != True:
                    self.Chk_RumSet_OLDUSEZ.SetValue(True)
                # self._OldRum['Z'] = True

                self.Pnl_RumSet_Z_SUB.Hide()
                self.Pnl_RumSet_Z_SUB.SetMaxSize(wx.Size(1, 1))
                self.Pnl_RumSet_Z_SUB1.Show()

            self._RumData['Z']['use'] = val
        elif mode == 4:
            other_val = self.Chk_RumSet_OLDUSEC.GetValue()
            if val == True:
                if other_val != False:
                    self.Chk_RumSet_OLDUSEC.SetValue(False)
                # self._OldRum['C'] = False

                self.Pnl_RumSet_C_SUB1.Hide()
                self.Pnl_RumSet_C_SUB.Show()
                self.Pnl_RumSet_C_SUB.SetMaxSize(wx.Size(-1, -1))
                rumus = str(
                    '= (' + str(self._RumData['C']['constant']) + ' * x) + (' + str(self._RumData['C']['plus']) + ')')
                self.Txt_RumSet_ViewC.SetLabel(rumus)
            elif val == False:
                if other_val != True:
                    self.Chk_RumSet_OLDUSEC.SetValue(True)
                # self._OldRum['C'] = True

                self.Pnl_RumSet_C_SUB.Hide()
                self.Pnl_RumSet_C_SUB.SetMaxSize(wx.Size(1, 1))
                self.Pnl_RumSet_C_SUB1.Show()

            self._RumData['C']['use'] = val

        self.Refresh()

    def ShowFrams2(self, mode, val):
        if mode == 1:
            other_val = self.Chk_RumSet_ISUSEDX.GetValue()

            if val == True:
                if other_val != False:
                    self.Chk_RumSet_ISUSEDX.SetValue(False)
                # self._RumData['X']['use'] = False

                self.Pnl_RumSet_X_SUB.Hide()
                self.Pnl_RumSet_X_SUB.SetMaxSize(wx.Size(1, 1))
                self.Pnl_RumSet_X_SUB1.Show()

                ratio = self.Encoder_List[6]['ratio']
                offset = self.Encoder_List[6]['offset']
                rumus2 = str('= (' + str('x') + ' + (' + str(offset) + ' * ' + str(-1) + '))' + ' / ' + str(ratio))
                self.Txt_RumSet_ViewX1.SetLabel(rumus2)
            elif val == False:

                if other_val != True:
                    self.Chk_RumSet_ISUSEDX.SetValue(True)

                # self._RumData['X']['use'] = True


                self.Pnl_RumSet_X_SUB1.Hide()
                self.Pnl_RumSet_X_SUB.SetMaxSize(wx.Size(-1, -1))
                self.Pnl_RumSet_X_SUB.Show()

            self._OldRum['X'] = val
        elif mode == 2:

            other_val = self.Chk_RumSet_ISUSEDY.GetValue()

            if val == True:
                if other_val != False:
                    self.Chk_RumSet_ISUSEDY.SetValue(False)
                # self._RumData['Y']['use'] = False

                self.Pnl_RumSet_Y_SUB.Hide()
                self.Pnl_RumSet_Y_SUB.SetMaxSize(wx.Size(1, 1))
                self.Pnl_RumSet_Y_SUB1.Show()

                ratio = self.Encoder_List[4]['ratio']
                offset = self.Encoder_List[4]['offset']
                rumus2 = str('= (' + str('x') + ' + (' + str(offset) + ' * ' + str(-1) + '))' + ' / ' + str(ratio))
                self.Txt_RumSet_ViewY1.SetLabel(rumus2)
            elif val == False:
                if other_val != True:
                    self.Chk_RumSet_ISUSEDY.SetValue(True)
                # self._RumData['Y']['use'] = True

                self.Pnl_RumSet_Y_SUB1.Hide()
                self.Pnl_RumSet_Y_SUB.SetMaxSize(wx.Size(-1, -1))
                self.Pnl_RumSet_Y_SUB.Show()
            self._OldRum['Y'] = val
        elif mode == 3:

            other_val = self.Chk_RumSet_ISUSEDZ.GetValue()
            if val == True:
                if other_val != False:
                    self.Chk_RumSet_ISUSEDZ.SetValue(False)
                # self._RumData['Z']['use'] = False

                self.Pnl_RumSet_Z_SUB.SetMaxSize(wx.Size(1, 1))
                self.Pnl_RumSet_Z_SUB.Hide()
                self.Pnl_RumSet_Z_SUB1.Show()

                ratio = self.Encoder_List[1]['ratio']
                offset = self.Encoder_List[1]['offset']
                rumus2 = str('= (' + str('x') + ' + (' + str(offset) + ' * ' + str(-1) + '))' + ' / ' + str(ratio))
                self.Txt_RumSet_ViewZ1.SetLabel(rumus2)
            elif val == False:
                if other_val != True:
                    self.Chk_RumSet_ISUSEDZ.SetValue(True)
                # self._RumData['Z']['use'] = True

                self.Pnl_RumSet_Z_SUB1.Hide()
                self.Pnl_RumSet_Z_SUB.SetMaxSize(wx.Size(-1, -1))
                self.Pnl_RumSet_Z_SUB.Show()
            self._OldRum['Z'] = val
        elif mode == 4:

            other_val = self.Chk_RumSet_ISUSEDC.GetValue()
            if val == True:
                if other_val != False:
                    self.Chk_RumSet_ISUSEDC.SetValue(False)
                # self._RumData['C']['use'] = False

                self.Pnl_RumSet_C_SUB.Hide()
                self.Pnl_RumSet_C_SUB.SetMaxSize(wx.Size(1, 1))
                self.Pnl_RumSet_C_SUB1.Show()

                ratio = self.Encoder_List[5]['ratio']
                offset = self.Encoder_List[5]['offset']
                rumus2 = str('= (' + str('x') + ' + (' + str(offset) + ' * ' + str(-1) + '))' + ' / ' + str(ratio))
                self.Txt_RumSet_ViewC1.SetLabel(rumus2)
            elif val == False:
                if other_val != True:
                    self.Chk_RumSet_ISUSEDC.SetValue(True)
                # self._RumData['C']['use'] = True

                self.Pnl_RumSet_C_SUB1.Hide()
                self.Pnl_RumSet_C_SUB.SetMaxSize(wx.Size(-1, -1))
                self.Pnl_RumSet_C_SUB.Show()

            self._OldRum['C'] = val

        self.Refresh()

    def ChangeRumus(self, motor, types):

        # rumus = str( '(' + str(self.Txt_RumSet_ConstantX) + ' * x) + (' + str(self.Txt_RumSet_PlusX) + ')' )

        if motor == 'X':
            rumus = str('= (' + str(self.Txt_RumSet_ConstantX.GetValue()) + ' * x) + (' + str(self.Txt_RumSet_PlusX.GetValue()) + ')')
            self.Txt_RumSet_ViewX.SetLabel(rumus)
        elif motor == 'Y':
            rumus = str('= (' + str(self.Txt_RumSet_ConstantY.GetValue()) + ' * x) + (' + str(self.Txt_RumSet_PlusY.GetValue()) + ')')
            self.Txt_RumSet_ViewY.SetLabel(rumus)
        elif motor == 'Z':
            rumus = str('= (' + str(self.Txt_RumSet_ConstantZ.GetValue()) + ' * x) + (' + str(self.Txt_RumSet_PlusZ.GetValue()) + ')')
            self.Txt_RumSet_ViewZ.SetLabel(rumus)
        elif motor == 'C':
            rumus = str('= (' + str(self.Txt_RumSet_ConstantC.GetValue()) + ' * x) + (' + str(self.Txt_RumSet_PlusC.GetValue()) + ')')
            self.Txt_RumSet_ViewC.SetLabel(rumus)


    def ChangeRumus2(self, motor, types):

        # rumus = str( '(' + str(self.Txt_RumSet_ConstantX) + ' * x) + (' + str(self.Txt_RumSet_PlusX) + ')' )

        if motor == 'X':
            ratio = self.Encoder_List[6]['ratio']
            offset = self.Encoder_List[6]['offset']
            rumus2 = str('= (' + str('x') + ' + (' + str(offset) + ' * ' + str(-1) + '))' + ' / ' + str(ratio))
            self.Txt_RumSet_ViewX1.SetLabel(rumus2)
        elif motor == 'Y':
            ratio = self.Encoder_List[4]['ratio']
            offset = self.Encoder_List[4]['offset']
            rumus2 = str('= (' + str('x') + ' + (' + str(offset) + ' * ' + str(-1) + '))' + ' / ' + str(ratio))
            self.Txt_RumSet_ViewY1.SetLabel(rumus2)
        elif motor == 'Z':
            ratio = self.Encoder_List[1]['ratio']
            offset = self.Encoder_List[1]['offset']
            rumus2 = str('= (' + str('x') + ' + (' + str(offset) + ' * ' + str(-1) + '))' + ' / ' + str(ratio))
            self.Txt_RumSet_ViewZ1.SetLabel(rumus2)
        elif motor == 'C':
            ratio = self.Encoder_List[5]['ratio']
            offset = self.Encoder_List[5]['offset']
            rumus2 = str('= (' + str('x') + ' + (' + str(offset) + ' * ' + str(-1) + '))' + ' / ' + str(ratio))
            self.Txt_RumSet_ViewC1.SetLabel(rumus2)



    def ApplySet(self):

        self._RumData['X']['use'] = self.Chk_RumSet_ISUSEDX.GetValue()
        self._RumData['Y']['use'] = self.Chk_RumSet_ISUSEDY.GetValue()
        self._RumData['Z']['use'] = self.Chk_RumSet_ISUSEDZ.GetValue()
        self._RumData['C']['use'] = self.Chk_RumSet_ISUSEDC.GetValue()
        print self._RumData

        self._OldRum['X'] = self.Chk_RumSet_OLDUSEX.GetValue()
        self._OldRum['Y'] = self.Chk_RumSet_OLDUSEY.GetValue()
        self._OldRum['Z'] = self.Chk_RumSet_OLDUSEZ.GetValue()
        self._OldRum['C'] = self.Chk_RumSet_OLDUSEC.GetValue()

        # NEW RUMUS
        # ===============================================================================================================

        # X
        #============================================================================
        if self._RumData['X']['use'] == False:
            self.AddConfigs(group='CalculateEncoder Data', title='X_USING', value='0')
            # self.AddConfigs(group='CalculateEncoder Data', title='X_Constant', value='0')
            # self.AddConfigs(group='CalculateEncoder Data', title='X_Plus', value='0')



        elif self._RumData['X']['use'] == True:
            constant = self.Txt_RumSet_ConstantX.GetValue()
            if constant == '':
                constant = 0
            else:
                try:
                    constant = float(constant)
                except Exception as e:
                    Msgbox = wx.MessageDialog(None, str(e) + '\n' + 'Data Constant X Tidak Valid',
                                              'Error Saving Setting', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                    return

            plus = self.Txt_RumSet_PlusX.GetValue()
            if plus == '':
                plus= 0
            else:
                try:
                    plus= float(plus)
                except Exception as e:
                    Msgbox = wx.MessageDialog(None, str(e) + '\n' + 'Data Plus X Tidak Valid',
                                              'Error Saving Setting', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                    return


            self.AddConfigs(group='CalculateEncoder Data', title='X_USING', value='1')

            self._RumData['X']['constant'] = float(constant)
            self.AddConfigs(group='CalculateEncoder Data', title='X_Constant', value=str(constant))

            self._RumData['X']['plus'] = float(plus)
            self.AddConfigs(group='CalculateEncoder Data', title='X_Plus', value=str(plus))



        # Y
        # ============================================================================
        if self._RumData['Y']['use'] == False:
            self.AddConfigs(group='CalculateEncoder Data', title='Y_USING', value='0')
            # self.AddConfigs(group='CalculateEncoder Data', title='Y_Constant', value='0')
            # self.AddConfigs(group='CalculateEncoder Data', title='Y_Plus', value='0')



        elif self._RumData['Y']['use'] == True:
            constant = self.Txt_RumSet_ConstantY.GetValue()
            if constant == '':
                constant = 0
            else:
                try:
                    constant = float(constant)
                except Exception as e:
                    Msgbox = wx.MessageDialog(None, str(e) + '\n' + 'Data Constant Y Tidak Valid',
                                              'Error Saving Setting', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                    return

            plus = self.Txt_RumSet_PlusY.GetValue()
            if plus == '':
                plus = 0
            else:
                try:
                    plus = float(plus)
                except Exception as e:
                    Msgbox = wx.MessageDialog(None, str(e) + '\n' + 'Data Plus Y Tidak Valid',
                                              'Error Saving Setting', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                    return

            self.AddConfigs(group='CalculateEncoder Data', title='Y_USING', value='1')

            self._RumData['Y']['constant'] = float(constant)
            self.AddConfigs(group='CalculateEncoder Data', title='Y_Constant', value=str(constant))

            self._RumData['Y']['plus'] = float(plus)
            self.AddConfigs(group='CalculateEncoder Data', title='Y_Plus', value=str(plus))



        # Z
        # ============================================================================
        if self._RumData['Z']['use'] == False:
            self.AddConfigs(group='CalculateEncoder Data', title='Z_USING', value='0')
            # self.AddConfigs(group='CalculateEncoder Data', title='Z_Constant', value='0')
            # self.AddConfigs(group='CalculateEncoder Data', title='Z_Plus', value='0')



        elif self._RumData['Z']['use'] == True:
            constant = self.Txt_RumSet_ConstantZ.GetValue()
            if constant == '':
                constant = 0
            else:
                try:
                    constant = float(constant)
                except Exception as e:
                    Msgbox = wx.MessageDialog(None, str(e) + '\n' + 'Data Constant Z Tidak Valid',
                                              'Error Saving Setting', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                    return

            plus = self.Txt_RumSet_PlusZ.GetValue()
            if plus == '':
                plus = 0
            else:
                try:
                    plus = float(plus)
                except Exception as e:
                    Msgbox = wx.MessageDialog(None, str(e) + '\n' + 'Data Plus Z Tidak Valid',
                                              'Error Saving Setting', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                    return

            self.AddConfigs(group='CalculateEncoder Data', title='Z_USING', value='1')

            self._RumData['Z']['constant'] = float(constant)
            self.AddConfigs(group='CalculateEncoder Data', title='Z_Constant', value=str(constant))

            self._RumData['Z']['plus'] = float(plus)
            self.AddConfigs(group='CalculateEncoder Data', title='Z_Plus', value=str(plus))



        # C
        # ============================================================================
        if self._RumData['C']['use'] == False:
            self.AddConfigs(group='CalculateEncoder Data', title='C_USING', value='0')
            # self.AddConfigs(group='CalculateEncoder Data', title='C_Constant', value='0')
            # self.AddConfigs(group='CalculateEncoder Data', title='C_Plus', value='0')



        elif self._RumData['C']['use'] == True:
            constant = self.Txt_RumSet_ConstantC.GetValue()
            if constant == '':
                constant = 0
            else:
                try:
                    constant = float(constant)
                except Exception as e:
                    Msgbox = wx.MessageDialog(None, str(e) + '\n' + 'Data Constant C Tidak Valid',
                                              'Error Saving Setting', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                    return

            plus = self.Txt_RumSet_PlusC.GetValue()
            if plus == '':
                plus = 0
            else:
                try:
                    plus = float(plus)
                except Exception as e:
                    Msgbox = wx.MessageDialog(None, str(e) + '\n' + 'Data Plus C Tidak Valid',
                                              'Error Saving Setting', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                    return

            self.AddConfigs(group='CalculateEncoder Data', title='C_USING', value='1')

            self._RumData['C']['constant'] = float(constant)
            self.AddConfigs(group='CalculateEncoder Data', title='C_Constant', value=str(constant))

            self._RumData['C']['plus'] = float(plus)
            self.AddConfigs(group='CalculateEncoder Data', title='C_Plus', value=str(plus))

        # OLD RUMUS
        #===============================================================================================================

        # X
        # ============================================================================
        if self._OldRum['X'] == False:
            self.AddConfigs(group='Old Rumus', title='X_USING', value='0')
            # self.AddConfigs(group='CalculateEncoder Data', title='X_Constant', value='0')
            # self.AddConfigs(group='CalculateEncoder Data', title='X_Plus', value='0')



        elif self._OldRum['X'] == True:
            offset = self.Txt_RumSet_OffsetX.GetValue()
            if offset == '':
                offset = 0
            else:
                try:
                    offset = float(offset)
                except Exception as e:
                    Msgbox = wx.MessageDialog(None, str(e) + '\n' + 'Data offset X Tidak Valid',
                                              'Error Saving Setting', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                    return

            ratio = self.Txt_RumSet_RatioX.GetValue()
            if ratio == '':
                ratio = 0
            else:
                try:
                    ratio = float(ratio)
                except Exception as e:
                    Msgbox = wx.MessageDialog(None, str(e) + '\n' + 'Data ratio X Tidak Valid',
                                              'Error Saving Setting', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                    return

            self.AddConfigs(group='Old Rumus', title='X_USING', value='1')

            # self._OldRum['X']['offset'] = float(offset)
            # self.AddConfigs(group='Old Rumus', title='X_offset', value=str(offset))


            # self._OldRum['X']['ratio'] = float(ratio)
            # self.AddConfigs(group='Old Rumus', title='X_ratio', value=str(ratio))

            self.SaveOffset(motorid=6, offset=offset)
            self.SaveRatio(motorid=6, ratio=ratio)

        # Y
        # ============================================================================
        if self._OldRum['Y'] == False:
            self.AddConfigs(group='Old Rumus', title='Y_USING', value='0')
            # self.AddConfigs(group='Old Rumus', title='Y_offset', value='0')
            # self.AddConfigs(group='Old Rumus', title='Y_ratio', value='0')



        elif self._OldRum['Y'] == True:
            offset = self.Txt_RumSet_OffsetY.GetValue()
            if offset == '':
                offset = 0
            else:
                try:
                    offset = float(offset)
                except Exception as e:
                    Msgbox = wx.MessageDialog(None, str(e) + '\n' + 'Data offset Y Tidak Valid',
                                              'Error Saving Setting', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                    return

            ratio = self.Txt_RumSet_RatioY.GetValue()
            if ratio == '':
                ratio = 0
            else:
                try:
                    ratio = float(ratio)
                except Exception as e:
                    Msgbox = wx.MessageDialog(None, str(e) + '\n' + 'Data ratio Y Tidak Valid',
                                              'Error Saving Setting', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                    return

            self.AddConfigs(group='Old Rumus', title='Y_USING', value='1')

            # self._OldRum['Y']['offset'] = float(offset)
            # self.AddConfigs(group='Old Rumus', title='Y_offset', value=str(offset))
            #
            # self._OldRum['Y']['ratio'] = float(ratio)
            # self.AddConfigs(group='Old Rumus', title='Y_ratio', value=str(ratio))

            self.SaveOffset(motorid=4 ,offset=offset)
            self.SaveRatio(motorid=4, ratio=ratio)

        # Z
        # ============================================================================
        if self._OldRum['Z'] == False:
            self.AddConfigs(group='Old Rumus', title='Z_USING', value='0')
            # self.AddConfigs(group='Old Rumus', title='Z_offset', value='0')
            # self.AddConfigs(group='Old Rumus', title='Z_ratio', value='0')



        elif self._OldRum['Z'] == True:
            offset = self.Txt_RumSet_OffsetZ.GetValue()
            if offset == '':
                offset = 0
            else:
                try:
                    offset = float(offset)
                except Exception as e:
                    Msgbox = wx.MessageDialog(None, str(e) + '\n' + 'Data offset Z Tidak Valid',
                                              'Error Saving Setting', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                    return

            ratio = self.Txt_RumSet_RatioZ.GetValue()
            if ratio == '':
                ratio = 0
            else:
                try:
                    ratio = float(ratio)
                except Exception as e:
                    Msgbox = wx.MessageDialog(None, str(e) + '\n' + 'Data ratio Z Tidak Valid',
                                              'Error Saving Setting', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                    return

            self.AddConfigs(group='Old Rumus', title='Z_USING', value='1')

            # self._OldRum['Z']['offset'] = float(offset)
            # self.AddConfigs(group='Old Rumus', title='Z_offset', value=str(offset))
            #
            # self._OldRum['Z']['ratio'] = float(ratio)
            # self.AddConfigs(group='Old Rumus', title='Z_ratio', value=str(ratio))

            self.SaveOffset(motorid=1, offset=offset)
            self.SaveRatio(motorid=1, ratio=ratio)

        # C
        # ============================================================================
        if self._OldRum['C'] == False:
            self.AddConfigs(group='Old Rumus', title='C_USING', value='0')
            # self.AddConfigs(group='Old Rumus', title='C_offset', value='0')
            # self.AddConfigs(group='Old Rumus', title='C_ratio', value='0')



        elif self._OldRum['C'] == True:
            offset = self.Txt_RumSet_OffsetC.GetValue()
            if offset == '':
                offset = 0
            else:
                try:
                    offset = float(offset)
                except Exception as e:
                    Msgbox = wx.MessageDialog(None, str(e) + '\n' + 'Data offset C Tidak Valid',
                                              'Error Saving Setting', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                    return

            ratio = self.Txt_RumSet_RatioC.GetValue()
            if ratio == '':
                ratio = 0
            else:
                try:
                    ratio = float(ratio)
                except Exception as e:
                    Msgbox = wx.MessageDialog(None, str(e) + '\n' + 'Data ratio C Tidak Valid',
                                              'Error Saving Setting', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                    return

            self.AddConfigs(group='Old Rumus', title='C_USING', value='1')

            # self._OldRum['C']['offset'] = float(offset)
            # self.AddConfigs(group='Old Rumus', title='C_offset', value=str(offset))
            #
            # self._OldRum['C']['ratio'] = float(ratio)
            # self.AddConfigs(group='Old Rumus', title='C_ratio', value=str(ratio))

            self.SaveOffset(motorid=5, offset=offset)
            self.SaveRatio(motorid=5, ratio=ratio)

        # ENCODER STARTER
        self.EncoderStarter()

        # Save End
        Msgbox = wx.MessageDialog(None, 'Save Data Succesful!',
                                  'Done', wx.OK | wx.ICON_INFORMATION | wx.STAY_ON_TOP)

        klick = Msgbox.ShowModal()


    def Exit(self):
        self.Destroy()

    def AddConfigs(self, group, title, value ):
        config = ConfigParser.ConfigParser()
        config.readfp(open(r'config.txt'))
        with open('config.txt', 'w+') as configfile:
            config.set(str(group), str(title), str(value))
            config.write(configfile)


    def SaveOffset(self, motorid, offset):
        if offset == '':
            Msgbox = wx.MessageDialog(None, 'Value Offset Cannot Empty!',
                                      'Done', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

            klick = Msgbox.ShowModal()
            return
        self.UpdateOffsetEncoder(motorid=int(motorid), values=float(offset))

    def SaveRatio(self, motorid, ratio):
        if ratio == '':
            Msgbox = wx.MessageDialog(None, 'Value Offset Cannot Empty!',
                                      'Done', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

            klick = Msgbox.ShowModal()
            return
        self.UpdateRatioEncoder(motorid=int(motorid), values=float(ratio))
# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import ConfigParser


###########################################################################
## Class FramesEncodePers
###########################################################################

class FramesEncodePers(wx.Frame):
    def __init__(self, parent):


        #load
        # self._PersamaanEncoder = {6: float(pers_x), 4: float(pers_y), 1: float(pers_z), 5: float(pers_c)}
        xx = self._PersamaanEncoder[6]
        yy = self._PersamaanEncoder[4]
        zz = self._PersamaanEncoder[1]
        cc = self._PersamaanEncoder[5]

        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=u"Setting Persamaan Encoder", pos=wx.DefaultPosition,
                          size=wx.Size(301, 250), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        fgSizer1 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer1.SetFlexibleDirection(wx.BOTH)
        fgSizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.olololol = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.olololol.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_INACTIVEBORDER))

        fgSizer2 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer2.SetFlexibleDirection(wx.BOTH)
        fgSizer2.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Lulululu = wx.StaticText(self.olololol, wx.ID_ANY, u"Persamanaan Encoder Value", wx.DefaultPosition,
                                      wx.DefaultSize, 0)
        self.Lulululu.Wrap(-1)
        self.Lulululu.SetFont(wx.Font(14, 74, 90, 92, False, "Arial"))

        fgSizer2.Add(self.Lulululu, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.i_dun_even_care = wx.Panel(self.olololol, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.i_dun_even_care.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHTTEXT))

        fgSizer3 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer3.SetFlexibleDirection(wx.BOTH)
        fgSizer3.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        gbSizer1 = wx.GridBagSizer(0, 0)
        gbSizer1.SetFlexibleDirection(wx.BOTH)
        gbSizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Txt_Encode_Xpers = wx.StaticText(self.i_dun_even_care, wx.ID_ANY, u"Motor X", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.Txt_Encode_Xpers.Wrap(-1)
        gbSizer1.Add(self.Txt_Encode_Xpers, wx.GBPosition(0, 0), wx.GBSpan(1, 1), wx.ALL, 5)

        self.Txt_PersEnd_X = wx.TextCtrl(self.i_dun_even_care, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                         wx.DefaultSize, 0)
        gbSizer1.Add(self.Txt_PersEnd_X, wx.GBPosition(0, 1), wx.GBSpan(1, 1), wx.ALL, 5)

        self.Txt_Encode_Ypers = wx.StaticText(self.i_dun_even_care, wx.ID_ANY, u"Motor Y", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.Txt_Encode_Ypers.Wrap(-1)
        gbSizer1.Add(self.Txt_Encode_Ypers, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL, 5)

        self.Txt_PersEnd_Y = wx.TextCtrl(self.i_dun_even_care, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                         wx.DefaultSize, 0)
        gbSizer1.Add(self.Txt_PersEnd_Y, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL, 5)

        self.Txt_Encode_Zpers = wx.StaticText(self.i_dun_even_care, wx.ID_ANY, u"Motor Z", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.Txt_Encode_Zpers.Wrap(-1)
        gbSizer1.Add(self.Txt_Encode_Zpers, wx.GBPosition(2, 0), wx.GBSpan(1, 1), wx.ALL, 5)

        self.Txt_PersEnd_Z = wx.TextCtrl(self.i_dun_even_care, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                         wx.DefaultSize, 0)
        gbSizer1.Add(self.Txt_PersEnd_Z, wx.GBPosition(2, 1), wx.GBSpan(1, 1), wx.ALL, 5)

        self.Txt_Encode_Cpers = wx.StaticText(self.i_dun_even_care, wx.ID_ANY, u"Motor C", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.Txt_Encode_Cpers.Wrap(-1)
        gbSizer1.Add(self.Txt_Encode_Cpers, wx.GBPosition(3, 0), wx.GBSpan(1, 1), wx.ALL, 5)

        self.Txt_PersEnd_C = wx.TextCtrl(self.i_dun_even_care, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                         wx.DefaultSize, 0)
        gbSizer1.Add(self.Txt_PersEnd_C, wx.GBPosition(3, 1), wx.GBSpan(1, 1), wx.ALL, 5)

        fgSizer3.Add(gbSizer1, 1, wx.EXPAND, 5)

        fgSizer4 = wx.FlexGridSizer(0, 2, 0, 0)
        fgSizer4.SetFlexibleDirection(wx.BOTH)
        fgSizer4.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Cmd_SetPersEncod = wx.Button(self.i_dun_even_care, wx.ID_ANY, u"Set", wx.DefaultPosition, wx.DefaultSize,
                                          0)
        fgSizer4.Add(self.Cmd_SetPersEncod, 0, wx.ALL, 5)

        self.Cmd_CancelPersEncod = wx.Button(self.i_dun_even_care, wx.ID_ANY, u"Cancel", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        fgSizer4.Add(self.Cmd_CancelPersEncod, 0, wx.ALL, 5)

        fgSizer3.Add(fgSizer4, 1, wx.EXPAND, 5)

        self.i_dun_even_care.SetSizer(fgSizer3)
        self.i_dun_even_care.Layout()
        fgSizer3.Fit(self.i_dun_even_care)
        fgSizer2.Add(self.i_dun_even_care, 1, wx.EXPAND | wx.ALL, 5)

        self.olololol.SetSizer(fgSizer2)
        self.olololol.Layout()
        fgSizer2.Fit(self.olololol)
        fgSizer1.Add(self.olololol, 1, wx.EXPAND | wx.ALL, 5)

        self.SetSizer(fgSizer1)
        self.Layout()

        self.Centre(wx.BOTH)

        # SET VAL
        self.Txt_PersEnd_X.SetValue(str(xx))
        self.Txt_PersEnd_Y.SetValue(str(yy))
        self.Txt_PersEnd_Z.SetValue(str(zz))
        self.Txt_PersEnd_C.SetValue(str(cc))

        self.Show()

        # Connect Events
        self.Cmd_SetPersEncod.Bind(wx.EVT_BUTTON, lambda x:self.PersSet(x=self.Txt_PersEnd_X.GetValue(),
                                                                        y=self.Txt_PersEnd_Y.GetValue(),
                                                                        z=self.Txt_PersEnd_Z.GetValue(),
                                                                        c=self.Txt_PersEnd_C.GetValue()))
        self.Cmd_CancelPersEncod.Bind(wx.EVT_BUTTON, lambda x:self.PersCancel())

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def PersSet(self, x,y,z,c):
        try:
            x = float(x)
        except Exception as e:
            pass
        else:
            self._PersamaanEncoder[6] = round(x,9)
            config = ConfigParser.ConfigParser()
            config.readfp(open(r'config.txt'))
            with open('config.txt', 'w+') as configfile:
                config.set('Persamaan Encoder', 'X', str(self._PersamaanEncoder[6]))
                config.write(configfile)

        try:
            y = float(y)
        except Exception as e:
            pass
        else:
            self._PersamaanEncoder[4] = round(y,9)
            config = ConfigParser.ConfigParser()
            config.readfp(open(r'config.txt'))
            with open('config.txt', 'w+') as configfile:
                config.set('Persamaan Encoder', 'Y', str(self._PersamaanEncoder[4]))
                config.write(configfile)

        try:
            z = float(z)
        except Exception as e:
            pass
        else:
            self._PersamaanEncoder[1] = round(z,9)
            config = ConfigParser.ConfigParser()
            config.readfp(open(r'config.txt'))
            with open('config.txt', 'w+') as configfile:
                config.set('Persamaan Encoder', 'Z', str(self._PersamaanEncoder[1]))
                config.write(configfile)

        try:
            c = float(c)
        except Exception as e:
            pass
        else:
            self._PersamaanEncoder[5] = round(c,9)
            config = ConfigParser.ConfigParser()
            config.readfp(open(r'config.txt'))
            with open('config.txt', 'w+') as configfile:
                config.set('Persamaan Encoder', 'C', str(self._PersamaanEncoder[5]))
                config.write(configfile)


        wx.MessageBox('Set Persamaan Done','SETTING PERSAMAAN')


    def PersCancel(self):
        self.Destroy()



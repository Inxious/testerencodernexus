
-- OFFSET MANAGEMENT 
-- SAVE OffsetMapping
CREATE 
--ALTER
PROCEDURE SP_Nxs_AddOffsetMapping
@Pipete AS VARCHAR(250),
@Coor_Group AS VARCHAR(250),
@X_Off AS FLOAT,
@Y_Off AS FLOAT,
@Z_Off AS FLOAT,
@A_Off AS FLOAT,
@C_Off AS FLOAT
AS
BEGIN
	
	DECLARE @Pipete_ID AS INTEGER , @Group AS INTEGER
	SET @Pipete_ID = (SELECT Pipete_ID FROM M_Pipete_Mapping WHERE Pipete_Name = @Pipete)
	SET @Group = (SELECT GroupID FROM M_KoordinatGroup WHERE Group_Name = @Coor_Group)
	
	-- insert data header
	INSERT INTO M_OffsetMappingHeader(Pipete_ID, Koor_Group)
	VALUES (@Pipete_ID, @Group)
	
	DECLARE @IDS AS INTEGER
	SET @IDS = SCOPE_IDENTITY()
	
	-- insert data detail
	INSERT INTO M_OffsetMappingDetail(Ofmap_HeaderID,Motor_Used,
				Offset_Value)
	VALUES 
		(@IDS, 6, @X_Off), -- X
		(@IDS, 4, @Y_Off), -- Y
		(@IDS, 1, @Z_Off), -- Z
		(@IDS, 3, @A_Off), -- A
		(@IDS, 5, @C_Off)  -- C
END

-- LOAD DATA OFFSET
CREATE PROCEDURE SP_Nxs_GetDataOffsetCoor
AS 
BEGIN

SELECT Ofmap_HeaderID AS [ID], Pipete_Name , Group_Name,
		[MOTOR_X],[MOTOR_Y],[MOTOR_Z],[MOTOR_A],[MOTOR_C]
FROM (
SELECT h.Ofmap_HeaderID, p.Pipete_Name, g.Group_Name,m.MM_Name AS [Motor_Used],d.Offset_Value
FROM M_OffsetMappingHeader AS h
INNER JOIN M_OffsetMappingDetail AS d ON h.Ofmap_HeaderID = d.Ofmap_HeaderID
INNER JOIN M_Motor AS m ON d.Motor_Used = m.MotorID
INNER JOIN M_Pipete_Mapping AS p ON h.Pipete_ID = p.Pipete_ID
INNER JOIN M_KoordinatGroup AS g ON h.Koor_Group = g.GroupID
) AS Tablese
PIVOT
( 
	SUM(Offset_Value)
	FOR Motor_Used IN ([MOTOR_X],[MOTOR_Y],[MOTOR_Z],[MOTOR_A],[MOTOR_C])
) AS PivotTable

END

-- UPDATE OFFSET MAPPING
CREATE 
--ALTER
PROCEDURE SP_Nxs_UpdateOffsetMapping
@Mapping_ID AS INT,
@Pipete AS VARCHAR(250),
@Coor_Group AS VARCHAR(250),
@X_Off AS FLOAT,
@Y_Off AS FLOAT,
@Z_Off AS FLOAT,
@A_Off AS FLOAT,
@C_Off AS FLOAT
AS
BEGIN

	-- update data header
	DECLARE @Pipete_ID AS INTEGER
	DECLARE @Coor_Group_ID AS INTEGER
	SET @Pipete_ID = (SELECT Pipete_ID FROM M_Pipete_Mapping WHERE Pipete_Name = @Pipete)
	SET @Coor_Group_ID = (SELECT GroupID FROM M_KoordinatGroup WHERE Group_Name = @Coor_Group)
	
	UPDATE M_OffsetMappingHeader
	SET Pipete_ID = @Pipete_ID, Koor_Group = @Coor_Group_ID
	WHERE Ofmap_HeaderID = @Mapping_ID
	
	--DECLARE @IDS AS INTEGER
	--SET @IDS = SCOPE_IDENTITY()
	
	-- update data detail
	--X
	UPDATE M_OffsetMappingDetail
	SET Offset_Value = @X_Off
	WHERE Motor_Used = 6 AND Ofmap_HeaderID = @Mapping_ID

	--Y
	UPDATE M_OffsetMappingDetail
	SET Offset_Value = @Y_Off
	WHERE Motor_Used = 4 AND Ofmap_HeaderID = @Mapping_ID

	--Z
	UPDATE M_OffsetMappingDetail
	SET Offset_Value = @Z_Off
	WHERE Motor_Used = 1 AND Ofmap_HeaderID = @Mapping_ID

	--A
	UPDATE M_OffsetMappingDetail
	SET Offset_Value = @A_Off
	WHERE Motor_Used = 3 AND Ofmap_HeaderID = @Mapping_ID

	--C
	UPDATE M_OffsetMappingDetail
	SET Offset_Value = @C_Off
	WHERE Motor_Used = 5 AND Ofmap_HeaderID = @Mapping_ID
	
END


-- DELETE OFFSET MAPPING
CREATE PROCEDURE SP_Nxs_DeleteOffsetMapping
@MappingID AS INTEGER
AS
BEGIN
	-- DELETE DETAIL
	DELETE FROM M_OffsetMappingDetail
	WHERE Ofmap_HeaderID = @MappingID
	
	-- DELETE HEADER
	DELETE FROM M_OffsetMappingHeader
	WHERE Ofmap_HeaderID = @MappingID

END

-- SELECT ALL DATA OFFSET
CREATE PROCEDURE SP_Nxs_GetAllOffsetMappingData
AS
BEGIN

	SELECT h.Ofmap_HeaderID, p.Pipete_Name, g.Group_Name,
			dp.[6] AS [Offset_X], dp.[4] AS [Offset_Y], dp.[1] AS [Offset_Z], dp.[3] AS [Offset_A], dp.[5] as [Offset_C]
	FROM M_OffsetMappingHeader as h
	INNER JOIN M_Pipete_Mapping AS p ON h.Pipete_ID = p.Pipete_ID
	INNER JOIN M_KoordinatGroup AS g ON h.Koor_Group = g.GroupID
	INNER JOIN (
		SELECT * FROM (
			SELECT Ofmap_HeaderID, Motor_Used, Offset_Value
			FROM M_OffsetMappingDetail) sources
		PIVOT
		(
			SUM(Offset_Value)
			FOR Motor_Used IN ([6],[4],[1],[3],[5])
		) targets) AS dp ON h.Ofmap_HeaderID = dp.Ofmap_HeaderID 
	 
END

-- SEARCH BY GROUP 
CREATE 
--ALTER
PROCEDURE SP_Nxs_SearchOffsetMapping
@Group_Name AS VARCHAR(MAX)
AS
BEGIN

	SELECT h.Ofmap_HeaderID, p.Pipete_Name, g.Group_Name,
			dp.[6] AS [Offset_X], dp.[4] AS [Offset_Y], dp.[1] AS [Offset_Z], dp.[3] AS [Offset_A], dp.[5] as [Offset_C]
	FROM M_OffsetMappingHeader as h
	INNER JOIN M_Pipete_Mapping AS p ON h.Pipete_ID = p.Pipete_ID
	INNER JOIN M_KoordinatGroup AS g ON h.Koor_Group = g.GroupID
	INNER JOIN (
		SELECT * FROM (
			SELECT Ofmap_HeaderID, Motor_Used, Offset_Value
			FROM M_OffsetMappingDetail) sources
		PIVOT
		(
			SUM(Offset_Value)
			FOR Motor_Used IN ([6],[4],[1],[3],[5])
		) targets) AS dp ON h.Ofmap_HeaderID = dp.Ofmap_HeaderID
	WHERE g.Group_Name = @Group_Name
	
END



CREATE 
--ALTER
PROCEDURE SP_Nxs_OffsetSort
@ID_List AS TEXT,
@Sorte AS VARCHAR(250),
@Column AS TEXT
AS
BEGIN
	EXECUTE(' SELECT h.Ofmap_HeaderID, p.Pipete_Name, g.Group_Name,
					dp.[6] AS [Offset_X], dp.[4] AS [Offset_Y], dp.[1] AS [Offset_Z], dp.[3] AS [Offset_A], dp.[5] as [Offset_C]
				FROM M_OffsetMappingHeader as h
				INNER JOIN M_Pipete_Mapping AS p ON h.Pipete_ID = p.Pipete_ID
				INNER JOIN M_KoordinatGroup AS g ON h.Koor_Group = g.GroupID
				INNER JOIN (
					SELECT * FROM (
						SELECT Ofmap_HeaderID, Motor_Used, Offset_Value
						FROM M_OffsetMappingDetail) sources
					PIVOT
					(
						SUM(Offset_Value)
						FOR Motor_Used IN ([6],[4],[1],[3],[5])
					) targets) AS dp ON h.Ofmap_HeaderID = dp.Ofmap_HeaderID
	
				WHERE h.Ofmap_HeaderID IN '+ @ID_List +'
				ORDER BY '+ @Column +' '+ @Sorte
	)
	
END

EXEC SP_Nxs_OffsetSort '(1,2,3)','ASC','Pipete_Name'

CREATE 
--ALTER
PROCEDURE SP_Nxs_GetOffsetMapColumnList
AS
BEGIN
	DECLARE @Tab TABLE ([COLUMNS_ALIAS] Varchar(MAX), [COLUMNS] Varchar(MAX))
	INSERT INTO @Tab
	VALUES
		('Ofmap_HeaderID','h.Ofmap_HeaderID'),
		('Pipete_Name','p.Pipete_Name'),
		('Group_Name','g.Group_Name'),
		('Offset_X','dp.[6]'),
		('Offset_Y','dp.[4]'),
		('Offset_Z','dp.[1]'),
		('Offset_A','dp.[3]'),
		('Offset_C','dp.[5]')
		
	SELECT * FROM @Tab
	
END

CREATE 

-- GET ALL EXISTED GROUP
CREATE PROCEDURE SP_Nxs_GetAllGroupExisted
AS
BEGIN

	SELECT Group_Name 
	FROM M_KoordinatGroup
	ORDER BY Group_Name ASC

END


-- GET ALL EXISTED PIPETE 
CREATE PROCEDURE SP_Nxs_GetAllPipeteExisted
AS
BEGIN

	SELECT Pipete_Name
	FROM M_Pipete_Mapping
	ORDER BY Pipete_Name ASC

END



--CREATE TABLE Examples(
--ID INTEGER IDENTITY(1,1),
--NAME VARCHAR(MAX)
--)

--SELECT * FROM EXAMPLES

--INSERT INTO Examples 
--OUTPUT Inserted.Name
--VALUES('Reze')

--DECLARE @IDS AS INTEGER
--SET @IDS = SCOPE_IDENTITY()
--SELECT @IDS


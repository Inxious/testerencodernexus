
USE NexusDB;

CREATE TABLE M_Pipete_Mapping (
	Pipete_ID INTEGER IDENTITY(1,1) NOT NULL,
	Pipete_Name VARCHAR(250) NOT NULL,
	Pipete_ConfigSet INTEGER NOT NULL,
	PRIMARY KEY(Pipete_ID),
	CONSTRAINT FK_PipeteConfigSet FOREIGN KEY (Pipete_ConfigSet)
	REFERENCES M_ConfigurasiHeader(MC_HeaderID)
)

ALTER TABLE M_Pipete_Mapping
ADD Pipete_UninstallConfig INTEGER


CREATE TABLE M_KoordinatGroup (
	GroupID INTEGER IDENTITY(1,1) NOT NULL,
	Group_Name VARCHAR(250) NOT NULL,
	Note TEXT,
	PRIMARY KEY(GroupID)
)


BEGIN TRAN
CREATE TABLE M_OffsetMappingHeader (
	Ofmap_HeaderID INTEGER IDENTITY(1,1) NOT NULL,
	Pipete_ID INTEGER NOT NULL,
	Koor_Group INTEGER NOT NULL,
	PRIMARY KEY (Ofmap_HeaderID),
	CONSTRAINT FK_CorespondingPipete FOREIGN KEY (Pipete_ID)
	REFERENCES M_Pipete_Mapping(Pipete_ID),
	CONSTRAINT FK_CorespondingGroup FOREIGN KEY (Koor_Group)
	REFERENCES M_KoordinatGroup(GroupID)
)

ALTER TABLE M_Motor ADD CONSTRAINT PK_MotorID PRIMARY KEY(MotorID)
CREATE TABLE M_OffsetMappingDetail (
	Ofmap_DetailID INTEGER IDENTITY(1,1) NOT NULL,
	Ofmap_HeaderID INTEGER NOT NULL,
	Motor_Used INTEGER NOT NULL,
	Offset_Value FLOAT NOT NULL,
	PRIMARY KEY (Ofmap_DetailID),
	CONSTRAINT FK_OfmapHeadDetail FOREIGN KEY (Ofmap_HeaderID)
	REFERENCES M_OffsetMappingHeader(Ofmap_HeaderID),
	CONSTRAINT FK_CorespondingMotor FOREIGN KEY (Motor_Used)
	REFERENCES M_Motor(MotorID)
)

BEGIN TRAN
-- ALTER M_Koordinat ADD Koor_Group
ALTER TABLE M_Koordinat ADD Koor_Group CHAR(50) NULL;
COMMIT
SELECT * FROM M_Koordinat

--SELECT *
--FROM M_Motor
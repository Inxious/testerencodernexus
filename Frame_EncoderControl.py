# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.dataview


###########################################################################
## Class MyFrame1
###########################################################################

class MyFrame1(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition,
                          size=wx.Size(945, 691), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOWFRAME))

        fgSizer1 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer1.SetFlexibleDirection(wx.BOTH)
        fgSizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer2 = wx.FlexGridSizer(1, 3, 0, 0)
        fgSizer2.SetFlexibleDirection(wx.BOTH)
        fgSizer2.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_panel2 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel2.SetForegroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOWFRAME))
        self.m_panel2.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_INACTIVECAPTION))

        fgSizer3 = wx.FlexGridSizer(4, 1, 0, 0)
        fgSizer3.SetFlexibleDirection(wx.BOTH)
        fgSizer3.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText1 = wx.StaticText(self.m_panel2, wx.ID_ANY, u"Tester Encoder", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.m_staticText1.Wrap(-1)
        self.m_staticText1.SetFont(wx.Font(14, 72, 90, 92, False, "Rockwell"))
        self.m_staticText1.SetForegroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOWFRAME))

        fgSizer3.Add(self.m_staticText1, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.m_panel8 = wx.Panel(self.m_panel2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel8.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHTTEXT))

        fgSizer5 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer5.SetFlexibleDirection(wx.BOTH)
        fgSizer5.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText5 = wx.StaticText(self.m_panel8, wx.ID_ANY, u"Koneksi Motor", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.m_staticText5.Wrap(-1)
        self.m_staticText5.SetFont(wx.Font(11, 74, 90, 92, False, "Arial"))

        fgSizer5.Add(self.m_staticText5, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        gbSizer11 = wx.GridBagSizer(0, 0)
        gbSizer11.SetFlexibleDirection(wx.BOTH)
        gbSizer11.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText21 = wx.StaticText(self.m_panel8, wx.ID_ANY, u"COM Port", wx.DefaultPosition, wx.DefaultSize,
                                            0)
        self.m_staticText21.Wrap(-1)
        gbSizer11.Add(self.m_staticText21, wx.GBPosition(0, 0), wx.GBSpan(1, 1), wx.ALL, 5)

        Cmb_TEnd_MotorPortChoices = []
        self.Cmb_TEnd_MotorPort = wx.ComboBox(self.m_panel8, wx.ID_ANY, u"Port List", wx.DefaultPosition,
                                              wx.Size(70, -1), Cmb_TEnd_MotorPortChoices, 0)
        gbSizer11.Add(self.Cmb_TEnd_MotorPort, wx.GBPosition(0, 1), wx.GBSpan(1, 1), wx.ALL, 5)

        self.m_staticText31 = wx.StaticText(self.m_panel8, wx.ID_ANY, u"BaudRate", wx.DefaultPosition, wx.DefaultSize,
                                            0)
        self.m_staticText31.Wrap(-1)
        gbSizer11.Add(self.m_staticText31, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL, 5)

        self.Txt_TEnd_MotorBaudrate = wx.TextCtrl(self.m_panel8, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                  wx.Size(70, -1), 0)
        gbSizer11.Add(self.Txt_TEnd_MotorBaudrate, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL, 5)

        self.Cmd_TEnd_MotorConn = wx.Button(self.m_panel8, wx.ID_ANY, u"Connect", wx.DefaultPosition, wx.Size(60, -1),
                                            0)
        gbSizer11.Add(self.Cmd_TEnd_MotorConn, wx.GBPosition(3, 0), wx.GBSpan(1, 1),
                      wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Cmd_TEnd_MotorDisConn = wx.Button(self.m_panel8, wx.ID_ANY, u"Disconnect", wx.DefaultPosition,
                                               wx.Size(70, -1), 0)
        gbSizer11.Add(self.Cmd_TEnd_MotorDisConn, wx.GBPosition(3, 1), wx.GBSpan(1, 1),
                      wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Lbl_TEnd_MotorConnStat = wx.StaticText(self.m_panel8, wx.ID_ANY, u"Not Connected", wx.DefaultPosition,
                                                    wx.DefaultSize, 0)
        self.Lbl_TEnd_MotorConnStat.Wrap(-1)
        gbSizer11.Add(self.Lbl_TEnd_MotorConnStat, wx.GBPosition(2, 0), wx.GBSpan(1, 2),
                      wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        fgSizer5.Add(gbSizer11, 1, wx.EXPAND, 5)

        self.m_panel8.SetSizer(fgSizer5)
        self.m_panel8.Layout()
        fgSizer5.Fit(self.m_panel8)
        fgSizer3.Add(self.m_panel8, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel9 = wx.Panel(self.m_panel2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel9.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHTTEXT))

        fgSizer6 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer6.SetFlexibleDirection(wx.BOTH)
        fgSizer6.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText10 = wx.StaticText(self.m_panel9, wx.ID_ANY, u"Koneksi Encoder", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText10.Wrap(-1)
        self.m_staticText10.SetFont(wx.Font(11, 74, 90, 92, False, "Arial"))

        fgSizer6.Add(self.m_staticText10, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        gbSizer1 = wx.GridBagSizer(0, 0)
        gbSizer1.SetFlexibleDirection(wx.BOTH)
        gbSizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText2 = wx.StaticText(self.m_panel9, wx.ID_ANY, u"COM Port", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText2.Wrap(-1)
        gbSizer1.Add(self.m_staticText2, wx.GBPosition(0, 0), wx.GBSpan(1, 1), wx.ALL, 5)

        Cmb_TEnd_EncoderPortChoices = []
        self.Cmb_TEnd_EncoderPort = wx.ComboBox(self.m_panel9, wx.ID_ANY, u"Port List", wx.DefaultPosition,
                                                wx.Size(70, -1), Cmb_TEnd_EncoderPortChoices, 0)
        gbSizer1.Add(self.Cmb_TEnd_EncoderPort, wx.GBPosition(0, 1), wx.GBSpan(1, 1), wx.ALL, 5)

        self.m_staticText3 = wx.StaticText(self.m_panel9, wx.ID_ANY, u"BaudRate", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText3.Wrap(-1)
        gbSizer1.Add(self.m_staticText3, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL, 5)

        self.Txt_TEnd_EncoderBaudrate = wx.TextCtrl(self.m_panel9, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                    wx.Size(70, -1), 0)
        gbSizer1.Add(self.Txt_TEnd_EncoderBaudrate, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL, 5)

        self.Cmd_TEnd_EncoderConn = wx.Button(self.m_panel9, wx.ID_ANY, u"Connect", wx.DefaultPosition, wx.Size(60, -1),
                                              0)
        gbSizer1.Add(self.Cmd_TEnd_EncoderConn, wx.GBPosition(3, 0), wx.GBSpan(1, 1),
                     wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Cmd_TEnd_EncoderDisConn = wx.Button(self.m_panel9, wx.ID_ANY, u"Disconnect", wx.DefaultPosition,
                                                 wx.Size(70, -1), 0)
        gbSizer1.Add(self.Cmd_TEnd_EncoderDisConn, wx.GBPosition(3, 1), wx.GBSpan(1, 1),
                     wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Lbl_TEnd_EncoderConnStat = wx.StaticText(self.m_panel9, wx.ID_ANY, u"Not Connected", wx.DefaultPosition,
                                                      wx.DefaultSize, 0)
        self.Lbl_TEnd_EncoderConnStat.Wrap(-1)
        gbSizer1.Add(self.Lbl_TEnd_EncoderConnStat, wx.GBPosition(2, 0), wx.GBSpan(1, 2),
                     wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        fgSizer6.Add(gbSizer1, 1, wx.EXPAND, 5)

        self.m_panel9.SetSizer(fgSizer6)
        self.m_panel9.Layout()
        fgSizer6.Fit(self.m_panel9)
        fgSizer3.Add(self.m_panel9, 1, wx.EXPAND | wx.ALL, 5)

        gbSizer8 = wx.GridBagSizer(0, 0)
        gbSizer8.SetFlexibleDirection(wx.BOTH)
        gbSizer8.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Cmd_TEnd_SetSpeed = wx.Button(self.m_panel2, wx.ID_ANY, u"Set Speed", wx.DefaultPosition, wx.DefaultSize,
                                           0)
        gbSizer8.Add(self.Cmd_TEnd_SetSpeed, wx.GBPosition(0, 0), wx.GBSpan(1, 1), wx.ALL, 5)

        #Encoder Persamaan
        self.Cmd_TEnd_SetPers = wx.Button(self.m_panel2, wx.ID_ANY, u"Set Persamaan", wx.DefaultPosition, wx.DefaultSize,
                                           0)
        gbSizer8.Add(self.Cmd_TEnd_SetPers, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL, 5)

        #Set Rumus Calculate Pulse
        # Encoder Persamaan
        self.Cmd_TEnd_SetRum = wx.Button(self.m_panel2, wx.ID_ANY, u"Set Rumus", wx.DefaultPosition,
                                          wx.DefaultSize,
                                          0)
        gbSizer8.Add(self.Cmd_TEnd_SetRum, wx.GBPosition(2, 0), wx.GBSpan(1, 1), wx.ALL, 5)

        # #Set Pipete Mapping
        # self.Cmd_TEnd_MapPipete = wx.Button(self.m_panel2, wx.ID_ANY, u"Map Pipete", wx.DefaultPosition,
        #                                  wx.DefaultSize,
        #                                  0)
        # gbSizer8.Add(self.Cmd_TEnd_MapPipete, wx.GBPosition(3, 0), wx.GBSpan(1, 1), wx.ALL, 5)
        #
        # # Set Koordinate Group
        # self.Cmd_TEnd_GroupKoordinate = wx.Button(self.m_panel2, wx.ID_ANY, u"Group Koordinat", wx.DefaultPosition,
        #                                     wx.DefaultSize,
        #                                     0)
        # gbSizer8.Add(self.Cmd_TEnd_GroupKoordinate, wx.GBPosition(4, 0), wx.GBSpan(1, 1), wx.ALL, 5)
        #
        # # Set Offset Group
        # self.Cmd_TEnd_GroupOffset = wx.Button(self.m_panel2, wx.ID_ANY, u"Set Offset", wx.DefaultPosition,
        #                                           wx.DefaultSize,
        #                                           0)
        # gbSizer8.Add(self.Cmd_TEnd_GroupOffset, wx.GBPosition(5, 0), wx.GBSpan(1, 1), wx.ALL, 5)

        fgSizer3.Add(gbSizer8, 1, wx.EXPAND, 5)

        self.m_panel2.SetSizer(fgSizer3)
        self.m_panel2.Layout()
        fgSizer3.Fit(self.m_panel2)
        fgSizer2.Add(self.m_panel2, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel3 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel3.SetForegroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOWFRAME))

        gSizer1 = wx.GridSizer(2, 2, 0, 0)

        self.m_panel4 = wx.Panel(self.m_panel3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel4.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_INACTIVECAPTION))

        fgSizer4 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer4.SetFlexibleDirection(wx.BOTH)
        fgSizer4.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText4 = wx.StaticText(self.m_panel4, wx.ID_ANY, u"Motor X", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText4.Wrap(-1)
        self.m_staticText4.SetFont(wx.Font(10, 74, 90, 92, False, "Arial"))

        fgSizer4.Add(self.m_staticText4, 0, wx.ALL, 5)

        self.m_panel10 = wx.Panel(self.m_panel4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel10.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHTTEXT))

        fgSizer7 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer7.SetFlexibleDirection(wx.BOTH)
        fgSizer7.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText11 = wx.StaticText(self.m_panel10, wx.ID_ANY, u"Moving", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText11.Wrap(-1)
        fgSizer7.Add(self.m_staticText11, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        gbSizer7 = wx.GridBagSizer(0, 0)
        gbSizer7.SetFlexibleDirection(wx.BOTH)
        gbSizer7.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Txt_TEnd_XMoveVal = wx.TextCtrl(self.m_panel10, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        gbSizer7.Add(self.Txt_TEnd_XMoveVal, wx.GBPosition(0, 0), wx.GBSpan(1, 2), wx.ALL | wx.EXPAND, 5)

        self.Cmd_TEnd_XMove = wx.Button(self.m_panel10, wx.ID_ANY, u"Move", wx.DefaultPosition, wx.Size(60, -1), 0)
        gbSizer7.Add(self.Cmd_TEnd_XMove, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL, 5)

        self.Cmd_TEnd_XHome = wx.Button(self.m_panel10, wx.ID_ANY, u"Home", wx.DefaultPosition, wx.Size(60, -1), 0)
        gbSizer7.Add(self.Cmd_TEnd_XHome, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL, 5)

        fgSizer7.Add(gbSizer7, 1, wx.EXPAND, 5)

        self.m_staticText13 = wx.StaticText(self.m_panel10, wx.ID_ANY, u"Encoder", wx.DefaultPosition, wx.DefaultSize,
                                            0)
        self.m_staticText13.Wrap(-1)
        fgSizer7.Add(self.m_staticText13, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        fgSizer16 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer16.SetFlexibleDirection(wx.BOTH)
        fgSizer16.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Txt_EncoderGOX = wx.TextCtrl(self.m_panel10, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                          wx.Size(-1, -1), 0)
        self.Txt_EncoderGOX.SetMinSize(wx.Size(70, -1))

        fgSizer16.Add(self.Txt_EncoderGOX, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Cmd_EncoderGOX = wx.Button(self.m_panel10, wx.ID_ANY, u"GO", wx.DefaultPosition, wx.DefaultSize, 0)
        self.Cmd_EncoderGOX.SetMinSize(wx.Size(40, -1))

        fgSizer16.Add(self.Cmd_EncoderGOX, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer7.Add(fgSizer16, 1, wx.EXPAND, 5)

        gSizer3 = wx.GridSizer(1, 2, 0, 0)

        self.Cmd_TEnd_XEncoderRead = wx.Button(self.m_panel10, wx.ID_ANY, u"READ", wx.DefaultPosition, wx.Size(60, -1),
                                               0)
        gSizer3.Add(self.Cmd_TEnd_XEncoderRead, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Cmd_TEnd_XEncoderHome = wx.Button(self.m_panel10, wx.ID_ANY, u"HOME", wx.DefaultPosition, wx.Size(60, -1),
                                               0)
        gSizer3.Add(self.Cmd_TEnd_XEncoderHome, 0, wx.ALL, 5)

        fgSizer7.Add(gSizer3, 1, wx.EXPAND, 5)

        self.m_panel10.SetSizer(fgSizer7)
        self.m_panel10.Layout()
        fgSizer7.Fit(self.m_panel10)
        fgSizer4.Add(self.m_panel10, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel4.SetSizer(fgSizer4)
        self.m_panel4.Layout()
        fgSizer4.Fit(self.m_panel4)
        gSizer1.Add(self.m_panel4, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel5 = wx.Panel(self.m_panel3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel5.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_INACTIVECAPTION))

        fgSizer41 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer41.SetFlexibleDirection(wx.BOTH)
        fgSizer41.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText41 = wx.StaticText(self.m_panel5, wx.ID_ANY, u"Motor Y", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText41.Wrap(-1)
        self.m_staticText41.SetFont(wx.Font(10, 74, 90, 92, False, "Arial"))

        fgSizer41.Add(self.m_staticText41, 0, wx.ALL, 5)

        self.m_panel101 = wx.Panel(self.m_panel5, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel101.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_BTNHIGHLIGHT))

        fgSizer71 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer71.SetFlexibleDirection(wx.BOTH)
        fgSizer71.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText111 = wx.StaticText(self.m_panel101, wx.ID_ANY, u"Moving", wx.DefaultPosition, wx.DefaultSize,
                                             0)
        self.m_staticText111.Wrap(-1)
        fgSizer71.Add(self.m_staticText111, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        gbSizer71 = wx.GridBagSizer(0, 0)
        gbSizer71.SetFlexibleDirection(wx.BOTH)
        gbSizer71.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Txt_TEnd_YMoveVal = wx.TextCtrl(self.m_panel101, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        gbSizer71.Add(self.Txt_TEnd_YMoveVal, wx.GBPosition(0, 0), wx.GBSpan(1, 2), wx.ALL | wx.EXPAND, 5)

        self.Cmd_TEnd_YMove = wx.Button(self.m_panel101, wx.ID_ANY, u"Move", wx.DefaultPosition, wx.DefaultSize, 0)
        self.Cmd_TEnd_YMove.SetMinSize(wx.Size(60, -1))

        gbSizer71.Add(self.Cmd_TEnd_YMove, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL, 5)

        self.Cmd_TEnd_YHome = wx.Button(self.m_panel101, wx.ID_ANY, u"Home", wx.DefaultPosition, wx.DefaultSize, 0)
        self.Cmd_TEnd_YHome.SetMinSize(wx.Size(60, -1))

        gbSizer71.Add(self.Cmd_TEnd_YHome, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL, 5)

        fgSizer71.Add(gbSizer71, 1, wx.EXPAND, 5)

        self.m_staticText131 = wx.StaticText(self.m_panel101, wx.ID_ANY, u"Encoder", wx.DefaultPosition, wx.DefaultSize,
                                             0)
        self.m_staticText131.Wrap(-1)
        fgSizer71.Add(self.m_staticText131, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        fgSizer161 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer161.SetFlexibleDirection(wx.BOTH)
        fgSizer161.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Txt_EncoderGOY = wx.TextCtrl(self.m_panel101, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                          wx.DefaultSize, 0)
        self.Txt_EncoderGOY.SetMinSize(wx.Size(70, -1))

        fgSizer161.Add(self.Txt_EncoderGOY, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Cmd_EncoderGOY = wx.Button(self.m_panel101, wx.ID_ANY, u"GO", wx.DefaultPosition, wx.DefaultSize, 0)
        self.Cmd_EncoderGOY.SetMinSize(wx.Size(40, -1))

        fgSizer161.Add(self.Cmd_EncoderGOY, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer71.Add(fgSizer161, 1, wx.EXPAND, 5)

        gSizer4 = wx.GridSizer(1, 2, 0, 0)

        self.Cmd_TEnd_YEncoderRead = wx.Button(self.m_panel101, wx.ID_ANY, u"READ", wx.DefaultPosition, wx.DefaultSize,
                                               0)
        self.Cmd_TEnd_YEncoderRead.SetMinSize(wx.Size(60, -1))

        gSizer4.Add(self.Cmd_TEnd_YEncoderRead, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Cmd_TEnd_YEncoderHome = wx.Button(self.m_panel101, wx.ID_ANY, u"HOME", wx.DefaultPosition, wx.DefaultSize,
                                               0)
        self.Cmd_TEnd_YEncoderHome.SetMinSize(wx.Size(60, -1))

        gSizer4.Add(self.Cmd_TEnd_YEncoderHome, 0, wx.ALL, 5)

        fgSizer71.Add(gSizer4, 1, wx.EXPAND, 5)

        self.m_panel101.SetSizer(fgSizer71)
        self.m_panel101.Layout()
        fgSizer71.Fit(self.m_panel101)
        fgSizer41.Add(self.m_panel101, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel5.SetSizer(fgSizer41)
        self.m_panel5.Layout()
        fgSizer41.Fit(self.m_panel5)
        gSizer1.Add(self.m_panel5, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel6 = wx.Panel(self.m_panel3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel6.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_INACTIVECAPTION))

        fgSizer42 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer42.SetFlexibleDirection(wx.BOTH)
        fgSizer42.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText42 = wx.StaticText(self.m_panel6, wx.ID_ANY, u"Motor Z", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText42.Wrap(-1)
        self.m_staticText42.SetFont(wx.Font(10, 74, 90, 92, False, "Arial"))

        fgSizer42.Add(self.m_staticText42, 0, wx.ALL, 5)

        self.m_panel102 = wx.Panel(self.m_panel6, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel102.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHTTEXT))

        fgSizer72 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer72.SetFlexibleDirection(wx.BOTH)
        fgSizer72.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText112 = wx.StaticText(self.m_panel102, wx.ID_ANY, u"Moving", wx.DefaultPosition, wx.DefaultSize,
                                             0)
        self.m_staticText112.Wrap(-1)
        fgSizer72.Add(self.m_staticText112, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        gbSizer72 = wx.GridBagSizer(0, 0)
        gbSizer72.SetFlexibleDirection(wx.BOTH)
        gbSizer72.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Txt_TEnd_ZMoveVal = wx.TextCtrl(self.m_panel102, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        gbSizer72.Add(self.Txt_TEnd_ZMoveVal, wx.GBPosition(0, 0), wx.GBSpan(1, 2), wx.ALL | wx.EXPAND, 5)

        self.Cmd_TEnd_ZMove = wx.Button(self.m_panel102, wx.ID_ANY, u"Move", wx.DefaultPosition, wx.DefaultSize, 0)
        self.Cmd_TEnd_ZMove.SetMinSize(wx.Size(60, -1))

        gbSizer72.Add(self.Cmd_TEnd_ZMove, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL, 5)

        self.Cmd_TEnd_ZHome = wx.Button(self.m_panel102, wx.ID_ANY, u"Home", wx.DefaultPosition, wx.DefaultSize, 0)
        self.Cmd_TEnd_ZHome.SetMinSize(wx.Size(60, -1))

        gbSizer72.Add(self.Cmd_TEnd_ZHome, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL, 5)

        fgSizer72.Add(gbSizer72, 1, wx.EXPAND, 5)

        self.m_staticText132 = wx.StaticText(self.m_panel102, wx.ID_ANY, u"Encoder", wx.DefaultPosition, wx.DefaultSize,
                                             0)
        self.m_staticText132.Wrap(-1)
        fgSizer72.Add(self.m_staticText132, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        fgSizer162 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer162.SetFlexibleDirection(wx.BOTH)
        fgSizer162.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Txt_EncoderGOZ = wx.TextCtrl(self.m_panel102, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                          wx.DefaultSize, 0)
        self.Txt_EncoderGOZ.SetMinSize(wx.Size(70, -1))

        fgSizer162.Add(self.Txt_EncoderGOZ, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Cmd_EncoderGOZ = wx.Button(self.m_panel102, wx.ID_ANY, u"GO", wx.DefaultPosition, wx.DefaultSize, 0)
        self.Cmd_EncoderGOZ.SetMinSize(wx.Size(40, -1))

        fgSizer162.Add(self.Cmd_EncoderGOZ, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer72.Add(fgSizer162, 1, wx.EXPAND, 5)

        gSizer5 = wx.GridSizer(1, 2, 0, 0)

        self.Cmd_TEnd_ZEncoderRead = wx.Button(self.m_panel102, wx.ID_ANY, u"READ", wx.DefaultPosition, wx.DefaultSize,
                                               0)
        self.Cmd_TEnd_ZEncoderRead.SetMinSize(wx.Size(60, -1))

        gSizer5.Add(self.Cmd_TEnd_ZEncoderRead, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Cmd_TEnd_ZEncoderHome = wx.Button(self.m_panel102, wx.ID_ANY, u"HOME", wx.DefaultPosition, wx.DefaultSize,
                                               0)
        self.Cmd_TEnd_ZEncoderHome.SetMinSize(wx.Size(60, -1))

        gSizer5.Add(self.Cmd_TEnd_ZEncoderHome, 0, wx.ALL, 5)

        fgSizer72.Add(gSizer5, 1, wx.EXPAND, 5)

        self.m_panel102.SetSizer(fgSizer72)
        self.m_panel102.Layout()
        fgSizer72.Fit(self.m_panel102)
        fgSizer42.Add(self.m_panel102, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel6.SetSizer(fgSizer42)
        self.m_panel6.Layout()
        fgSizer42.Fit(self.m_panel6)
        gSizer1.Add(self.m_panel6, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel7 = wx.Panel(self.m_panel3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel7.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_INACTIVECAPTION))

        fgSizer43 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer43.SetFlexibleDirection(wx.BOTH)
        fgSizer43.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText43 = wx.StaticText(self.m_panel7, wx.ID_ANY, u"Motor C", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText43.Wrap(-1)
        self.m_staticText43.SetFont(wx.Font(10, 74, 90, 92, False, "Arial"))

        fgSizer43.Add(self.m_staticText43, 0, wx.ALL, 5)

        self.m_panel103 = wx.Panel(self.m_panel7, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel103.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHTTEXT))

        fgSizer73 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer73.SetFlexibleDirection(wx.BOTH)
        fgSizer73.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText113 = wx.StaticText(self.m_panel103, wx.ID_ANY, u"Moving", wx.DefaultPosition, wx.DefaultSize,
                                             0)
        self.m_staticText113.Wrap(-1)
        fgSizer73.Add(self.m_staticText113, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        gbSizer73 = wx.GridBagSizer(0, 0)
        gbSizer73.SetFlexibleDirection(wx.BOTH)
        gbSizer73.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Txt_TEnd_CMoveVal = wx.TextCtrl(self.m_panel103, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        gbSizer73.Add(self.Txt_TEnd_CMoveVal, wx.GBPosition(0, 0), wx.GBSpan(1, 2), wx.ALL | wx.EXPAND, 5)

        self.Cmd_TEnd_CMove = wx.Button(self.m_panel103, wx.ID_ANY, u"Move", wx.DefaultPosition, wx.DefaultSize, 0)
        self.Cmd_TEnd_CMove.SetMinSize(wx.Size(60, -1))

        gbSizer73.Add(self.Cmd_TEnd_CMove, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL, 5)

        self.Cmd_TEnd_CHome = wx.Button(self.m_panel103, wx.ID_ANY, u"Home", wx.DefaultPosition, wx.DefaultSize, 0)
        self.Cmd_TEnd_CHome.SetMinSize(wx.Size(60, -1))

        gbSizer73.Add(self.Cmd_TEnd_CHome, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL, 5)

        fgSizer73.Add(gbSizer73, 1, wx.EXPAND, 5)

        self.m_staticText133 = wx.StaticText(self.m_panel103, wx.ID_ANY, u"Encoder", wx.DefaultPosition, wx.DefaultSize,
                                             0)
        self.m_staticText133.Wrap(-1)
        fgSizer73.Add(self.m_staticText133, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        fgSizer163 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer163.SetFlexibleDirection(wx.BOTH)
        fgSizer163.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Txt_EncoderGOC = wx.TextCtrl(self.m_panel103, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                          wx.DefaultSize, 0)
        self.Txt_EncoderGOC.SetMinSize(wx.Size(70, -1))

        fgSizer163.Add(self.Txt_EncoderGOC, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Cmd_EncoderGOC = wx.Button(self.m_panel103, wx.ID_ANY, u"GO", wx.DefaultPosition, wx.DefaultSize, 0)
        self.Cmd_EncoderGOC.SetMinSize(wx.Size(40, -1))

        fgSizer163.Add(self.Cmd_EncoderGOC, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer73.Add(fgSizer163, 1, wx.EXPAND, 5)

        gSizer6 = wx.GridSizer(1, 2, 0, 0)

        self.Cmd_TEnd_CEncoderRead = wx.Button(self.m_panel103, wx.ID_ANY, u"READ", wx.DefaultPosition, wx.DefaultSize,
                                               0)
        self.Cmd_TEnd_CEncoderRead.SetMinSize(wx.Size(60, -1))

        gSizer6.Add(self.Cmd_TEnd_CEncoderRead, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Cmd_TEnd_CEncoderHome = wx.Button(self.m_panel103, wx.ID_ANY, u"HOME", wx.DefaultPosition, wx.DefaultSize,
                                               0)
        self.Cmd_TEnd_CEncoderHome.SetMinSize(wx.Size(60, -1))

        gSizer6.Add(self.Cmd_TEnd_CEncoderHome, 0, wx.ALL, 5)

        fgSizer73.Add(gSizer6, 1, wx.EXPAND, 5)

        self.m_panel103.SetSizer(fgSizer73)
        self.m_panel103.Layout()
        fgSizer73.Fit(self.m_panel103)
        fgSizer43.Add(self.m_panel103, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel7.SetSizer(fgSizer43)
        self.m_panel7.Layout()
        fgSizer43.Fit(self.m_panel7)
        gSizer1.Add(self.m_panel7, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel3.SetSizer(gSizer1)
        self.m_panel3.Layout()
        gSizer1.Fit(self.m_panel3)
        fgSizer2.Add(self.m_panel3, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel14 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer24 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer24.SetFlexibleDirection(wx.BOTH)
        fgSizer24.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_panel15 = wx.Panel(self.m_panel14, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel15.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_INACTIVECAPTION))

        fgSizer25 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer25.SetFlexibleDirection(wx.BOTH)
        fgSizer25.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText24 = wx.StaticText(self.m_panel15, wx.ID_ANY, u"PROCESS", wx.DefaultPosition, wx.DefaultSize,
                                            0)
        self.m_staticText24.Wrap(-1)
        self.m_staticText24.SetFont(wx.Font(12, 74, 90, 92, False, "Arial"))

        fgSizer25.Add(self.m_staticText24, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.m_panel17 = wx.Panel(self.m_panel15, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel17.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHTTEXT))

        gbSizer74 = wx.GridBagSizer(0, 0)
        gbSizer74.SetFlexibleDirection(wx.BOTH)
        gbSizer74.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        gbSizer74.SetMinSize(wx.Size(405, -1))
        self.Lbl_List_Process = wx.StaticText(self.m_panel17, wx.ID_ANY, u"Proses List", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.Lbl_List_Process.Wrap(-1)
        gbSizer74.Add(self.Lbl_List_Process, wx.GBPosition(0, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        Cmb_ProcessListChoices = []
        self.Cmb_ProcessList = wx.ComboBox(self.m_panel17, wx.ID_ANY, u"~ Choose Process ~", wx.DefaultPosition,
                                           wx.Size(300, -1), Cmb_ProcessListChoices, wx.CB_DROPDOWN)
        self.Cmb_ProcessList.SetMinSize(wx.Size(300, -1))

        gbSizer74.Add(self.Cmb_ProcessList, wx.GBPosition(0, 1), wx.GBSpan(1, 2), wx.ALL, 5)

        self.Cmd_RunProcess = wx.Button(self.m_panel17, wx.ID_ANY, u"Run Process", wx.DefaultPosition, wx.DefaultSize,
                                        0)
        gbSizer74.Add(self.Cmd_RunProcess, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL, 5)

        self.Cmd_RefreshProcess = wx.Button(self.m_panel17, wx.ID_ANY, u"Refresh Process", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        gbSizer74.Add(self.Cmd_RefreshProcess, wx.GBPosition(1, 2), wx.GBSpan(1, 1), wx.ALL, 5)

        self.m_panel17.SetSizer(gbSizer74)
        self.m_panel17.Layout()
        gbSizer74.Fit(self.m_panel17)
        fgSizer25.Add(self.m_panel17, 1, wx.ALL | wx.EXPAND, 5)

        self.m_panel15.SetSizer(fgSizer25)
        self.m_panel15.Layout()
        fgSizer25.Fit(self.m_panel15)
        fgSizer24.Add(self.m_panel15, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel16 = wx.Panel(self.m_panel14, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel16.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_INACTIVECAPTION))

        fgSizer26 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer26.SetFlexibleDirection(wx.BOTH)
        fgSizer26.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText25 = wx.StaticText(self.m_panel16, wx.ID_ANY, u"CONFIGURASI", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText25.Wrap(-1)
        self.m_staticText25.SetFont(wx.Font(12, 74, 90, 92, False, "Arial"))

        fgSizer26.Add(self.m_staticText25, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.m_panel18 = wx.Panel(self.m_panel16, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel18.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHTTEXT))

        fgSizer27 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer27.SetFlexibleDirection(wx.BOTH)
        fgSizer27.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer28 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer28.SetFlexibleDirection(wx.BOTH)
        fgSizer28.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer28.SetMinSize(wx.Size(405, -1))
        self.Dv_ConfigurasiList = wx.dataview.DataViewListCtrl(self.m_panel18, wx.ID_ANY, wx.DefaultPosition,
                                                               wx.Size(300, 230),
                                                               wx.dataview.DV_HORIZ_RULES | wx.dataview.DV_MULTIPLE | wx.dataview.DV_ROW_LINES)
        self.Dv_ConfigurasiList.SetMinSize(wx.Size(300, 230))

        fgSizer28.Add(self.Dv_ConfigurasiList, 0, wx.ALL, 5)

        bSizer1 = wx.BoxSizer(wx.VERTICAL)

        self.Cmd_RunConfig = wx.Button(self.m_panel18, wx.ID_ANY, u"Run Config", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer1.Add(self.Cmd_RunConfig, 0, wx.ALL, 5)

        self.Cmd_RunLoop = wx.Button(self.m_panel18, wx.ID_ANY, u"Run By Loop", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer1.Add(self.Cmd_RunLoop, 0, wx.ALL, 5)

        self.Cmd_Clear = wx.Button(self.m_panel18, wx.ID_ANY, u"Clear", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer1.Add(self.Cmd_Clear, 0, wx.ALL, 5)

        self.Cmd_ListAll = wx.Button(self.m_panel18, wx.ID_ANY, u"List All", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer1.Add(self.Cmd_ListAll, 0, wx.ALL, 5)

        self.Cmd_CheckAll = wx.Button(self.m_panel18, wx.ID_ANY, u"Check All", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer1.Add(self.Cmd_CheckAll, 0, wx.ALL, 5)

        self.Cmd_UncheckAll = wx.Button(self.m_panel18, wx.ID_ANY, u"UnCheck All", wx.DefaultPosition, wx.DefaultSize,
                                        0)
        bSizer1.Add(self.Cmd_UncheckAll, 0, wx.ALL, 5)

        fgSizer28.Add(bSizer1, 1, wx.EXPAND, 5)

        fgSizer27.Add(fgSizer28, 1, wx.EXPAND, 5)

        fgSizer29 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer29.SetFlexibleDirection(wx.BOTH)
        fgSizer29.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        bSizer2 = wx.BoxSizer(wx.HORIZONTAL)

        self.Lbl_CheckedCount = wx.StaticText(self.m_panel18, wx.ID_ANY, u"Checked Count", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.Lbl_CheckedCount.Wrap(-1)
        bSizer2.Add(self.Lbl_CheckedCount, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_CheckedCount = wx.TextCtrl(self.m_panel18, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        bSizer2.Add(self.Txt_CheckedCount, 0, wx.ALL, 5)

        fgSizer29.Add(bSizer2, 1, wx.EXPAND, 5)

        bSizer3 = wx.BoxSizer(wx.HORIZONTAL)

        self.Lbl_LoopCount = wx.StaticText(self.m_panel18, wx.ID_ANY, u"Loop (Optional) ", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.Lbl_LoopCount.Wrap(-1)
        bSizer3.Add(self.Lbl_LoopCount, 0, wx.ALL, 5)

        self.Txt_LoopCount = wx.TextCtrl(self.m_panel18, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                         0)
        bSizer3.Add(self.Txt_LoopCount, 0, wx.ALL, 5)

        fgSizer29.Add(bSizer3, 1, wx.EXPAND, 5)

        fgSizer27.Add(fgSizer29, 1, wx.EXPAND, 5)

        self.m_panel18.SetSizer(fgSizer27)
        self.m_panel18.Layout()
        fgSizer27.Fit(self.m_panel18)
        fgSizer26.Add(self.m_panel18, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel16.SetSizer(fgSizer26)
        self.m_panel16.Layout()
        fgSizer26.Fit(self.m_panel16)
        fgSizer24.Add(self.m_panel16, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel14.SetSizer(fgSizer24)
        self.m_panel14.Layout()
        fgSizer24.Fit(self.m_panel14)
        fgSizer2.Add(self.m_panel14, 1, wx.EXPAND | wx.ALL, 5)

        fgSizer1.Add(fgSizer2, 1, wx.EXPAND, 5)

        self.m_panel1 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel1.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_INACTIVECAPTION))

        gSizer2 = wx.GridSizer(1, 2, 0, 0)

        fgSizer21 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer21.SetFlexibleDirection(wx.BOTH)
        fgSizer21.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText26 = wx.StaticText(self.m_panel1, wx.ID_ANY, u"Log Motor", wx.DefaultPosition, wx.DefaultSize,
                                            0)
        self.m_staticText26.Wrap(-1)
        self.m_staticText26.SetFont(wx.Font(12, 74, 90, 92, False, "Arial"))

        fgSizer21.Add(self.m_staticText26, 0, wx.ALL, 5)

        Lb_TEnd_LogMotorChoices = []
        self.Lb_TEnd_LogMotor = wx.ListBox(self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.Size(450, 120),
                                           Lb_TEnd_LogMotorChoices, 0)
        self.Lb_TEnd_LogMotor.SetMinSize(wx.Size(450, 120))

        fgSizer21.Add(self.Lb_TEnd_LogMotor, 0, wx.ALL, 5)

        gSizer2.Add(fgSizer21, 1, wx.EXPAND, 5)

        fgSizer22 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer22.SetFlexibleDirection(wx.BOTH)
        fgSizer22.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText27 = wx.StaticText(self.m_panel1, wx.ID_ANY, u"Log Encoder", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText27.Wrap(-1)
        self.m_staticText27.SetFont(wx.Font(12, 74, 90, 92, False, "Arial"))

        fgSizer22.Add(self.m_staticText27, 0, wx.ALL, 5)

        Lb_TEnd_LogEncoderChoices = []
        self.Lb_TEnd_LogEncoder = wx.ListBox(self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.Size(450, 120),
                                             Lb_TEnd_LogEncoderChoices, 0)
        self.Lb_TEnd_LogEncoder.SetMinSize(wx.Size(450, 120))

        fgSizer22.Add(self.Lb_TEnd_LogEncoder, 0, wx.ALL, 5)

        gSizer2.Add(fgSizer22, 1, wx.EXPAND, 5)

        self.m_panel1.SetSizer(gSizer2)
        self.m_panel1.Layout()
        gSizer2.Fit(self.m_panel1)
        fgSizer1.Add(self.m_panel1, 1, wx.EXPAND | wx.ALL, 5)

        self.SetSizer(fgSizer1)
        self.Layout()

        self.Centre(wx.BOTH)
        self.TEndStarter(420)
        self.StarterDataview()

        #Menu Bar
        self.Mb_Settings = wx.MenuBar(0)
        self.Menu_Config = wx.Menu()
        self.Mitem_CoorGroup = wx.MenuItem(self.Menu_Config, wx.ID_ANY, u"Coordinate Group", wx.EmptyString, wx.ITEM_NORMAL)
        self.Menu_Config.Append(self.Mitem_CoorGroup)

        self.Mitem_CoorOff = wx.MenuItem(self.Menu_Config, wx.ID_ANY, u"Coordinate Offset", wx.EmptyString, wx.ITEM_NORMAL)
        self.Menu_Config.Append(self.Mitem_CoorOff)

        self.Mitem_PipeteData = wx.MenuItem(self.Menu_Config, wx.ID_ANY, u"Pipete Data Install", wx.EmptyString,
                                            wx.ITEM_NORMAL)
        self.Menu_Config.Append(self.Mitem_PipeteData)

        self.Mitem_PipeteDataUninstall = wx.MenuItem(self.Menu_Config, wx.ID_ANY, u"Pipete Data Uninstall", wx.EmptyString,
                                            wx.ITEM_NORMAL)
        self.Menu_Config.Append(self.Mitem_PipeteDataUninstall)

        self.Mb_Settings.Append(self.Menu_Config, u"Configuration")

        self.SetMenuBar(self.Mb_Settings)


        self.Show()
        self.SetHeadString(name='PIPETE USED = NONE')



        # Connect Events
        self.Bind(wx.EVT_ACTIVATE, self.TEndStarter(0))
        self.Cmd_TEnd_MotorConn.Bind(wx.EVT_BUTTON, lambda x:self.TEndConnectSer(1,self.Cmb_TEnd_MotorPort.GetValue(),self.Txt_TEnd_MotorBaudrate.GetValue()))
        self.Cmd_TEnd_MotorDisConn.Bind(wx.EVT_BUTTON, lambda x:self.TEndDisConnectSer(1))
        self.Cmd_TEnd_EncoderConn.Bind(wx.EVT_BUTTON, lambda x:self.TEndConnectSer(2,self.Cmb_TEnd_EncoderPort.GetValue(),self.Txt_TEnd_EncoderBaudrate.GetValue()))
        self.Cmd_TEnd_EncoderDisConn.Bind(wx.EVT_BUTTON, lambda x:self.TEndDisConnectSer(2))
        self.Cmd_TEnd_XMove.Bind(wx.EVT_BUTTON, lambda x:self.TEndMoveMotor(1, self.Txt_TEnd_XMoveVal.GetValue()))
        self.Cmd_TEnd_XHome.Bind(wx.EVT_BUTTON, lambda x:self.TEndHomeMotor(1))
        self.Cmd_TEnd_XEncoderRead.Bind(wx.EVT_BUTTON, lambda x:self.TEndReadEncoder(1))
        self.Cmd_TEnd_XEncoderHome.Bind(wx.EVT_BUTTON, lambda x:self.TEndHomeEncoder(1))
        self.Cmd_TEnd_YMove.Bind(wx.EVT_BUTTON, lambda x:self.TEndMoveMotor(2, self.Txt_TEnd_YMoveVal.GetValue()))
        self.Cmd_TEnd_YHome.Bind(wx.EVT_BUTTON, lambda x:self.TEndHomeMotor(2))
        self.Cmd_TEnd_YEncoderRead.Bind(wx.EVT_BUTTON, lambda x:self.TEndReadEncoder(2))
        self.Cmd_TEnd_YEncoderHome.Bind(wx.EVT_BUTTON, lambda x:self.TEndHomeEncoder(2))
        self.Cmd_TEnd_ZMove.Bind(wx.EVT_BUTTON, lambda x:self.TEndMoveMotor(3, self.Txt_TEnd_ZMoveVal.GetValue()))
        self.Cmd_TEnd_ZHome.Bind(wx.EVT_BUTTON, lambda x:self.TEndHomeMotor(3))
        self.Cmd_TEnd_ZEncoderRead.Bind(wx.EVT_BUTTON, lambda x:self.TEndReadEncoder(3))
        self.Cmd_TEnd_ZEncoderHome.Bind(wx.EVT_BUTTON, lambda x:self.TEndHomeEncoder(3))
        self.Cmd_TEnd_CMove.Bind(wx.EVT_BUTTON, lambda x:self.TEndMoveMotor(4, self.Txt_TEnd_CMoveVal.GetValue()))
        self.Cmd_TEnd_CHome.Bind(wx.EVT_BUTTON, lambda x:self.TEndHomeMotor(4))
        self.Cmd_TEnd_CEncoderRead.Bind(wx.EVT_BUTTON, lambda x:self.TEndReadEncoder(4))
        self.Cmd_TEnd_CEncoderHome.Bind(wx.EVT_BUTTON, lambda x:self.TEndHomeEncoder(4))

        #Phase 2
        self.Cmd_EncoderGOX.Bind(wx.EVT_BUTTON, lambda x:self.GoEncoder(1,self.Txt_EncoderGOX.GetValue()))
        self.Cmd_EncoderGOY.Bind(wx.EVT_BUTTON, lambda x:self.GoEncoder(2,self.Txt_EncoderGOY.GetValue()))
        self.Cmd_EncoderGOZ.Bind(wx.EVT_BUTTON, lambda x:self.GoEncoder(3,self.Txt_EncoderGOZ.GetValue()))
        self.Cmd_EncoderGOC.Bind(wx.EVT_BUTTON, lambda x:self.GoEncoder(4,self.Txt_EncoderGOC.GetValue()))
        self.Bind(wx.dataview.EVT_DATAVIEW_ITEM_ACTIVATED, lambda x:self.CheckSelected(self.Dv_ConfigurasiList.GetSelections()), id=wx.ID_ANY)
        self.Bind(wx.dataview.EVT_DATAVIEW_ITEM_VALUE_CHANGED, lambda x:self.CheckCount(), id=wx.ID_ANY)
        self.Cmd_RunProcess.Bind(wx.EVT_BUTTON, lambda x:self.RunProcess())
        self.Cmd_RefreshProcess.Bind(wx.EVT_BUTTON, lambda x:self.RefreshProcess())
        self.Cmd_RunConfig.Bind(wx.EVT_BUTTON, lambda x:self.RunConfig(1))
        self.Cmd_RunLoop.Bind(wx.EVT_BUTTON, lambda x:self.RunConfig(2))
        self.Cmd_Clear.Bind(wx.EVT_BUTTON, lambda x:self.ClearConfig())
        self.Cmd_ListAll.Bind(wx.EVT_BUTTON, lambda x:self.LoadConfig())
        self.Cmd_CheckAll.Bind(wx.EVT_BUTTON, lambda x:self.CheckConfig())
        self.Cmd_UncheckAll.Bind(wx.EVT_BUTTON, lambda x:self.UncheckConfig())
        self.Cmb_ProcessList.Bind(wx.EVT_COMBOBOX, lambda x:self.ViewConfig())
        self.Cmd_TEnd_SetSpeed.Bind(wx.EVT_BUTTON, lambda x:self.SetSpeedos())
        self.Cmd_TEnd_SetPers.Bind(wx.EVT_BUTTON, lambda x: self.SetPersdos())
        self.Cmd_TEnd_SetRum.Bind(wx.EVT_BUTTON, lambda x: self.SetRumdos())

        #Add 2
        # self.Cmd_TEnd_MapPipete.Bind(wx.EVT_BUTTON, lambda x: self.SetMapPipetos())
        # self.Cmd_TEnd_GroupKoordinate.Bind(wx.EVT_BUTTON, lambda x: self.SetKoorGroupos())
        # self.Cmd_TEnd_GroupOffset.Bind(wx.EVT_BUTTON, lambda x: self.SetOffsetos())
        self.Bind(wx.EVT_MENU, self.SetMapPipetos, id=self.Mitem_PipeteData.GetId())
        self.Bind(wx.EVT_MENU, self.SetKoorGroupos, id=self.Mitem_CoorGroup.GetId())
        self.Bind(wx.EVT_MENU, self.SetOffsetos, id=self.Mitem_CoorOff.GetId())
        self.Bind(wx.EVT_MENU, self.SetMapPipetosUninstall, id=self.Mitem_PipeteDataUninstall.GetId())

    def __del__(self):
        pass



    #IMPORTED
    def FrameData(self, mode, **kwargs):
        if mode == 1: #Proces Listing

            sSQL = (" EXEC SP_Nxs_GetProses 1, ?")
            Values = ["NONE"]
            data = self.GETData(2, SQL=sSQL, value=Values)
            datalist = [x[0] for x in data]

            return (datalist)

        elif mode == 2: #All Listing

            sSQL = (" EXEC SP_Nxs_GetConfiguration 3, ?")
            Values = ["NONE"]
            data = self.GETData(2, SQL=sSQL, value=Values)
            datalist = [x[0] for x in data]

            return (datalist)


        elif mode == 3: #Selected Listing
            if kwargs['name'] == '' or len(kwargs['name']) == 0:
                return

            sSQL = (" EXEC SP_Nxs_ControlFrame_Data 1, ?")
            Values = [kwargs["name"]]
            data = self.GETData(2, SQL=sSQL, value = Values)
            datalist = [x[0] for x in data]



            #SET THE CURRENT ID OF SELECTED PROCESS TO ID_Process
            sSQL = (" EXEC SP_Nxs_ControlFrame_Data 2, ?")
            Values = [kwargs["name"]]
            data = self.GETData(2, SQL=sSQL, value=Values)
            data = [x[0] for x in data]
            if type(data) == list:
                data = data[0]
            if data not in (None,''):
                self.GUID_Proces = self.MDiagnostikNexus(2)
                self.ID_Proces = data
            else:
                self.GUID_Proces = self.MDiagnostikNexus(2)
                self.ID_Proces = ''

            return (datalist)

    def MFCheckUnCheck(self, mode):

        hasil = []
        for row in range(self.Dv_ConfigurasiList.GetItemCount()):
            allitem = []
            for col in range(self.Dv_ConfigurasiList.GetColumnCount()):
                if col == 0:
                    data = self.Dv_ConfigurasiList.GetToggleValue(row, col)
                else:
                    data = self.Dv_ConfigurasiList.GetValue(row, col)
                    data = data.encode()
                allitem.append(data)

            hasil.append(allitem)

        # Check
        if mode == 1:
            self.Dv_ConfigurasiList.DeleteAllItems()
            for itemrow in hasil:
                itemrow[0] = True
                self.Dv_ConfigurasiList.AppendItem(itemrow)

        # Uncheck
        elif mode == 2:
            self.Dv_ConfigurasiList.DeleteAllItems()
            for itemrow in hasil:
                itemrow[0] = False
                self.Dv_ConfigurasiList.AppendItem(itemrow)

    def ActionPasser(self, type, mode, **kwargs):
        if self.Event_Status == "ON":
            return

        if type == "Proces":
            if mode == "Proces Running":
                # == Wait Running Proces Warning
                if self.Proces_Step == "Running":
                    wx.MessageBox( "Warning : Proces Conflict!",
                                     "Ada Proces Yang Masih Berjalan\n" +
                                     "Mohon Tunggu Agar Proses Selesai\n")
                else:
                    # if True:
                    self.Proces_Data = kwargs["Data"]
                    self.Event_Types = "Proces"
                    self.Event_Status = "ON"

    #Phase 2
    def RunProcess(self):
        self.Proces_UnfinishedList = ''
        pass

    def RefreshProcess(self):
        self.Dv_ConfigurasiList.DeleteAllItems()
        self.Cmb_ProcessList.Clear()

        data = self.FrameData(1)
        if len(data) == 0:
            return

        for i in data:
            self.Cmb_ProcessList.AppendItems(i)

        self.Cmb_ProcessList.SetValue("~ Choose Process ~")
        self.CheckCount()

    def ViewConfig(self):
        self.Dv_ConfigurasiList.DeleteAllItems()

        name = self.Cmb_ProcessList.GetValue()
        data = self.FrameData(3,name=name)
        if len(data) == 0:
            return

        for i in data:
            hasil = []
            hasil.extend([True, i, "SELECTED"])
            self.Dv_ConfigurasiList.AppendItem(hasil)

        self.CheckCount()

    def GoEncoder(self, mode, togoes):
        idlist = {1:6,2:4,3:1,4:5}

        self.ManID = int(idlist[int(mode)])
        self.ManKoor = float(togoes)

        self.ManEvent_Type = 'GoEncoder'
        self.ManEvent_Status = 'ON'



    def ThreadGoEncoder(self, MotorID, Koordinat, **kwargs):
        # IN USE OF END CODER
        # print ('#---- ======== GO Encoder ======== ----#')
        self.TEndUpdateLog(5, 'GO Encoder')
        is_z = kwargs.get('z')

        if self.Features.get('Encoder') == True:

            if int(MotorID) in self.Encoder_List:
                # CALCULATE KOORDINAT [ PULSE TO GO ]
                #converted_koordinat = self.CConvertPulseToEncoder(1, MotorID, float(Koordinat))
                motorname = self.GetMotor(1, MotorID)
                togoes = float(Koordinat)

                # Offset
                # offset = self.GetMotor(6, MotorID)
                try:
                    offset = int(self.Offset_Encoder[MotorID])
                except Exception:
                    offset = 0
                if self.EncoderOffset_Type == 'PULSE':
                    offset_encoder = self.CConvertPulseToEncoder(1, MotorID, float(offset))
                else:
                    offset_encoder = offset
                togoes = float(togoes) + float(offset_encoder)
                # ---------------

                # Log
                final = "Encoder GO TO | Real Values | " + str(togoes)
                self.TEndUpdateLog(2, final)

                # # New Persamaan Encoder
                # togoes = self.CEncoderCalculatePersamaan(id=int(MotorID),val=togoes)
                # final = "Encoder GO TO | ReCalculate | " + str(togoes)
                # self.TEndUpdateLog(2, final)

                gap = self.CCalculateGapEncoder(1, MotorID, float(togoes))
                print ('GAPPP')
                print (gap)
                if gap == 0:
                    self.Not_Moving = True
                    self.TEndUpdateLog(2, '[ALREADY ON POSITION]')
                    return
                else:

                    # New Persamaan Encoder
                    # togoes = self.CEncoderCalculatePersamaan(id=int(MotorID),val=togoes)
                    # togoes = self.CEncoderCalculatePersamaan(id=int(MotorID), val=float(gap))
                    # final = "Encoder GO TO | ReCalculate | " + str(togoes)
                    # self.TEndUpdateLog(2, final)


                    #Revisi Persamaan
                    final1 = "Encoder GAP | REAL | " + str(gap)
                    self.TEndUpdateLog(2, final1)
                    gap = self.CEncoderCalculatePersamaan(id=int(MotorID), val=gap)
                    final2 = "Encoder GAP | PERSAMAAN | " + str(gap)
                    self.TEndUpdateLog(2, final2)

                    #ReCall Gap ( PERSAMAAN )
                    # gap = self.CCalculateGapEncoder2(1, MotorID, float(togoes))

                    self.Not_Moving = False
                    Koordinat = self.CCalculatePulseEncoder(1, MotorID, gap, z=is_z)
                    print('==================================//////=================')
                    now_pulse = self.GETNows(2, MotorID)
                    we_go = float(Koordinat) + float(now_pulse)
                    Koordinat = float(we_go)
                    print (str(togoes))
                    print (str(gap))
                    print (str(Koordinat))
                    print (str(now_pulse))
                    print (str(we_go))
                    print('==================================//////=================')
                # --------------------------

                # MOVE MOTOR
                if self.Not_Moving == False:
                    if self.RoboGO(MotorID, values=Koordinat, home=0, ex=False,serial=1) == "DONE":  # ToDo : SET RoboGO To Word
                        self.Koordinat_IS = Koordinat
                        return ("DONE")
                # --------------------------

                # READ MOTOR
                if self.Not_Moving == False:
                    self.UNO_Boards = 1
                    self.ReadAINO(1, 1, motorname)
                    self.UNO_Readings = "STARTED"

                    while not self.UNO_Readings == "ENDED":
                        pass
                # ---------------------------

                # VALIDATION
                if self.Features.get('Encoder') == True:
                    self.realsoon = self.timesoon
                    if self.Not_Moving == False and MotorID in self.Encoder_List:
                        togo = float(Koordinat)  # Get Coordinate Value
                        #converted_koordinat = self.CConvertPulseToEncoder(1, MotorID, float(togo))
                        #togo = converted_koordinat
                        status = self.CValidateEncoder(1, MotorID, togoes)
                        if status != "DONE":
                            recover = self.CEncoderMissRecovery(1, togoes, MotorID)
                            if recover != 'FINISH':
                                # ToDo : Fail Action
                                pass
                # ---------------------------
        self.TEndUpdateLog(5, 'GO Encoder END')

    def StarterDataview(self):
        self.Dv_ConfigurasiList.AppendToggleColumn('Run',width=50)
        self.Dv_ConfigurasiList.AppendTextColumn('Name',width=150)
        self.Dv_ConfigurasiList.AppendTextColumn('Status',width=100)


    def CheckSelected(self, selected):
        print selected
        for item in selected:
            rows = self.Dv_ConfigurasiList.ItemToRow(item)

            value = self.Dv_ConfigurasiList.GetToggleValue(rows, 0)
            print (value)

            if value == False:
                self.Dv_ConfigurasiList.SetValue(True,rows,0)
                hasil = "SELECTED"
            elif value == True:
                self.Dv_ConfigurasiList.SetValue(False, rows, 0)
                hasil = "PENDING"

            self.Dv_ConfigurasiList.SetValue(hasil, rows, 2)

        #Count
        self.CheckCount()

    def RunConfig(self, mode):
        # -- Recording Data TO Array
        self.Is_All = False
        rows = self.Dv_ConfigurasiList.GetItemCount()
        cols = self.Dv_ConfigurasiList.GetColumnCount()
        hasil = []
        for row in range(rows):
            allitem = []
            for col in range(cols):
                if col == 0:
                    data = self.Dv_ConfigurasiList.GetToggleValue(row, col)
                else:
                    data = self.Dv_ConfigurasiList.GetValue(row, col)
                    data = data.encode()
                allitem.append(data)

            hasil.append(allitem)

        Empty = True
        print ("========================================================")
        for value in hasil:
            if (True in value) == True or ("True" in value) == True:
                print value
                Empty = False
        print ("========================================================")
        print (Empty)
        if Empty == True:
            wx.MessageBox("Warning : None Has Selected!", "Belum Ada Gerakan Yang Dipilih")

        elif Empty == False:

            # NOT LOOP
            if mode == 1:
                self.LoopCount = 1
                self.ActionPasser("Proces", "Proces Running", Data=hasil)
            elif mode == 2:
                Loop = self.Txt_LoopCount.GetValue()
                if Loop in ('',None,):
                    return
                else:
                    try:
                        Loop = int(Loop)
                    except Exception as e:
                        return
                self.LoopCount = Loop
                self.ActionPasser("Proces", "Proces Running", Data=hasil)

    def CheckCount(self):
        rows = self.Dv_ConfigurasiList.GetItemCount()
        hasil = []
        for row in range(rows):
            data = self.Dv_ConfigurasiList.GetToggleValue(row, 0)
            if data == True:
                hasil.append(data)
        count = len(hasil)
        self.Txt_CheckedCount.SetValue(str(count))
        #return (int(count))


    def ClearConfig(self):
        self.Dv_ConfigurasiList.DeleteAllItems()
        self.CheckCount()

    def LoadConfig(self):
        # Reset Cmb Process
        self.ID_Proces = ''
        self.GUID_Proces = self.MDiagnostikNexus(2)
        self.Cmb_ProcessList.SetValue('')

        self.Dv_ConfigurasiList.DeleteAllItems()

        data = self.FrameData(2)
        if len(data) == 0:
            return

        for i in data:
            hasil = []
            hasil.extend([False, i, "PENDING"])
            self.Dv_ConfigurasiList.AppendItem(hasil)

        self.CheckCount()

    def CheckConfig(self):
        self.MFCheckUnCheck(1)
        self.CheckCount()

    def UncheckConfig(self):
        self.MFCheckUnCheck(2)
        self.CheckCount()

    # Virtual event handlers, overide them in your derived class
    def TEndStarter(self, board):

        #Serial
        # self.Txt_TEnd_MotorBaudrate.SetValue('115200')
        # self.Txt_TEnd_EncoderBaudrate.SetValue('115200')
        self.Txt_TEnd_MotorBaudrate.SetValue(self._ConnectionSet['motor']['baudrate'])
        self.Txt_TEnd_EncoderBaudrate.SetValue(self._ConnectionSet['encoder']['baudrate'])

        #Frame
        self.RefreshProcess()

        if board == 1:
            self.TEndRefreshSerial(1)
            self.TEndRefreshSerial(2)

        elif board == 2:
            self.TEndRefreshSerial(1)
            self.TEndRefreshSerial(2)

        elif board == 420:
            list = self.CPortScan()
            if list == None:
                print  ("No Serial")
                return
            self.Cmb_TEnd_MotorPort.Clear()
            self.Cmb_TEnd_MotorPort.SetItems(list)

            try:
                curport = str(self.ser1.port)
            except Exception as e:
                curport = '~ choose port ~'

            self.Cmb_TEnd_MotorPort.SetValue(curport)
            self.Cmb_TEnd_EncoderPort.Clear()
            self.Cmb_TEnd_EncoderPort.SetItems(list)

            try:
                curport2 = str(self.ser2.port)
            except Exception as e:
                curport2 = '~ choose port ~'

            self.Cmb_TEnd_EncoderPort.SetValue(curport2)

    def TEndRefreshSerial(self, mode):
        if mode == 1:
            list = self.CPortScan()
            if list == None:
                print  ("No Serial")
                return
            self.Cmb_TEnd_MotorPort.Clear()
            self.Cmb_TEnd_MotorPort.SetItems(list)

            try:
                curport = str(self.ser1.port)
            except Exception as e:
                curport = '~ choose port ~'

            self.Cmb_TEnd_MotorPort.SetValue(curport)

        elif mode == 2:
            list = self.CPortScan()
            if list == None:
                print  ("No Serial")
                return
            self.Cmb_TEnd_EncoderPort.Clear()
            self.Cmb_TEnd_EncoderPort.SetItems(list)

            try:
                curport2 = str(self.ser2.port)
            except Exception as e:
                curport2 = '~ choose port ~'

            self.Cmb_TEnd_EncoderPort.SetValue(curport2)


    def TEndConnectSer(self, board, com, brate):

        connect = self.CSerialCon(int(board), str(com), int(brate), 0)

        if connect == False:
            return

        if int(board) == 1:
            data = (" CONNECT " + str(self.ser1.port) + " , " + str(self.ser1.baudrate))
            self.TEndUpdateLog(1, data)
            self.Lbl_TEnd_MotorConnStat.SetLabel("CONNECTED")
            self.TEndRefreshSerial(1)

        elif int(board) == 2:
            data = (" CONNECT " + str(self.ser2.port) + " , " + str(self.ser2.baudrate))
            self.TEndUpdateLog(2, data)
            self.Lbl_TEnd_EncoderConnStat.SetLabel("CONNECTED")
            self.TEndRefreshSerial(2)

        else:
            return

        self.TEndStarter(board)


    def TEndDisConnectSer(self, board):

        disconnect = self.CSerialDisconn(board)

        if disconnect == False:
            return

        if board == 1:
            data = ("Motor DISCONNECT >> DONE")
            self.TEndUpdateLog(1, data)
            self.Lbl_TEnd_MotorConnStat.SetLabel("DISCONNECTED")
            self.TEndRefreshSerial(1)

        elif board == 2:
            data = ("Encoder DISCONNECT >> DONE")
            self.TEndUpdateLog(2, data)
            self.Lbl_TEnd_EncoderConnStat.SetLabel("DISCONNECTED")
            self.TEndRefreshSerial(2)

        else:
            return

        self.TEndStarter(board)


    def TEndMoveMotor(self, mode, value):
        print ('MOVE MOTOR')
        data = "NEXUS=MOVE_"
        motorlist = {1: 'X', 2: 'Y', 3: 'Z', 4: 'C'}
        idlist = {1:6,2:4,3:1,4:5}

        # Motor
        motor = motorlist[mode]

        # COMMAND
        command = data + motor + "#~" + motor + "=" + str(value) + "~$"

        self.TEndUpdateLog(3,command)
        self.CSendCommand(1, command)

        motorid = idlist[mode]
        self.Nows(1, motorid, float(value))


    def TEndHomeMotor(self, mode):
        print ('HOME MOTOR')
        data = "NEXUS=HOME_"
        motorlist = {1: 'X', 2: 'Y', 3: 'Z', 4: 'C'}
        idlist = {1: 6, 2: 4, 3: 1, 4: 5}

        # Motor
        motor = motorlist[mode]

        # COMMAND
        command = data + motor + "#~$"

        self.TEndUpdateLog(3, command)
        self.CSendCommand(1, command)
        self.Nows(1, idlist[mode], 0)

        #AUTO HOME ENCODER
        if self.ManEvent_Status == 'ON':
            print ('Sedang ada Event Berjalan di Manual Event')
            print ('Event Pass')
            return

        if self.AutoHome == True:
            self.ManID = idlist[mode]
            self.UNO_Boards = 1
            self.ManEvent_Type = 'WaitHome'
            self.ManEvent_Status = 'ON'


    def TEndReadEncoder(self, mode):
        print ('READ ENCODER')
        data = "NEXUS=ENCODER_"
        motorlist = {1: 'X', 2: 'Y', 3: 'Z', 4: 'C'}
        idlist = {1: 6, 2: 4, 3: 1, 4: 5}

        # Motor
        motor = motorlist[mode]

        # COMMAND
        command = data + motor + "#~$"
        self.TEndUpdateLog(4, command)
        self.CSendCommand(2, command)


    def TEndHomeEncoder(self, mode):
        print ('HOME ENCODER')
        data = "NEXUS=HOME_ENCODER_"
        motorlist = {1:'X',2:'Y',3:'Z',4:'C'}
        idlist = {1: 6, 2: 4, 3: 1, 4: 5}

        #Motor
        motor = motorlist[mode]

        # COMMAND
        command = data + motor + "#~$"
        self.TEndUpdateLog(4, command)
        self.CSendCommand(2, command)

    def TEndUpdateLog(self, mode, data):
        print ("NEW LOG")
        # ----- FEEDBACK ----
        if mode == 1:
            vLenData = int(self.Lb_TEnd_LogMotor.GetCount())
            #Limit Them Datas
            if vLenData > 250:
                item = self.Lb_TEnd_LogMotor.GetItems()
                cutted_item = item[250:]
                self.Lb_TEnd_LogMotor.SetItems(cutted_item)
            self.Lb_TEnd_LogMotor.AppendItems("Motor >> " + str(data))
            vLenData = int(self.Lb_TEnd_LogMotor.GetCount())
            self.Lb_TEnd_LogMotor.SetSelection(vLenData - 1)

        elif mode == 2:
            vLenData = int(self.Lb_TEnd_LogEncoder.GetCount())
            #Limit Them Datas
            if vLenData > 250:
                item = self.Lb_TEnd_LogEncoder.GetItems()
                cutted_item = item[250:]
                self.Lb_TEnd_LogEncoder.SetItems(cutted_item)
            self.Lb_TEnd_LogEncoder.AppendItems("Encoder >> " + str(data))
            vLenData = int(self.Lb_TEnd_LogEncoder.GetCount())
            self.Lb_TEnd_LogEncoder.SetSelection(vLenData - 1)
        # --------------------------------

        # ------ COMMAND -------
        elif mode == 3:
            vLenData = int(self.Lb_TEnd_LogMotor.GetCount())
            #Limit Them Datas
            if vLenData > 250:
                item = self.Lb_TEnd_LogMotor.GetItems()
                cutted_item = item[250:]
                self.Lb_TEnd_LogMotor.SetItems(cutted_item)
            self.Lb_TEnd_LogMotor.AppendItems("COMMAND >> " + str(data))
            vLenData = int(self.Lb_TEnd_LogMotor.GetCount())
            self.Lb_TEnd_LogMotor.SetSelection(vLenData - 1)

        elif mode == 4:
            vLenData = int(self.Lb_TEnd_LogEncoder.GetCount())
            #Limit Them Datas
            if vLenData > 250:
                item = self.Lb_TEnd_LogEncoder.GetItems()
                cutted_item = item[250:]
                self.Lb_TEnd_LogEncoder.SetItems(cutted_item)
            self.Lb_TEnd_LogEncoder.AppendItems("COMMAND >> " + str(data))
            vLenData = int(self.Lb_TEnd_LogEncoder.GetCount())
            self.Lb_TEnd_LogEncoder.SetSelection(vLenData - 1)
        #-----------------------------------

        # ALL
        elif mode == 5:
            vLenData1 = int(self.Lb_TEnd_LogMotor.GetCount())
            vLenData2 = int(self.Lb_TEnd_LogEncoder.GetCount())

            #Motor
            if vLenData1 > 250:
                item1 = self.Lb_TEnd_LogMotor.GetItems()
                cutted_item1 = item1[250:]
                self.Lb_TEnd_LogMotor.SetItems(cutted_item1)
            self.Lb_TEnd_LogMotor.AppendItems("# ======== " + str(data) + " ======== #")
            vLenData1 = int(self.Lb_TEnd_LogMotor.GetCount())
            self.Lb_TEnd_LogMotor.SetSelection(vLenData1 - 1)

            #Encoder
            if vLenData2 > 250:
                item2 = self.Lb_TEnd_LogEncoder.GetItems()
                cutted_item2 = item2[250:]
                self.Lb_TEnd_LogEncoder.SetItems(cutted_item2)
            self.Lb_TEnd_LogEncoder.AppendItems("# ======== " + str(data) + " ======== #")
            vLenData2 = int(self.Lb_TEnd_LogEncoder.GetCount())
            self.Lb_TEnd_LogEncoder.SetSelection(vLenData2 - 1)

        #INFO
        elif mode == 6:
            vLenData = int(self.Lb_TEnd_LogMotor.GetCount())
            # Limit Them Datas
            if vLenData > 250:
                item = self.Lb_TEnd_LogMotor.GetItems()
                cutted_item = item[250:]
                self.Lb_TEnd_LogMotor.SetItems(cutted_item)
            self.Lb_TEnd_LogMotor.AppendItems("INFO >> " + str(data))
            vLenData = int(self.Lb_TEnd_LogMotor.GetCount())
            self.Lb_TEnd_LogMotor.SetSelection(vLenData - 1)
            
        elif mode == 7:
            vLenData = int(self.Lb_TEnd_LogEncoder.GetCount())
            # Limit Them Datas
            if vLenData > 250:
                item = self.Lb_TEnd_LogEncoder.GetItems()
                cutted_item = item[250:]
                self.Lb_TEnd_LogEncoder.SetItems(cutted_item)
            self.Lb_TEnd_LogEncoder.AppendItems("INFO >> " + str(data))
            vLenData = int(self.Lb_TEnd_LogEncoder.GetCount())
            self.Lb_TEnd_LogEncoder.SetSelection(vLenData - 1)

    def SetHeadString(self, name):
        self.SetTitle(str(name))













import wx
from wx.lib.pubsub import setuparg1
from wx.lib.pubsub import setupkwargs
# from wx.lib.pubsub import pub
from pubsub import pub

import datetime
import serial
import time
import threading
import multiprocessing
import ConfigParser
import sys
import os
from platform import system as system_name  # Returns the system/OS name
from subprocess import call   as system_call  # Execute a shell command
import math


#APPEND CURR DIRECTORY TO SYSd
sys.path.append(os.getcwd())


#MODULE FILE
import Thread_Master
import Model


# Implementing MainFrame
class Controller2(Model.Model, Thread_Master.ThreadsPasser):

    def __init__(self):


        #IMPORT SETTING
        # LOAD DATABASE SETTING FROM CONFIG.txt
        config = ConfigParser.ConfigParser()
        config.readfp(open(r'config.txt'))

        self.X_TrueSpeed = config.get('Motor Configuration', 'X_Speed')
        self.Y_TrueSpeed = config.get('Motor Configuration', 'Y_Speed')

        #IMPORT SETTING
        # LOAD DATABASE SETTING FROM CONFIG.txt
        config = ConfigParser.ConfigParser()
        config.readfp(open(r'config.txt'))

        motordat = {'baudrate':None,
                    'com':None,
                    'connect':False}

        encodedat = {'baudrate':None,
                    'com':None,
                    'connect':False}

        self._ConnectionData = {'motor':motordat,
                                'encoder':encodedat}

        brate_m = config.get('Serial Configuration', 'motor_baudrate')
        com_m = config.get('Serial Configuration', 'motor_baudrate')

        brate_e = config.get('Serial Configuration', 'encoder_baudrate')
        com_e = config.get('Serial Configuration', 'encoder_baudrate')

        motorset = {'baudrate':brate_m,
                   'com':com_m}

        encodeset = {'baudrate':brate_e,
                    'com':com_e}

        self._ConnectionSet = {'motor':motorset,
                               'encoder':encodeset}

        self.X_TrueSpeed = config.get('Motor Configuration', 'X_Speed')
        self.Y_TrueSpeed = config.get('Motor Configuration', 'Y_Speed')
        self.Z_TrueSpeed = config.get('Motor Configuration', 'Z_Speed')
        self.A_TrueSpeed = config.get('Motor Configuration', 'A_Speed')
        self.B_TrueSpeed = config.get('Motor Configuration', 'B_Speed')
        self.C_TrueSpeed = config.get('Motor Configuration', 'C_Speed')
        self.R_TrueSpeed = config.get('Motor Configuration', 'R_Speed')

        self.X_TrueAccel = config.get('Motor Configuration', 'X_Accel')
        self.Y_TrueAccel = config.get('Motor Configuration', 'Y_Accel')
        self.Z_TrueAccel = config.get('Motor Configuration', 'Z_Accel')
        self.A_TrueAccel = config.get('Motor Configuration', 'A_Accel')
        self.B_TrueAccel = config.get('Motor Configuration', 'B_Accel')
        self.C_TrueAccel = config.get('Motor Configuration', 'C_Accel')
        self.R_TrueAccel = config.get('Motor Configuration', 'R_Accel')





        #SERIAL SETTING
        #Mode
        # self.Serial_Mode = config.get('Serial Configuration', 'Serial_Mode')

        #COM PORT
        self.Cport_Motor = config.get('Serial Configuration', 'Motor_Port')
        # self.Cport_Heater = config.get('Serial Configuration', 'Heater_Port')
        # self.Cport_Camera = config.get('Serial Configuration', 'Camera_Port')
        self.Cport_Encoder = config.get('Serial Configuration', 'Encoder_Port')

        #BAUDRATE
        self.Brate_Motor = config.get('Serial Configuration', 'Motor_Baudrate')
        # self.Brate_Heater = config.get('Serial Configuration', 'Heater_Baudrate')
        # self.Brate_Camera = config.get('Serial Configuration', 'Camera_Baudrate')
        self.Brate_Encoder = config.get('Serial Configuration', 'Encoder_Baudrate')

        #LIMIT CAMERA LOOP
        self.RotateTrial_Count = 0
        self.RotateTrial_Limit = config.get('SetPipete Configuration', 'Main_RotateLimit')
        self.RotateError_Limit = config.get('SetPipete Configuration', 'Error_RotateLimit')
        self.ScanMoving_Value = config.get('SetPipete Configuration', 'AjustingRotateValue')

        #FEATURES
        self.Features = {}
        Encoder = int(config.get('Nexus Features Activation', 'Encoder'))
        Camera = int(config.get('Nexus Features Activation', 'CameraRecognition'))
        self.Features.update({'Encoder': bool(Encoder)})
        self.Features.update({'Camera': bool(Camera)})

        #SUHU
        self.SuhuCounter = 0
        self.ThreadList = []


        # MODUL INIT
        Model.Model.__init__(self)

        T_Master = threading.Thread(target=Thread_Master.ThreadsPasser.__init__, args=(self,),
                                    name="Thread Master")

        T_Master.daemon = True
        T_Master.start()
        self.ThreadList.append(T_Master)

        # SPEED INIT
        config = ConfigParser.ConfigParser()
        config.readfp(open(r'config.txt'))

        #Controller starter
        self.Starter(44)

        # ENCODER STARTER
        self.EncoderStarter()

    #Apply Setting
    #self.Settings('Speed')
    def Settings(self, mode):
        if mode == 'Speed':
            id = {'X': 6, 'Y': 4, 'Z': 1, 'A': 3, 'B': 2, 'C': 5, 'R': 17}
            for ids in id:
                if ids == 'X':
                    speeds = self.X_TrueSpeed
                    accels = self.X_TrueAccel
                if ids == 'Y':
                    speeds = self.Y_TrueSpeed
                    accels = self.Y_TrueAccel
                if ids == 'Z':
                    speeds = self.Z_TrueSpeed
                    accels = self.Z_TrueAccel
                if ids == 'A':
                    speeds = self.A_TrueSpeed
                    accels = self.A_TrueAccel
                if ids == 'B':
                    speeds = self.B_TrueSpeed
                    accels = self.B_TrueAccel
                if ids == 'C':
                    speeds = self.C_TrueSpeed
                    accels = self.C_TrueAccel
                if ids == 'R':
                    speeds = self.R_TrueSpeed
                    accels = self.R_TrueAccel

    #CURRENT TIME
    def CurTime(self):
        return time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())

    # CURRENT TIME
    def CurTime2(self):
        return str(datetime.datetime.now())

    # ALSE CURRENT TIME BUT OBJECT
    def CCurTime3(self):
        return (datetime.datetime.now())

    #STARTER PACK ON CONTROLLER CLASS INIT
    def Starter(self, mode):

        self.XNow = 0
        self.YNow = 0
        self.ZNow = 0
        self.ANow = 0
        # self.BNow = 0
        self.MagNow = 0
        self.CNow = 0
        self.RNow = 0
        self.F1Now = str("")
        self.F2Now = str("")
        self.F3Now= str("")
        self.loop2 = ""
        self.MaxLimitTemp = 38
        self.MinLimitTemp = 36
        self.Camera_Connection = "OFF"
        self.ProcedureStart = False
        self.Rotataing = False
        self.SpeedChanged = False
        self.MotorIDBefore = 'None'
        self.SpeedBefore = 'None'
        self.AccelBefore = 'None'
        self.SerBefore = 'None'
        self.Proces_Selected = ''

        #Diagnostik V2
        self.GUID_Proces = ''
        self.GUID_Config = ''
        self.GUID_Motor = ''
        self.ID_Proces = ''
        self.ID_Config = ''
        self.ID_ConfigDetail = ''
        self.ID_Motor = ''

        self.X_Speed = ""
        self.Y_Speed = ""
        self.Z_Speed = ""
        self.A_Speed = ""
        self.B_Speed = ""
        self.C_Speed = ""
        self.R_Speed = ""

        self.X_Accel = ""
        self.Y_Accel = ""
        self.Z_Accel = ""
        self.A_Accel = ""
        self.B_Accel = ""
        self.C_Accel = ""
        self.R_Accel = ""


        #ORIGINAL SET SPEED
        #self.X_TrueSpeed = None
        #self.Y_TrueSpeed = None
        #self.Z_TrueSpeed = None
        #self.A_TrueSpeed = None
        #self.B_TrueSpeed = None
        #self.C_TrueSpeed = None
        #self.R_TrueSpeed = None

        # ORIGINAL SET ACCEL
        #self.X_TrueAccel = None
        #self.Y_TrueAccel = None
        #self.Z_TrueAccel = None
        #self.A_TrueAccel = None
        #self.B_TrueAccel = None
        #self.C_TrueAccel = None
        #self.R_TrueAccel = None

        #Thread STARTER
        self.Event_Status = "OFF"
        if mode == 0:
            self.Board1_Connection = "OFF"
            self.Board2_Connection = "OFF"
            self.Board3_Connection = "OFF"
            self.Encoder_Connection = "OFF"
        elif mode == 1:
            self.Board1_Connection = "OFF"
        elif mode == 2:
            self.Board2_Connection = "OFF"
            self.MaxLimitTemp = 38
            self.MinLimitTemp = 36
        elif mode == 3:
            self.Board3_Connection = "OFF"
        elif mode == 4:
            self.Encoder_Connection = "OFF"

    def EncoderStarter(self):
        # Encoder
        # ToDo: Get Data On Dbase
        # self.Encoder_List = {1:'MOTOR_Z',4:'MOTOR_Y',5:'MOTOR_C',6:'MOTOR_X'}
        self.Encoder_List = self.MLoadStarterDataEncoder(1)
        print self.Encoder_List
        self.Encoder_Motor = ''
        self.Encoder_Feedback = ''


    # def HeaterStarter(self):
    #     self.Board2_Connection = "OFF"
    #     self.MaxLimitTemp = 38
    #     self.MinLimitTemp = 36

    # ========= KONEKSI ========
    def SerialConn(self, board, com, brate, timeout):

        # Ard DUE (Motor)
        if board == 1:
            self.Board1_Connection = "OFF"
            self.ser1 = serial.Serial()
            self.ser1.port = com
            self.ser1.baudrate = brate
            self.ser1.timeout = timeout
            if self.ser1.is_open == True:
                self.ser1.close()
            self.ser1.open()
            self.LbLog.AppendItems("BOARD 1 CONNECT >> " + str(self.ser1.port) + " , " + str(self.ser1.baudrate))
            print("GOOD")
            time.sleep(2)
            self.LblComNow1.SetLabel(self.ser1.port)
            self.LblStat1.SetLabel("CONNECTED")
            self.Starter(1)
            self.Board1_Connection = "ON"
            self.Board1_Reading = "OFF"
            self.PortList = serial_scan()
            self.FrameAction(6, serial=self.PortList, portser=1)

        # Ard UNO (HEATER TEMP)
        elif board == 2:
            self.Board2_Connection = "OFF"
            self.ser2 = serial.Serial()
            self.ser2.port = com
            self.ser2.baudrate = brate
            self.ser2.timeout = timeout
            if self.ser2.is_open == True:
                self.ser2.close()
            self.ser2.open()
            self.LbLog.AppendItems("BOARD 2 CONNECT >> " + str(self.ser2.port) + " , " + str(self.ser2.baudrate))
            print("GOOD")
            time.sleep(2)
            self.LblComNow2.SetLabel(self.ser2.port)
            self.LblStat2.SetLabel("CONNECTED")
            self.Starter(2)

            # ADD LOG
            self.LogSuhuRt = {}
            self.LogRtReach = False
            self.CurrentFileSuhu = self.MFCreateFile(str(self.CurTime()) + ' - ' + str(self.SuhuCounter))
            self.SuhuCounter += 1
            self.SaveLogSuhuActive = True
            self.SuhuReadCount = 1

            self.Ser2_Active = True
            self.Board2_Connection = "ON"
            self.Board2_Reading = "ON"
            self.PortList = serial_scan()
            self.FrameAction(6, serial=self.PortList, portser=2)
            self.FrameAction(8, )
            if self.Standby_Start:
                self.ActionPasser("Frame", "Temperature StandBy", )

        # Ard UNO [2 FUSION]
        elif board == 3:
            self.Board3_Connection = "OFF"
            self.ser3 = serial.Serial()
            self.ser3.port = com
            self.ser3.baudrate = brate
            self.ser3.timeout = timeout
            if self.ser3.is_open == True:
                self.ser3.close()
            self.ser3.open()
            self.LbLog.AppendItems("CAMERA CONNECT >> " + str(self.ser3.port) + " , " + str(self.ser3.baudrate))
            print("GOOD")
            time.sleep(2)
            self.LblComNow3.SetLabel(self.ser3.port)
            self.LblStat3.SetLabel("CONNECTED")
            #self.Starter(2)
            self.Ser3_Active = True
            self.Camera_Connection = "ON"
            self.Camera_Reading = "OFF"
            self.PortList = serial_scan()
            self.FrameAction(6, serial=self.PortList, portser=3)

        # Ard UNO [ENCODER]
        elif board == 4:
            self.Encoder_Connection = "OFF"
            self.ser2 = serial.Serial()
            self.ser2.port = com
            self.ser2.baudrate = brate
            self.ser2.timeout = timeout
            print (self.ser2)
            if self.ser2.is_open == True:
                self.ser2.close()
            self.ser2.open()
            self.LbLog.AppendItems("ENCODER CONNECT >> " + str(self.ser2.port) + " , " + str(self.ser2.baudrate))
            print("GOOD")
            time.sleep(2)
            self.LblComNow4.SetLabel(self.ser2.port)
            self.LblStat4.SetLabel("CONNECTED")
            self.Starter(4)
            self.Encoder_Connection = "ON"
            self.Encoder_Reading = "OFF"
            self.PortList = serial_scan()
            self.FrameAction(6, serial=self.PortList, portser=4)




    #====== CUT THE CONNECTION ========
    def SerialDisconn(self, board):

        if board == 1:
            if self.ser1.is_open == True:
                self.Board1_Connection = "OFF"
                self.Board1_Reading = "OFF"
                self.ser1.close()
                self.LbLog.AppendItems("BOARD 1 DISCONNECT >> DONE")
                self.Ser1_Active = False
                self.PortList = serial_scan()
                self.FrameAction(6, serial=self.PortList , portser=1)
                self.LblComNow1.SetLabel("-")
                self.LblStat1.SetLabel("-")

        elif board == 2:
            if self.ser2.is_open == True:
                self.ActionPasser("Frame", "Temperature StandBy Off", )
                self.Board2_Connection = "OFF"
                self.Board2_Reading = "OFF"
                if self.Cmd_SET_Temp.GetLabel() == 'STOP':
                    self.ActionPasser("Frame", "Temperature StandBy", )
                self.Ser2_Active = False
                self.SaveLogSuhuActive = False

                #Run Event
                #self.Event_Types = "Action"
                self.SerialConEvent_Types = 'Communication'
                self.SerialConEvent_Name = "Temperature"
                self.Event_Temperature = "Stop"
                self.SerialConEvent_Status = "ON"

                time.sleep(1)
                self.ser2.close()
                self.LbLog.AppendItems("BOARD 2 DISCONNECT >> DONE")
                self.PortList = serial_scan()
                self.FrameAction(6, serial=self.PortList, portser=2)
                self.LblComNow2.SetLabel("-")
                self.LblStat2.SetLabel("-")



        elif board == 3:
            if self.ser3.is_open == True:
                self.Board3_Connection = "OFF"
                self.Board3_Reading = "OFF"
                self.ser3.close()
                self.LbLog.AppendItems("CAMERA DISCONNECT >> DONE")
                self.Ser3_Active = False
                self.PortList = serial_scan()
                self.FrameAction(6, serial=self.PortList , portser=3)
                self.LblComNow3.SetLabel("-")
                self.LblStat3.SetLabel("-")

    def CSerialRecon(self,mode):
        if mode == 3:
            self.SerialDisconn(3)
            time.sleep(1)
            self.SerialConn(3, self.CmbPort3.GetValue(), self.CmbBRate3.GetValue(), 0)


    def CAutoConnect(self,mode):

        self.CmbPort1.SetValue(str(self.Cport_Motor))
        self.CmbPort2.SetValue(str(self.Cport_Heater))
        self.CmbPort3.SetValue(str(self.Cport_Camera))
        self.CmbPort4.SetValue(str(self.Cport_Encoder))

        self.CmbBRate1.SetValue(str(self.Brate_Motor))
        self.CmbBRate2.SetValue(str(self.Brate_Heater))
        self.CmbBRate3.SetValue(str(self.Brate_Camera))
        self.CmbBRate4.SetValue(str(self.Brate_Encoder))

        if mode == 1:
            self.SerialConn(1, self.CmbPort1.GetValue(), self.CmbBRate1.GetValue(), 0)
            self.SerialConn(2, self.CmbPort2.GetValue(), self.CmbBRate2.GetValue(), 0)
            self.SerialConn(3, self.CmbPort3.GetValue(), self.CmbBRate3.GetValue(), 0)
            self.SerialConn(4, self.CmbPort4.GetValue(), self.CmbBRate4.GetValue(), 0)


    #===================================================================================================================
    #                                           -- --- --- ROBOT COMMAND --- --- --
    #===================================================================================================================

    # -- MOTOR RECORD SAVING --
    # Save Current Value On Variable

    def CCameraCommand(self,mode):
        #START MOTION
        if mode == 1:
            try:
                self.ser3.write('START_MOTION' + '\n\r')
            except Exception as e:
                print e

        elif mode == 2:
            try:
                self.ser3.write('STOP_MOTION' + '\n\r')
            except Exception as e:
                print e
        elif mode == 3:
            try:
                self.ser3.write('END' + '\n\r')
            except Exception as e:
                print e


    def Nows(self, mode, motorid, values):
        motor = self.GetMotor(1,motorid) # ToDo : Make func GetMotor in Model

        # -- SAVE COORDINATE RECORD
        if mode == 1:
            if motor == "MOTOR_X":
                print('GO TO =>' + ' X = ' + str(values) + ' || ' +
                      ' FROM ' + ' || ' + 'X = ' + str(self.XNow) + '')
                self.XNow = values
            elif motor == "MOTOR_Y":
                print('GO TO =>' + ' Y = ' + str(values) + ' || ' +
                      ' FROM ' + ' || ' + 'Y = ' + str(self.YNow) + '')
                self.YNow = values
            elif motor == "MOTOR_Z":
                print('GO TO =>' + ' Z = ' + str(values) + ' || ' +
                      ' FROM ' + ' || ' + 'Z = ' + str(self.ZNow) + '')
                self.ZNow = values
            elif motor == "MOTOR_A":
                print('GO TO =>' + ' A = ' + str(values) + ' || ' +
                      ' FROM ' + ' || ' + 'A = ' + str(self.ANow) + '')
                self.ANow = values
            # elif motor == "MOTOR_B":
            #     print('GO TO =>' + ' B = ' + str(values) + ' || ' +
            #           ' FROM ' + ' || ' + 'B = ' + str(self.BNow) + '')
            #     self.BNow = values
            elif motor == "MAGNET":
                print('MAGNET =>' +str(values) + ' || ' +
                      ' FROM ' +  str(self.MagNow) + '')
                self.MagNow = values
            elif motor == "MOTOR_C":
                print('GO TO =>' + ' C = ' + str(values) + ' || ' +
                      ' FROM ' + ' || ' + 'C = ' + str(self.CNow) + '')
                self.CNow = values
            elif motor == "MOTOR_R":
                print('GO TO =>' + ' R = ' + str(values) + ' || ' +
                      ' FROM ' + ' || ' + 'R = ' + str(self.RNow) + '')
                self.RNow = values
            elif motor == "MOTOR_FLIP1":
                print('GO TO =>' + ' F1 = ' + str(values) + ' || ' +
                      ' FROM ' + ' || ' + 'F1 = ' + str(self.F1Now) + '')
                self.F1Now = values
            elif motor == "MOTOR_FLIP2":
                print('GO TO =>' + ' F2 = ' + str(values) + ' || ' +
                      ' FROM ' + ' || ' + 'F2 = ' + str(self.F2Now) + '')
                self.F2Now = values
            elif motor == "MOTOR_FLIP3":
                print('GO TO =>' + ' F3 = ' + str(values) + ' || ' +
                      ' FROM ' + ' || ' + 'F3 = ' + str(self.F3Now) + '')
                self.F3Now = values
            elif motor == "MOTOR_HEAT":
                if values == 1:
                    self.Lbl_MF_Temp_Status.SetLabel("ON")
                elif values == 0:
                    self.Lbl_MF_Temp_Status.SetLabel("OFF")

                self.HtNow = values
            elif motor == "MOTOR_BLOW":
                self.BlNow = values



        # -- SAVE SPEED RECORD
        elif mode == 2:
            if motor == "MOTOR_X":
                self.X_Speed = values
            elif motor == "MOTOR_Y":
                self.Y_Speed = values
            elif motor == "MOTOR_Z":
                self.Z_Speed = values
            elif motor == "MOTOR_A":
                self.A_Speed = values
            # elif motor == "MOTOR_B":
            #     self.B_Speed = values
            elif motor == "MOTOR_C":
                self.C_Speed = values
            elif motor == "MOTOR_R":
                self.R_Speed = values

        # -- SAVE ACCEL RECORD
        elif mode == 3:
            if motor == "MOTOR_X":
                self.X_Accel = values
            elif motor == "MOTOR_Y":
                self.Y_Accel = values
            elif motor == "MOTOR_Z":
                self.Z_Accel = values
            elif motor == "MOTOR_A":
                self.A_Accel = values
            # elif motor == "MOTOR_B":
            #     self.B_Accel = values
            elif motor == "MOTOR_C":
                self.C_Accel = values
            elif motor == "MOTOR_R":
                self.R_Accel = values

        # -- SAVE TRUE SPEED RECORD
        elif mode == 4:
            if motor == "MOTOR_X":
                self.X_TrueSpeed = values
            elif motor == "MOTOR_Y":
                self.Y_TrueSpeed = values
            elif motor == "MOTOR_Z":
                self.Z_TrueSpeed = values
            elif motor == "MOTOR_A":
                self.A_TrueSpeed = values
            # elif motor == "MOTOR_B":
            #     self.B_TrueSpeed = values
            elif motor == "MOTOR_C":
                self.C_TrueSpeed = values
            elif motor == "MOTOR_R":
                self.R_TrueSpeed = values

        # -- SAVE TRUE ACCEL RECORD
        elif mode == 5:
            if motor == "MOTOR_X":
                self.X_TrueAccel = values
            elif motor == "MOTOR_Y":
                self.Y_TrueAccel = values
            elif motor == "MOTOR_Z":
                self.Z_TrueAccel = values
            elif motor == "MOTOR_A":
                self.A_TrueAccel = values
            # elif motor == "MOTOR_B":
            #     self.B_TrueAccel = values
            elif motor == "MOTOR_C":
                self.C_TrueAccel = values
            elif motor == "MOTOR_R":
                self.R_TrueAccel = values


    def CurrSpeedSET(self,mode,motor):
        if mode == 1:
            if motor == "MOTOR_X":
                return self.X_Speed
            elif motor == "MOTOR_Y":
                return self.Y_Speed
            elif motor == "MOTOR_Z":
                return self.Z_Speed
            elif motor == "MOTOR_A":
                return self.A_Speed
            # elif motor == "MOTOR_B":
            #     return self.B_Speed
            elif motor == "MOTOR_C":
                return self.C_Speed
            elif motor == "MOTOR_R":
                return self.R_Speed

        # -- SAVE ACCEL RECORD
        elif mode == 2:
            if motor == "MOTOR_X":
                return self.X_Accel
            elif motor == "MOTOR_Y":
                return self.Y_Accel
            elif motor == "MOTOR_Z":
                return self.Z_Accel
            elif motor == "MOTOR_A":
                return self.A_Accel
            # elif motor == "MOTOR_B":
            #     return self.B_Accel
            elif motor == "MOTOR_C":
                return self.C_Accel
            elif motor == "MOTOR_R":
                return self.R_Accel

    def GETNows(self, mode, motorid):
        motor = self.GetMotor(1,motorid) # ToDo : Make func GetMotor in Model

        if motor == "MOTOR_X":
            if mode == 1:
                return(self.X_Speed)
            elif mode == 2:
                return (self.XNow)
            elif mode == 3:
                return (self.X_Accel)
            elif mode == 4:
                return (self.X_TrueSpeed)
            elif mode == 5:
                return (self.X_TrueAccel)
            else:
                return (0)

        elif motor == "MOTOR_Y":
            if mode == 1:
                return (self.Y_Speed)
            elif mode == 2:
                return (self.YNow)
            elif mode == 3:
                return (self.Y_Accel)
            elif mode == 4:
                return (self.Y_TrueSpeed)
            elif mode == 5:
                return (self.Y_TrueAccel)
            else:
                return (0)

        elif motor == "MOTOR_Z":
            if mode == 1:
                return (self.Z_Speed)
            elif mode == 2:
                return (self.ZNow)
            elif mode == 3:
                return (self.Z_Accel)
            elif mode == 4:
                return (self.Z_TrueSpeed)
            elif mode == 5:
                return (self.Z_TrueAccel)
            else:
                return (0)

        elif motor == "MOTOR_A":
            if mode == 1:
                return (self.A_Speed)
            elif mode == 2:
                return (self.ANow)
            elif mode == 3:
                return (self.A_Accel)
            elif mode == 4:
                return (self.A_TrueSpeed)
            elif mode == 5:
                return (self.A_TrueAccel)
            else:
                return (0)

        elif motor == "MAGNET":
            # if mode == 1:
            #     return (self.B_Speed)
            # el
            if mode == 2:
                return (self.MagNow)
            # elif mode == 3:
            #     return (self.B_Accel)
            # elif mode == 4:
            #     return (self.B_TrueSpeed)
            # elif mode == 5:
            #     return (self.B_TrueAccel)
            else:
                return (0)

        elif motor == "MOTOR_C":
            if mode == 1:
                return (self.C_Speed)
            elif mode == 2:
                return (self.CNow)
            elif mode == 3:
                return (self.C_Accel)
            elif mode == 4:
                return (self.C_TrueSpeed)
            elif mode == 5:
                return (self.C_TrueAccel)
            else:
                return (0)

        elif motor == "MOTOR_R":
            if mode == 1:
                return (self.R_Speed)
            elif mode == 2:
                return (self.RNow)
            elif mode == 3:
                return (self.R_Accel)
            elif mode == 4:
                return (self.R_TrueSpeed)
            elif mode == 5:
                return (self.R_TrueAccel)
            else:
                return (0)

        else:
            return("NONE")
        #elif motor == "MOTOR_FLIP1":
        #elif motor == "MOTOR_FLIP2":
        #elif motor == "MOTOR_FLIP3":
        #elif motor == "MOTOR_HEAT":
        #elif motor == "MOTOR_BLOW":

    def RoboHEAT(self, mode):
        if mode == 0:
            print(self.ser2.is_open)
            data = (r'NEXUS=HEATER_OFF#')
            self.ser2.write(data + '~$' + '\r\n')
            print('HEATER OFF')
            self.Lbl_MF_Temp_Status.SetLabel('HEATER OFF')
            self.suhus = "off"

        elif mode == 1:
            print(self.ser2.is_open)
            data = (r'NEXUS=HEATER_ON#')
            self.ser2.write(data + '~$' + '\r\n')
            print('HEATER ON')
            self.Lbl_MF_Temp_Status.SetLabel('HEATER ON')
            self.suhus = "on"

    # -- MOTOR MOVEMENT COMMAND --
    def RoboGO(self, motorid, **kwargs):

        if self.GETNows(2,motorid) in (0,None,float(0),""):
            self.Nows(1,motorid,values=0)

        #PASS THE SAME KOOR MOVEMENT

        check = True

        if self.GETNows(2,motorid) in ("NONE",None):
            check = False

        if check == True:
            if float(self.GETNows(2,motorid)) == float(kwargs['values']) and kwargs['home'] == 0:
                self.Nows(1, motorid, values=float(kwargs['values']))
                final = (str(self.GetMotor(1,motorid)) + " [ALREADY ON POSITION] ")
                self.TEndUpdateLog(1, final)
                self.lastcommand =  (r'NEXUS=' + self.MoveCommand(motorid) + str(kwargs['values']))
                self.lastflag = self.GetMotor(7, motorid)
                return("DONE")
        elif check == False:
            pass

        #Info Moving
        if kwargs['home'] == 0:
            motorname = self.GetMotor(1, motorid)
            now_val = float(self.GETNows(2,motorid))
            togo_val = float(kwargs['values'])

            differ_val = float(togo_val - now_val)
            if differ_val < 0:
                indicator = '-'
            else:
                indicator = '+'

            final = str(motorname) + ' Moving || [' + str(indicator) + '] ' + str(differ_val)
            self.TEndUpdateLog(6, final)


        if kwargs['home'] == 0:
            if kwargs['ex'] == "HEAT" or kwargs['ex'] == "BLOW" or kwargs['ex'] == "MAGNET":
                final_value = 0
                #if int(kwargs['values']) == 0:
                #    data = (r'NEXUS=' + self.HomeCommand(motorid))
                if int(kwargs['values']) == 1:
                    data = (r'NEXUS=' + self.MoveCommand(motorid))
            else:
                print kwargs['values']
                #ToDo : Decide Use EnLine or MoPulse
                offset = self.GetMotor(6, motorid)
                #ToDo : With Encoder
                # IGNORE THE PULSE OFFSET CAUSE ENCODER ALREADY CALCULATE THAT
                if int(motorid) in self.Encoder_List:
                    #offset = self.CConvertPulseToEncoder( 1, motorid, float(offset))
                    offset = float(0)

                final_value = float(kwargs['values']) + float(offset)
                data = (r'NEXUS=' + str(self.MoveCommand(motorid)) + str(final_value))

            #self.LbLog.AppendItems("COMMAND >> " + str(data) + '~$')
            self.LogView(2, board=kwargs['serial'], line=data)

            data = (data + '~$' + '\n')
            data = data.encode()
            if kwargs['serial'] == 1:
                self.ser1.write(data + '~$' + '\n')
            elif kwargs['serial'] == 2:
                self.ser2.write(data + '~$' + '\n')

            if kwargs['ex'] == "HEAT":
                self.Lbl_MF_Temp_Status.SetLabel("ON")

            self.Nows(1, motorid,final_value)

        elif kwargs['home'] == 1:

            if kwargs['ex'] == "HEAT" or kwargs['ex'] == "BLOW" or kwargs['ex'] == "MAGNET":
                data = (r'NEXUS=' + self.HomeCommand(motorid))
            else:
                offset = self.GetMotor(6, motorid)
                final_value = float(kwargs['values']) + float(offset)
                data = (r'NEXUS=' + self.HomeCommand(motorid)) #+ str(final_value))

            #self.LbLog.AppendItems("COMMAND >> " + str(data) + '~$')
            self.LogView(2, board=kwargs['serial'], line=data)

            data = (data + '~$' + '\n')
            data = data.encode()
            if kwargs['serial'] == 1:
                self.ser1.write(data)
            elif kwargs['serial'] == 2:
                self.ser2.write(data)


            if kwargs['ex'] == "HEAT":
                self.Lbl_MF_Temp_Status.SetLabel("OFF")

            self.Nows(1, motorid, 0)

        try:
            data
        except Exception as e:
            pass
        else:
            self.lastcommand = data

        #print 'KKKKKKKKKKKKKKKKKKKKKKKKKKKKK ' + str(kwargs['serial']) + ' ' + str(data)

    #-- MOTOR SPEED COMMAND --
    def RoboSPEED(self, motorid, speed, **kwargs):

        if self.GETNows(1,motorid) == speed:
            return

        data = (r'NEXUS=' + str(self.SpeedCommand(motorid)) + str(speed))
        print data
        #self.LbLog.AppendItems("COMMAND >> " + str(data) + '~$')
        self.LogView(2, board=kwargs['serial'], line=data)

        if kwargs['serial'] == 1:
            self.ser1.write(data + '~$' + '\r\n')
        elif kwargs['serial'] == 2:
            self.ser2.write(data + '~$' + '\r\n')

        self.Nows(2, motorid, speed)

    # -- MOTOR ACCELERATION COMMAND --
    def RoboACCEL(self, motorid, accel, **kwargs):
        if self.GETNows(1,motorid) == accel:
            return

        data = (r'NEXUS=' + str(self.AccelCommand(motorid)) + str(accel))
        print data
        #self.LbLog.AppendItems("COMMAND >> " + str(data) + '~$')
        self.LogView(2, board=kwargs['serial'], line=data)

        if kwargs['serial'] == 1:
            self.ser1.write(data + '~$' + '\r\n')
        elif kwargs['serial'] == 2:
            self.ser2.write(data + '~$' + '\r\n')

        self.Nows(3, motorid, accel)

    # -- MOTOR WAITING COMMAND --
    def RoboWAIT(self, timevalue):
        time.sleep(timevalue)
        print timevalue

    def RoboVolumes(self, must_value):
        FULLCammera_Processing.FULLRotatingVolume.__init__(self,must_value)

    # -- MOTOR MANUAL COMMAND --
    def ManualCmd(self, ser, command):

        print ser
        data = command.encode()
        print command

        if int(ser) == 1:
            self.ser1.write(data + '\r\n')
        elif int(ser) == 2:
            self.ser2.write(data + '\r\n')
        elif int(ser) == 3:
            self.ser3.write(data + '\r\n')
        elif int(ser) == 4:
            self.ser2.write(data  + '\r\n')
        self.LbLog.AppendItems("COMMAND [" + str(ser) + "] >> " + str(data) )
    #===================================================================================================================
    #===================================================================================================================

    # ANTI BACKLASH FOR MOTOR
    #============================
    def AntiBacklash(self, mode, koor, koorbefore, motor, lash):
        if mode == 1:
            pass

            #print ("EXECUTING ANTIBACKLASH")
            #koor += self.Offset(1, motor)

            #if koorbefore == "":
            #    koorbefore = 0

            #if int(koor) >= 500 and koorbefore >= 500 and koor < koorbefore:
                #if motor == "X":
                   # antilash = int(koor) - int(lash)
                    #if self.RoboGOX(antilash, 0, 0) == "DONE":
                    #    return
                    #if self.ReadAINO(1, 1, "X") == "DONE":
                #pass
                #return

                #elif motor == "Y":
                    #antilash = int(koor) - int(lash)
                    #if self.RoboGOY(antilash, 0, 0) == "DONE":
                    #    return
                    #if self.ReadAINO(1, 1, "Y") == "DONE":
                    #    return

    def NFS(self,speed,accel):
        listmot = ['A']
        for motor in listmot:
            time.sleep(0.5)
            self.RoboSPEED(motor,speed)
            self.RoboACCEL(motor,accel)


    #===================================================================================================================
    #                                       -- --- --- MOTOR ON OFF AND EXTRA--- --- --
    #===================================================================================================================

    # -- HEATER --
    #def RoboHEAT(self, mode):

        #if mode == 0:
            #data = (self.HomeCommand(motorid))
            #self.LbLog.AppendItems("COMMAND >> " + str(data) + '~$')
            #self.ser2.write(data + '~$' + '\r\n')
            #print('HEATER OFF')
            #self.Lbl_MF_Temp_Status.SetLabel("OFF")

        #elif mode == 1:
            #data = (self.MoveCommand(motorid))
            #self.LbLog.AppendItems("COMMAND >> " + str(data) + '~$')
            #self.ser2.write(data + '~$' + '\r\n')
            #print('HEATER ON')
            #self.Lbl_MF_Temp_Status.SetLabel("ON")


    #===================================================================================================================
    #                                    NOT USED
    #===================================================================================================================
    def RoboONOFF(self, mode, stat):
        if mode == 1:
            if stat == "OFF":
                data = (r'MOVE_F1#F1=90')
                #self.LbLog.AppendItems("COMMAND >> " + str(data) + '~$')
                self.LogView(2, board=1, line=data)
                self.ser1.write(data + '~$' + '\r\n')
                print('FLIP 1 ON')

            elif stat == "ON":
                data = (r'MOVE_F1#F1=0')
                #self.LbLog.AppendItems("COMMAND >> " + str(data) + '~$')
                self.LogView(2, board=1, line=data)
                self.ser1.write(data + '~$' + '\r\n')
                print('FLIP 1 OFF')

        elif mode == 2:
            if stat == "OFF":
                data = (r'MOVE_F2#F2=#0')
                #self.LbLog.AppendItems("COMMAND >> " + str(data) + '~$')
                self.LogView(2, board=1, line=data)
                self.ser1.write(data + '~$' + '\r\n')
                print('FLIP 2 ON')

            elif stat == "ON":
                data = (r'MOVE_F2#F2=180')
                #self.LbLog.AppendItems("COMMAND >> " + str(data) + '~$')
                self.LogView(2, board=1, line=data)
                self.ser1.write(data + '~$' + '\r\n')
                print('FLIP 2 OFF')

#        elif mode == 3:

        elif mode == 4:
            if stat == "OFF":
                data = (r'HEATER_ON#')
                #self.LbLog.AppendItems("COMMAND >> " + str(data) + '~$')
                self.LogView(2, board=2, line=data)
                self.ser2.write(data + '~$' + '\r\n')
                print('HEATER ON')

            elif stat == "ON":
                data = (r'HEATER_OFF#')
                #self.LbLog.AppendItems("COMMAND >> " + str(data) + '~$')
                self.LogView(2, board=2, line=data)
                self.ser2.write(data + '~$' + '\r\n')
                print('HEATER OFF')

        elif mode == 5:
            if stat == "OFF":
                data = (r'MOVE_F3#F3=')
                #self.LbLog.AppendItems("COMMAND >> " + str(data) + '~$')
                self.LogView(2, board=1, line=data)
                self.ser1.write(data + '~$' + '\r\n')
                print('FLIP3 ON')

            elif stat == "ON":
                data = (r'MOVE_F3#F3=')
                #self.LbLog.AppendItems("COMMAND >> " + str(data) + '~$')
                self.LogView(2, board=1, line=data)
                self.ser1.write(data + '~$' + '\r\n')
                print('FLIP3 OFF')

    #===================================================================================================================
    #                                           END OF NOT USED
    #===================================================================================================================

    #-- PANEL TEMPERATURE --
    def GOON(self, mode):
        if mode == 3:
            if self.Cmd_MF_Temp_Standby.GetLabel() == "STANDBY":
                self.StandbyTemp = True
                self.LimitTemp = 37
                self.TimerTemp = 0
                self.suhus = ""
                self.Cmd_MF_Temp_Standby.SetLabel("STOP")
            elif self.Cmd_MF_Temp_Standby.GetLabel() == "STOP":
                self.STOPIT(3)

    #-- STOPING STANDBY THREAD --
    def STOPIT(self, mode):
        if mode == 3:
            self.StandbyTemp = False
            self.suhus = ""
            self.RoboHEAT(0)
            self.Cmd_MF_Temp_Standby.SetLabel("STANDBY")

    #-- READING FROM ARDUINO FOR END FLAG OF MOVEMENT --
    def ReadAINO(self, mode, ser, motorname):
        print("READAINO=======================")
        # print ser
        # print self.Board1_Connection
        # print self.Board1_Reading
        motorid = self.GetMotor(12,motorname)

        if mode == 1:
            FLAG = self.GetMotor(7, motorid)
            self.lastflag = str(FLAG)
            print("READAINO======================= 1")
            print ser
            print FLAG
            self.timesoon = self.CurTime()
            if ser == 1:
                self.Board1_Reading_Flag = FLAG
                # self.Board1_Reading = "ON"
                if FLAG not in (None,''):
                    self.Motor_ReadTypes = 'PROSES'
                else:
                    print 'FLAG IS EMPTY'
                    while True:
                        pass
            # elif ser == 2:
            #     self.Board2_Reading_Flag = FLAG
            #     # self.Board2_Reading = "ON"

        elif mode == 2:
            FLAG = self.GetMotor(8, motorid)
            self.lastflag = str(FLAG)
            print("READAINO======================= 2")
            print FLAG
            self.timesoon = self.CurTime()
            if ser == 1:
                self.Board1_Reading_Flag = FLAG
                if FLAG not in (None,''):
                    self.Motor_ReadTypes = 'PROSES'
                else:
                    print 'FLAG IS EMPTY'
                    while True:
                        pass
                # self.Board1_Reading = "ON"
            # elif ser == 2:
            #     self.Board2_Reading_Flag = FLAG
            #     # self.Board2_Reading = "ON"

    # -- VIEWING LOG MACHINE ON ListBox --
    def LogView(self, mode, **kwargs):
        if mode == 1: #ARDUINO
            if kwargs["board"] == 1:
                final = (str(kwargs["line"]))
                self.TEndUpdateLog(1, final)
            # elif kwargs["board"] == 2:
            #     pass
            # elif kwargs["board"] == 3:
            #     pass

        #COMMAND
        elif mode == 2:
            if kwargs["board"] == 1:
                final = (str(kwargs["line"]))
                self.TEndUpdateLog(3, final)
            # elif kwargs["board"] == 2:
            #     self.LbLog.AppendItems("SEND TO Board 2 >> "+ str(kwargs["line"]))
            #     self.LbLog.SetSelection(int(self.LbLog.GetCount()) - 1)
            # elif kwargs["board"] == 3:
            #     self.LbLog.AppendItems("SEND TO Board 3 >> "+ str(kwargs["line"]))
            #     self.LbLog.SetSelection(int(self.LbLog.GetCount()) - 1)

    # -- PROCES INIT ON DIFFERENT THREAD / PROCES --
    def GenerateProces(self, mode, func, para, **kwargs):
        if mode == 1:  # Multiprocessing
            return (multiprocessing.Process(target=func, args=para))
        elif mode == 2:  # Threading args
            return (threading.Thread(target=func, args=para))
        elif mode == 4: # Threading kwargs
            para2 = kwargs["kwarg"]
            para3 = kwargs['kwarg2']
            return (threading.Thread(target=func, args=para, kwargs={para2:para3}))
        elif mode == 3:
            te = threading.Thread(target=func, args=para)
            te.daemon = True
            te.start()

    def ClosingApp(self, event):
        if self.__close_callback__:
            self.__close_callback__()

    def CFormatSuhu(self, suhu):
        self.BackLenght_Suhu = 2
        self.Splitter_Suhu = '.'
        try:
            realsuhu = suhu
            suhu = str(suhu).split(self.Splitter_Suhu)
            if len(suhu) != 2:
                raise Exception
        except Exception as e:
            suhu = None
            return (suhu)
        else:
            if len(suhu[1]) > self.BackLenght_Suhu:
                suhu1 = suhu[1][0:self.BackLenght_Suhu]
            elif len(suhu[1]) < self.BackLenght_Suhu:
                suhu1 = str('0' * self.BackLenght_Suhu)
            else:
                return (realsuhu)

        result = str(suhu[0]) + str(suhu1)
        return (result)

    def CPingIp(self, host):

        """
        Returns True if host (str) responds to a ping request.
        Remember that a host may not respond to a ping (ICMP) request even if the host name is valid.
        """

        # Ping command count option as function of OS
        param = '-n' if system_name().lower() == 'windows' else '-c'

        # Building the command. Ex: "ping -c 1 google.com"
        command = ['ping', param, '1', host]

        # Pinging
        return system_call(command) == 0


    def CCReadEncoder(self, mode, motorid, command, feedback, marker):
        if mode == 1:
            self.Encoder_Motor = motorid
            self.Encoder_Command = command
            self.Encoder_Feedback = feedback
            self.Encoder_Marker = marker
            self.Encoder_ID = motorid
            # self.Encoder_Reading = 'ON'
            self.Encoder_ReadTypes = 'PROSES'


            print ('Reading With Encoder')

            #Time-Out MANAGER
            time_out = 25
            tstart = self.CCurTime3()
            while self.Encoder_ReadTypes == 'PROSES':
                # tnow = self.CCurTime3()
                # tpassed = tnow - tstart
                #
                # # If Over The limit time
                # if float(tpassed.total_seconds()) > float(time_out):
                #     self.Encoder_Reading_Result = ''
                #     self.Encoder_Reading_Status = 'TIME_OUT'
                #     self.Encoder_Reading = "OFF"
                #     #ToDo : Reconnect and ReRead
                #     print ('TIMED OUT [ENCODER [OUTSIDE]]')
                #     break
                pass

            #IF Using Offser
            # offset = self.GetMotor(6, motorid)
            try:
                offset = int(self.Offset_Encoder[motorid])
            except Exception:
                offset = 0
            if self.EncoderOffset_Type == 'PULSE':
                offset_encoder = self.CConvertPulseToEncoder(1, motorid, float(offset))
            else:
                offset_encoder = offset

            

            calculated = float(self.Encoder_Reading_Result) + float(offset_encoder)

            return (float(calculated))
            #return (self.Encoder_Reading_Result)

    #Temporary to Convert Pulse To Encoder
    def CConvertPulseToEncoder(self, mode, motorid, pulse):
        if mode == 1:
            motorid = int(motorid)
            ratio = self.Encoder_List[motorid]['ratio']
            offset = self.Encoder_List[motorid]['offset']
            converted_val = float(( float(pulse) * float(ratio) ) + float(offset))
            return (converted_val)


    def CCalculateGapEncoder(self, mode, motorid, to_encode, **kwargs):
        if mode == 1:
            command = self.Encoder_List[motorid]['command']
            feedback = self.Encoder_List[motorid]['feedback']
            marker = self.Encoder_List[motorid]['marker']
            now_encode = self.CCReadEncoder(1,motorid,command,feedback,marker)
            #Togo = EncoderValue
            togo = float(to_encode) - float(now_encode)

            #limit to 5 decimal
            togo = round(float(togo),9)

            if kwargs.get('viewlog') != False:
                final = ("Encoder Gap " + str(self.Encoder_List[motorid]['name']) + " // " + str(togo))
                self.TEndUpdateLog(2, final)

            return (float(togo))

    # SPECIALY FOR PERSAMAAN
    def CCalculateGapEncoder2(self, mode, motorid, to_encode, **kwargs):
        if mode == 1:
            command = self.Encoder_List[motorid]['command']
            feedback = self.Encoder_List[motorid]['feedback']
            marker = self.Encoder_List[motorid]['marker']
            now_encode = self.CCReadEncoder(1,motorid,command,feedback,marker)

            #PERSAMAAN
            now_encode = self.CEncoderCalculatePersamaan(id=int(motorid), val=now_encode)

            #Togo = EncoderValue
            togo = float(to_encode) - float(now_encode)

            #limit to 5 decimal
            togo = round(float(togo),9)

            if kwargs.get('viewlog') != False:
                final = ("Encoder Gap " + str(self.Encoder_List[motorid]['name']) + " // " + str(togo))
                self.TEndUpdateLog(2, final)

            return (float(togo))



    def CCalculatePulseEncoder(self, mode, motorid, togo, **kwargs):
        #Get Value Perbandingan
        #ToGo = EncoderLine
        ratio = self.Encoder_List[motorid]['ratio']
        offset = self.Encoder_List[motorid]['offset']
        # is_z = kwargs.get('z')

        dict_name = {1:'Z',4:'Y',5:'C',6:'X'}

        # offset = 0

        #UnderGap Checking
        if math.fabs(float(togo)) <= float(self.UnderGapLimit):
            pulseresult = self.UnderGapConversion(togo)
            return (pulseresult)
        else:

            # IF RATIO PULSE:ENCODER // WITH RATIO OF  1 PULSE = ??? ENCODER
            if mode == 1:
                try:
                    ratio = float(ratio)
                except Exception as e:
                    print e
                else:

                    #Check if Minus
                    if float(togo) < 0:
                        is_min = True
                    else:
                        is_min = False

                    togo = float(math.fabs(float(togo)))


                    # pulseresult = (float(1) / float(ratio))  (float(togo) + (float(offset) * float(-1)))

                    # #Old
                    # pulseresult = (float(togo) + (float(offset) * float(-1))) / float(ratio)

                    #Revisi 2
                    da_names = dict_name[int(motorid)]
                    using_new_rumus = self._RumData[da_names]['use']

                    #Revisi
                    # if is_z == True:
                    #     pulseresult = float( (float(4.479) * float(togo)) - 2.129)

                    if using_new_rumus == True:
                        final = '| USING NEW RUMUS |'
                        self.TEndUpdateLog(2, final)
                        pulseresult = float( (float(self._RumData[da_names]['constant']) * float(togo)) +
                                            (self._RumData[da_names]['plus']) )
                        rumus = '=| (' + str(self._RumData[da_names]['constant']) + ' * ' + str(togo) + ') + (' + str(self._RumData[da_names]['plus']) + ')'
                        self.TEndUpdateLog(2, str(rumus))

                    else:
                        final = '| OLD RUMUS |'
                        self.TEndUpdateLog(2, final)
                        # Old
                        pulseresult = (float(togo) + (float(offset) * float(-1))) / float(ratio)
                        rumus2 = str('=| (' + str('x') + ' + (' + str(offset) + ' * ' + str(-1) + '))' + ' / ' + str(ratio))
                        self.TEndUpdateLog(2, str(rumus2))


                    #ROUND LIMIT sTO 5
                    pulseresult = round(float(pulseresult), 9)
                    results = '=| ' + str(pulseresult)
                    self.TEndUpdateLog(2, str(results))

                    #IF Minus
                    if is_min == True:
                        pulseresult = pulseresult * -1

                return (pulseresult)

            #IF Calculate ENCODER:PULSE  // WITH RATIO OF  1 ENCODER = ??? PULSE
            elif mode == 2:
                try:
                    ratio = float(ratio)
                except Exception as e:
                    print e
                else:

                    # Check if Minus
                    if float(togo) < 0:
                        is_min = True
                    else:
                        is_min = False

                    togo = float(math.fabs(float(togo)))

                    pulseresult = (float(togo) * float(ratio)) + float(offset)

                    # ROUND LIMIT TO 5
                    pulseresult = round(float(pulseresult), 9)

                    # IF Minus
                    if is_min == True:
                        pulseresult = pulseresult * -1

                return (pulseresult)

    def CEncoderMissRecovery(self, mode, to, motorid, **kwargs):

        # PASS THE Recovery if  Encoder Recovery = False
        if not self.EncoderRecovery:
            return ('FINISH')

        if mode == 1:
            recover = False

            #Level 1
            for i in range(int(self.EncoderRecovery_Loop)): # ToDo : Temporary is 5 time, can decide tho
                gap = self.CCalculateGapEncoder(1,motorid,to)

                # Revisi Persamaan
                final1 = "Recovery | REAL | " + str(gap)
                self.TEndUpdateLog(2, final1)
                gap = self.CEncoderCalculatePersamaan(id=int(motorid), val=gap)
                final2 = "Recovery | PERSAMAAN | " + str(gap)
                self.TEndUpdateLog(2, final2)

                pulsevalue = self.CCalculatePulseEncoder(1,motorid,gap)
                now_pulse = self.GETNows(2, motorid)

                we_go = float(pulsevalue) + float(now_pulse)
                pulsevalue = float(we_go)

                serial = 1

                #Execute Command
                # serial is 1 cause 1 is motor
                if self.RoboGO(motorid, values=pulsevalue, home=0, ex='NONE', serial=serial) == "DONE":
                    self.Not_Moving = True
                else:
                    self.Not_Moving = False

                #Read
                if self.Not_Moving == False:
                    motorname = self.GetMotor(1, motorid)
                    self.ReadAINO(1, serial, motorname)
                    self.UNO_Readings = "STARTED"

                    while not self.UNO_Readings == "ENDED":
                        pass

                status = self.CValidateEncoder(1, motorid, to)
                if status == "DONE":
                    recover = True
                    break

            # #Level 2 - uNLIMITED
            # if recover == False:
            #     while status != "DONE":
            #         pulsevalue = self.CCalculateGapEncoder(1, motorid, to)
            #
            #         serial = 1
            #
            #         # Execute Command
            #         # serial is 1 cause 1 is motor
            #         self.RoboGO(motorid, values=pulsevalue, home=0, ex='NONE', serial=serial)
            #
            #         # Read
            #         motorname = self.GetMotor(1, motorid)
            #         self.ReadAINO(1, serial, motorname)
            #         self.UNO_Readings = "STARTED"
            #
            #         while not self.UNO_Readings == "ENDED":
            #             pass
            #
            #         status = self.CValidateEncoder(1, motorid, to)
            #         if status == "DONE":
            #             recover = True
            #             break

            if recover == True:
                return('FINISH')
            else:
                return ('FAILED')



    def CValidateEncoder(self, mode, motorid, to):
        if mode == 1:
            gaprange = float(self.CCalculateGapEncoder(1, motorid, to, viewlog = False))
            if gaprange in (0,0.0):
                return('DONE')
            else:
                return('NOPE')

    #Basic
    def CPulseMissCalculate(self, motorid, to):
        gap = self.CCalculateGapEncoder(1, motorid, to)
        pulsevalue = self.CCalculatePulseEncoder(1, motorid, gap)
        if pulsevalue < 0:
            postpulsevalue = pulsevalue -1
        else:
            postpulsevalue = pulsevalue
        if postpulsevalue <= 10:
            return (None)
        else:
            step1 = pulsevalue - 10
            return (step1)


            #Advance Version
    # def CPulseMissCalculate(self, motorid, to):
    #     pulsevalue = self.CCalculateGapEncoder(1, motorid, to)
    #     if pulsevalue < 0:
    #         postpulsevalue = pulsevalue -1
    #     else:
    #         postpulsevalue = pulsevalue
    #     if postpulsevalue <= 10:
    #         return (None)
    #     else:
    #         step1 = pulsevalue - 10
    #         return (step1)

    def CHomeEncoder(self, motorid):
        data = "HOME_ENCODER_"
        arraymotor = {6:'X',4:'Y',1:'Z',5:'C'}
        idmotor = {6:1, 4:2, 1:3, 5:4}
        #motor
        #motor = arraymotor[int(motorid)]

        #Command
        #command = 'NEXUS=' + data + str(motor) + '#~$' + '\n'
        self.TEndHomeEncoder(idmotor[int(motorid)])
        # try:
        #     self.ser2.write(command.encode())
        # except Exception as e:
        #     print (e)
        # else:
        #     time.sleep(1)
        # self.ser2.flushOutput()
        # self.ser2.flushInput()

        time.sleep(1)
        command = self.Encoder_List[motorid]['command']
        feedback = self.Encoder_List[motorid]['feedback']
        marker = self.Encoder_List[motorid]['marker']
        now_encode = self.CCReadEncoder(1, int(motorid), str(command), str(feedback), str(marker))
        if float(now_encode) in (0,0.0,):
            print ("HOME Encoder " + str(motorid) + " DONE")
            self.Encoder_Home = True
        else:
            print ("HOME Encoder " + str(motorid) + " FAIL")
            self.Encoder_Home = False

    def UnderGapConversion(self, gap):
        calculation = float(gap) * float(self.UnderGapIncrement)
        return(float(calculation))

    def CEncoderCalculatePersamaan(self, id, val):
        persamaan = self._PersamaanEncoder[int(id)]
        calculated = float(val) / float(persamaan)
        return (calculated)







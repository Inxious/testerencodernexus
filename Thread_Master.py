import time
import serialscan


# THREAD FOR PASSING VAR FOR EACH BOARDTHREADS

class ThreadsPasser():


    def __init__(self):

        self.Event_Status = 'OFF'
        self.Proces_Step = "Started"
        print "THREAD EVENT RUNNING"

        # Infinity Loop Until App Exit
        while True:

            if self.Event_Status == "ON":
                if self.Event_Types == "Proces":
                    # == Running Proces
                    self.Proces_Step = "Running"

                    try:
                        self.LoopCount = int(self.LoopCount)
                    except Exception as e:
                        self.LoopCount = 1
                        print self.LoopCount

                    for i in range(self.LoopCount):
                        for datalist in self.Proces_Data:
                            # -- Running the proceslist
                            if not self.Proces_Step == "Unfinished":

                                # Only Run Checked Value
                                if datalist[0] == True:
                                    configname = datalist[1]
                                    self.TEndUpdateLog(5, configname)
                                    self.TableGet(1, act=configname)
                                    self.TEndUpdateLog(5, str(configname) + " " + "END")


                                # -- Adding current proceslist if it was failed before it ends
                                if self.Proces_Step == "Unfinished":
                                    self.Proces_UnfinishedList.append(datalist[1])

                            # -- Adding rest of proceslist that is unfinished
                            if self.Proces_Step == "Unfinished":
                                self.Proces_UnfinishedList.append(datalist[1])



                        # Message Alert
                        if self.Proces_Step == "Unfinished":
                            self.MessagesBox("Ordinary", "Warning : Proces Failed!",
                                             "Ada Proses Yang Gagal !" +
                                             "Proses Akan Berhenti !")

                        # == When Proces Ends

                        self.Event_Types = "NONE"
                        self.Is_All = False

                    # self.MFEnable()
                    if self.Proces_Step == "Running":
                        self.Proces_Step = "Ends"

                    self.Event_Status = "OFF"




-- PIPETE MANAGE

-- SAVE NEW PIPETE
CREATE PROCEDURE SP_Nxs_SaveNewPipete
@Name AS VARCHAR(250),
@Config_ID AS INTEGER
AS
BEGIN
	
	INSERT INTO M_Pipete_Mapping(Pipete_Name, Pipete_ConfigSet)
	VALUES (@Name, @Config_ID)

END

-- UPDATE DATA PIPETE
CREATE PROCEDURE SP_Nxs_UpdateDataPipete
@PipeteID AS INTEGER,
@Name AS VARCHAR(250),
@Config_ID AS INTEGER
AS
BEGIN
	
	UPDATE M_Pipete_Mapping
	SET Pipete_Name = @Name, Pipete_ConfigSet = @Config_ID
	WHERE Pipete_ID = @PipeteID

END

-- DELETE DATA PIPETE
CREATE PROCEDURE SP_Nxs_DeleteDataPipete
@PipeteID AS INTEGER
AS 
BEGIN

	DELETE FROM M_Pipete_Mapping
	WHERE Pipete_ID = @PipeteID

END


-- GET ALL DATA PIPETE
CREATE 
--ALTER
PROCEDURE SP_Nxs_GetAllPipeteData
AS
BEGIN 

	SELECT p.Pipete_ID, p.Pipete_Name, c.MC_Action_Name --Pipete_ConfigSet
	FROM M_Pipete_Mapping AS p
	INNER JOIN M_ConfigurasiHeader AS c ON p.Pipete_ConfigSet = c.MC_HeaderID

END


 -- SP_Nxs_GetCoorColumnList
CREATE 
--ALTER
PROCEDURE SP_Nxs_GetPipeteColumnList
AS
BEGIN
	DECLARE @Tab TABLE ([COLUMNS_ALIAS] Varchar(MAX), [COLUMNS] Varchar(MAX))
	INSERT INTO @Tab
	VALUES
		('Pipete ID','p.Pipete_ID'),
		('Pipete Name','p.Pipete_Name'),
		('Pipete Install Config','c.MC_Action_Name ')
		
		
	SELECT * FROM @Tab
END


-- GET ALL CONFIG EXIST
CREATE PROCEDURE SP_Nxs_GetAllExistedConfig
AS
BEGIN

	SELECT MC_Action_Name FROM M_ConfigurasiHeader
	WHERE IsActive = 1
	ORDER BY MC_Action_Name ASC

END



-- GET DETAILED INFO OF CONFIG
CREATE PROCEDURE SP_Nxs_GetDetailConfigInfo
@Config_Name AS VARCHAR(MAX)
AS
BEGIN
	
	SELECT MC_HeaderID, MC_Action_Name
	FROM M_ConfigurasiHeader
	WHERE MC_Action_Name = @Config_Name
END

EXEC  SP_Nxs_GetPipeteColumnList;

SELECT * FROM M_Pipete_Mapping
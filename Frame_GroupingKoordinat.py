# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.dataview


###########################################################################
## Class Frame_GroupingKoordinat
###########################################################################

class Frame_GroupingKoordinat(wx.Frame):

    def __init__(self, parent):
        self.Frame_GroupingKoor = wx.Frame(parent, id=wx.ID_ANY, title=u"FORM INPUT KOORDINAT GROUP", pos=wx.DefaultPosition,
                          size=wx.Size(727, 710), style=wx.CAPTION|wx.CLOSE_BOX|wx.SYSTEM_MENU|wx.TAB_TRAVERSAL)

        self.Frame_GroupingKoor.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.Frame_GroupingKoor.SetBackgroundColour(wx.Colour(255, 255, 255))

        fgSizer6 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer6.SetFlexibleDirection(wx.BOTH)
        fgSizer6.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText8 = wx.StaticText(self.Frame_GroupingKoor, wx.ID_ANY, u"KOORDINAT GROUPING", wx.DefaultPosition, wx.DefaultSize,
                                           0)
        self.m_staticText8.Wrap(-1)
        self.m_staticText8.SetFont(wx.Font(16, 74, 90, 92, False, "Calibri"))

        fgSizer6.Add(self.m_staticText8, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.m_panel7 = wx.Panel(self.Frame_GroupingKoor, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer14 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer14.SetFlexibleDirection(wx.BOTH)
        fgSizer14.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer16 = wx.FlexGridSizer(1, 4, 0, 0)
        fgSizer16.SetFlexibleDirection(wx.BOTH)
        fgSizer16.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText16 = wx.StaticText(self.m_panel7, wx.ID_ANY, u"Data Koordinat", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText16.Wrap(-1)
        self.m_staticText16.SetFont(wx.Font(12, 74, 90, 92, False, "Arial"))

        fgSizer16.Add(self.m_staticText16, 0, wx.ALL, 5)

        self.m_staticText22 = wx.StaticText(self.m_panel7, wx.ID_ANY, u"Sort by :", wx.DefaultPosition, wx.DefaultSize,
                                            0)
        self.m_staticText22.Wrap(-1)
        fgSizer16.Add(self.m_staticText22, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Rad_KoorSortAsc = wx.RadioButton(self.m_panel7, wx.ID_ANY, u"Ascending", wx.DefaultPosition,
                                              wx.DefaultSize, wx.RB_GROUP)
        self.Rad_KoorSortAsc.SetValue(True)
        fgSizer16.Add(self.Rad_KoorSortAsc, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Rad_KoorSortDesc = wx.RadioButton(self.m_panel7, wx.ID_ANY, u"Descending", wx.DefaultPosition,
                                               wx.DefaultSize, 0)
        fgSizer16.Add(self.Rad_KoorSortDesc, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        fgSizer14.Add(fgSizer16, 1, wx.EXPAND, 5)

        fgSizer15 = wx.FlexGridSizer(1, 5, 0, 0)
        fgSizer15.SetFlexibleDirection(wx.BOTH)
        fgSizer15.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText20 = wx.StaticText(self.m_panel7, wx.ID_ANY, u"Search By Name", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText20.Wrap(-1)
        fgSizer15.Add(self.m_staticText20, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_KoorSearchName = wx.TextCtrl(self.m_panel7, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.Txt_KoorSearchName.SetMinSize(wx.Size(250, -1))

        fgSizer15.Add(self.Txt_KoorSearchName, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Cmd_KoorSearch = wx.Button(self.m_panel7, wx.ID_ANY, u"Search", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer15.Add(self.Cmd_KoorSearch, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Cmd_KoorRefreshGroup = wx.Button(self.m_panel7, wx.ID_ANY, u"Refresh Data", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        fgSizer15.Add(self.Cmd_KoorRefreshGroup, 0, wx.ALL, 5)

        fgSizer14.Add(fgSizer15, 1, wx.EXPAND, 5)

        self.Dv_DataKoordinat = wx.dataview.DataViewListCtrl(self.m_panel7, wx.ID_ANY, wx.DefaultPosition,
                                                             wx.DefaultSize, 0)
        self.Dv_DataKoordinat.SetMinSize(wx.Size(700, 200))

        fgSizer14.Add(self.Dv_DataKoordinat, 0, wx.ALL, 5)

        self.m_panel7.SetSizer(fgSizer14)
        self.m_panel7.Layout()
        fgSizer14.Fit(self.m_panel7)
        fgSizer6.Add(self.m_panel7, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel6 = wx.Panel(self.Frame_GroupingKoor, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer8 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer8.SetFlexibleDirection(wx.BOTH)
        fgSizer8.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText9 = wx.StaticText(self.m_panel6, wx.ID_ANY, u"Form Group Set", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.m_staticText9.Wrap(-1)
        self.m_staticText9.SetFont(wx.Font(12, 74, 90, 92, False, "Arial"))

        fgSizer8.Add(self.m_staticText9, 0, wx.ALL, 5)

        self.m_panel8 = wx.Panel(self.m_panel6, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        gbSizer2 = wx.GridBagSizer(0, 0)
        gbSizer2.SetFlexibleDirection(wx.BOTH)
        gbSizer2.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText10 = wx.StaticText(self.m_panel8, wx.ID_ANY, u"Selected Koordinat", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText10.Wrap(-1)
        self.m_staticText10.SetFont(wx.Font(9, 74, 90, 92, False, "Arial"))

        gbSizer2.Add(self.m_staticText10, wx.GBPosition(0, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_staticText11 = wx.StaticText(self.m_panel8, wx.ID_ANY, u"Koordinat ID", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText11.Wrap(-1)
        gbSizer2.Add(self.m_staticText11, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_KoorSelectedID = wx.TextCtrl(self.m_panel8, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                              wx.DefaultSize, wx.TE_READONLY)
        gbSizer2.Add(self.Txt_KoorSelectedID, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                     5)

        self.m_staticText18 = wx.StaticText(self.m_panel8, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText18.Wrap(-1)
        gbSizer2.Add(self.m_staticText18, wx.GBPosition(0, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_staticText12 = wx.StaticText(self.m_panel8, wx.ID_ANY, u"Koordinate Name", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText12.Wrap(-1)
        gbSizer2.Add(self.m_staticText12, wx.GBPosition(2, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_KoorSelectedName = wx.TextCtrl(self.m_panel8, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                wx.DefaultSize, wx.TE_READONLY)
        self.Txt_KoorSelectedName.SetMinSize(wx.Size(350, -1))

        gbSizer2.Add(self.Txt_KoorSelectedName, wx.GBPosition(2, 1), wx.GBSpan(1, 2), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                     5)

        self.m_staticText33 = wx.StaticText(self.m_panel8, wx.ID_ANY, u"Koordinat Group", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText33.Wrap(-1)
        gbSizer2.Add(self.m_staticText33, wx.GBPosition(3, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_KoorSelectedGroup = wx.TextCtrl(self.m_panel8, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                 wx.Size(350, -1), wx.TE_READONLY)
        self.Txt_KoorSelectedGroup.SetMinSize(wx.Size(350, -1))

        gbSizer2.Add(self.Txt_KoorSelectedGroup, wx.GBPosition(3, 1), wx.GBSpan(1, 1),
                     wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_staticText13 = wx.StaticText(self.m_panel8, wx.ID_ANY, u"Set Group", wx.DefaultPosition, wx.DefaultSize,
                                            0)
        self.m_staticText13.Wrap(-1)
        self.m_staticText13.SetFont(wx.Font(9, 74, 90, 92, False, "Arial"))

        gbSizer2.Add(self.m_staticText13, wx.GBPosition(4, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Rad_KoorSetNewGroup = wx.RadioButton(self.m_panel8, wx.ID_ANY, u"Set to a New Group", wx.DefaultPosition,
                                                  wx.DefaultSize, wx.RB_GROUP)
        self.Rad_KoorSetNewGroup.SetValue(True)
        gbSizer2.Add(self.Rad_KoorSetNewGroup, wx.GBPosition(5, 0), wx.GBSpan(1, 3), wx.ALL | wx.ALIGN_CENTER_VERTICAL,
                     5)

        self.m_panel9 = wx.Panel(self.m_panel8, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer12 = wx.FlexGridSizer(1, 4, 0, 0)
        fgSizer12.SetFlexibleDirection(wx.BOTH)
        fgSizer12.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Lbl_GK_GroupName = wx.StaticText(self.m_panel9, wx.ID_ANY, u"Group Name", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.Lbl_GK_GroupName.Wrap(-1)
        fgSizer12.Add(self.Lbl_GK_GroupName, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_KoorNewGroupName = wx.TextCtrl(self.m_panel9, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                wx.DefaultSize, 0)
        fgSizer12.Add(self.Txt_KoorNewGroupName, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Cmd_KoorNewGroupSave = wx.Button(self.m_panel9, wx.ID_ANY, u"Save", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer12.Add(self.Cmd_KoorNewGroupSave, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Cmd_KoorNewGroupReset = wx.Button(self.m_panel9, wx.ID_ANY, u"Reset", wx.DefaultPosition, wx.DefaultSize,
                                               0)
        fgSizer12.Add(self.Cmd_KoorNewGroupReset, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_panel9.SetSizer(fgSizer12)
        self.m_panel9.Layout()
        fgSizer12.Fit(self.m_panel9)
        gbSizer2.Add(self.m_panel9, wx.GBPosition(6, 0), wx.GBSpan(1, 3), wx.EXPAND | wx.ALL, 5)

        self.Rad_KoorSetGroup = wx.RadioButton(self.m_panel8, wx.ID_ANY, u"Set to Already Exists Group",
                                               wx.DefaultPosition, wx.DefaultSize, 0)
        gbSizer2.Add(self.Rad_KoorSetGroup, wx.GBPosition(7, 0), wx.GBSpan(1, 3), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_panel10 = wx.Panel(self.m_panel8, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer13 = wx.FlexGridSizer(1, 4, 0, 0)
        fgSizer13.SetFlexibleDirection(wx.BOTH)
        fgSizer13.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.Lbl_GK_SelectGroup = wx.StaticText(self.m_panel10, wx.ID_ANY, u"Select Group", wx.DefaultPosition,
                                                wx.DefaultSize, 0)
        self.Lbl_GK_SelectGroup.Wrap(-1)
        fgSizer13.Add(self.Lbl_GK_SelectGroup, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        Cmb_KoorGroupListChoices = []
        self.Cmb_KoorGroupList = wx.ComboBox(self.m_panel10, wx.ID_ANY, u"- Choose -", wx.DefaultPosition,
                                             wx.DefaultSize, Cmb_KoorGroupListChoices, 0)
        fgSizer13.Add(self.Cmb_KoorGroupList, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Cmd_KoorGroupSave = wx.Button(self.m_panel10, wx.ID_ANY, u"Save", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer13.Add(self.Cmd_KoorGroupSave, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_panel10.SetSizer(fgSizer13)
        self.m_panel10.Layout()
        fgSizer13.Fit(self.m_panel10)
        gbSizer2.Add(self.m_panel10, wx.GBPosition(8, 0), wx.GBSpan(1, 3), wx.EXPAND | wx.ALL, 5)

        self.m_panel8.SetSizer(gbSizer2)
        self.m_panel8.Layout()
        gbSizer2.Fit(self.m_panel8)
        fgSizer8.Add(self.m_panel8, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel6.SetSizer(fgSizer8)
        self.m_panel6.Layout()
        fgSizer8.Fit(self.m_panel6)
        fgSizer6.Add(self.m_panel6, 1, wx.EXPAND | wx.ALL, 5)

        self.Frame_GroupingKoor.SetSizer(fgSizer6)
        self.Frame_GroupingKoor.Layout()

        self.Frame_GroupingKoor.Centre(wx.BOTH)

        self.Rad_KoorSetNewGroup.SetValue(True)


        # Connect Events
        self.Cmd_KoorSearch.Bind(wx.EVT_BUTTON, self.SearchName)
        self.Cmd_KoorRefreshGroup.Bind(wx.EVT_BUTTON, self.RefreshDataviewGroupKoor)
        self.Frame_GroupingKoor.Bind(wx.dataview.EVT_DATAVIEW_COLUMN_HEADER_CLICK, self.SortByHeader, id=wx.ID_ANY)
        self.Frame_GroupingKoor.Bind(wx.dataview.EVT_DATAVIEW_SELECTION_CHANGED, self.ApplyThisCoor, id=wx.ID_ANY)
        self.Rad_KoorSetNewGroup.Bind(wx.EVT_RADIOBUTTON, self.ShowNewGroup)
        self.Cmd_KoorNewGroupSave.Bind(wx.EVT_BUTTON, self.SaveNewGroup)
        self.Cmd_KoorNewGroupReset.Bind(wx.EVT_BUTTON, self.ResetNewGroup)
        self.Rad_KoorSetGroup.Bind(wx.EVT_RADIOBUTTON, self.ShowGroup)
        self.Cmd_KoorGroupSave.Bind(wx.EVT_BUTTON, self.SaveGroup)


        # --Show Form
        self.StarterPackGroupingKoor()
        self.Frame_GroupingKoor.Show()
        self.HideExistGroupForm()
        # self.HideNewGroupForm()


    def __del__(self):
        pass

    #Add ON
    def StarterPackGroupingKoor(self):
        self.ResetFormGroupingKoor()
        self.SetColumnDataView()
        self.RefreshDataviewGroupKoor()

    def ApplyThisCoor(self, event=None):
        self.ViewSelectedKoordinate()


    def ShowNewGroupForm(self):
        self.Lbl_GK_GroupName.Show()
        self.Txt_KoorNewGroupName.Show()
        self.Cmd_KoorNewGroupSave.Show()
        self.Cmd_KoorNewGroupReset.Show()
        self.Txt_KoorNewGroupName.SetValue('')

    def HideNewGroupForm(self):
        self.Lbl_GK_GroupName.Hide()
        self.Txt_KoorNewGroupName.Hide()
        self.Cmd_KoorNewGroupSave.Hide()
        self.Cmd_KoorNewGroupReset.Hide()


    def ShowExistGroupForm(self):
        self.Lbl_GK_SelectGroup.Show()
        self.Cmb_KoorGroupList.Show()
        self.Cmd_KoorGroupSave.Show()
        self.Cmb_KoorGroupList.SetValue('- Choose -')

    def HideExistGroupForm(self):
        self.Lbl_GK_SelectGroup.Hide()
        self.Cmb_KoorGroupList.Hide()
        self.Cmd_KoorGroupSave.Hide()

    def ResetFormGroupingKoor(self):
        self.Txt_KoorSearchName.SetValue('')
        self.Txt_KoorSelectedID.SetValue('')
        self.Txt_KoorSelectedName.SetValue('')

    def ViewSelectedKoordinate(self):
        #self.Dv_DataKoordinat.GetSelectedRow()
        print('TRIGGERED')
        row_count = self.Dv_DataKoordinat.GetItemCount()
        selecteds = False
        for item_row in range(row_count):
            if self.Dv_DataKoordinat.IsRowSelected(row=item_row):
                selecteds = True
                row_selected = item_row
                print('selected')
                break
        if selecteds == True:
            self.Txt_KoorSelectedName.SetValue(str(self.Dv_DataKoordinat.GetValue(row=row_selected,col=1)))
            self.Txt_KoorSelectedGroup.SetValue(str(self.Dv_DataKoordinat.GetValue(row=row_selected,col=7)))
            self.Txt_KoorSelectedID.SetValue(str(self.Dv_DataKoordinat.GetValue(row=row_selected,col=0)))

    def LoadDataToGroupingDv(self, data):
        self.Dv_DataKoordinat.DeleteAllItems()
        print (data)
        for row in data:
            if len(list(row)) == self.Dv_DataKoordinat.GetColumnCount():
                row = [str(x) for x in row]
                self.Dv_DataKoordinat.AppendItem(row)


        # if len(data) != self.Dv_DataKoordinat.GetColumnCount():
        #     dialog = wx.MessageDialog(parent=None,
        #                               message='Loaded Data Column have a different number',
        #                               caption='Error Loading Data',
        #                               style= wx.OK | wx.ICON_ERROR)
        #     resp = dialog.ShowModal()
        #     return
        #
        #
        # for row in data:
        #     self.Dv_DataKoordinat.AppendItem(list(row))

    def SearchKoordinateSet(self, search_words):
        #ToDo : SQL QUERY SELECT SEARCHED QUERY
        # SP_Nxs_SearchCoordinate Name
        data = self.GetSearchedCoor(words=search_words)
        self.LoadDataToGroupingDv(data=data)
        pass

    def SetColumnDataView(self):
        # ToDo : Query Data List Column
        # SP_Nxs_GetCoorColumnList
        sql_result, data, error = self.GetRunningColumnListGroup()
        print(data)
        # sql_result, data, error = None, None, None

        if sql_result in (None, False):
            # ERROR
            titles = 'Executing Query Error'
            texts = '[Error Query GetRunningColumnListGroup] \n ' + str(error)
            answer = self.MessageBox(type=1, text=texts, title=titles)
            return

        self.Data_DvKoordinat = {}
        col_num = 0
        for column_alias, column in data:
            # col = list(row.values())[0]
            # col = list(row)[0]
            # print (col)
            obj = self.Dv_DataKoordinat.AppendTextColumn(str(column_alias), width=len(column_alias) * 7 + 20)
            self.Data_DvKoordinat.update({col_num: [column_alias, column, obj]})
            col_num += 1

    def MessageBox(self, type, title, text):

        #Error
        if type == 1:
            exMsgbox = wx.MessageDialog(None, text, title, wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

            klick = exMsgbox.ShowModal()

        #Info
        elif type == 2:
            exMsgbox = wx.MessageDialog(None, text, title, wx.OK | wx.ICON_INFORMATION | wx.STAY_ON_TOP)

            klick = exMsgbox.ShowModal()

        #Warning
        elif type == 3:
            exMsgbox = wx.MessageDialog(None, text, title, wx.OK | wx.ICON_WARNING | wx.STAY_ON_TOP)

            klick = exMsgbox.ShowModal()

        #Question
        elif type == 4:
            exMsgbox = wx.MessageDialog(None, text, title, wx.OK | wx.CANCEL | wx.ICON_INFORMATION | wx.STAY_ON_TOP)

            klick = exMsgbox.ShowModal()

            if klick == wx.ID_OK:
                return (True)
            elif klick == wx.ID_CANCEL:
                return (False)
            else:
                return (False)

    # Virtual event handlers, overide them in your derived class
    def SearchName(self, event):
        name = self.Txt_KoorSearchName.GetValue()
        self.SearchKoordinateSet(search_words=name)

    def RefreshDataviewGroupKoor(self, event=None):
        #ToDo : Query Select Data
        # SP_Nxs_GetDataCoordinate
        new_data = self.GetAllDataGrouping()


        self.LoadDataToGroupingDv(data=new_data)
        pass


    def SortByHeader(self, event):
        if self.Dv_DataKoordinat.GetItemCount() == 0:
            return

        if self.Rad_KoorSortAsc.GetValue():
            sort_types = 'ASC'
        elif self.Rad_KoorSortDesc.GetValue():
            sort_types = 'DESC'

        row_count = self.Dv_DataKoordinat.GetItemCount()
        view_col_obj = event.GetDataViewColumn()

        # data_column_marking = {0:'running_ID',1:'user_ID',2:'platform_source',3:'crawl_source_type',
        #                        4:'crawl_active_pages',5:'search_input',6:'title_or_description_required_words',
        #                        7:'running_status',8:'record_lastupdate_datetime'}
        
        index_column = view_col_obj.GetModelColumn()
        sort_column = self.Data_DvKoordinat[index_column][1]
        print(view_col_obj)
        print(index_column)

        data_run_id_lis = []
        for rows in range(row_count):
            id_on_row = int(self.Dv_DataKoordinat.GetValue(row=rows, col=0))
            data_run_id_lis.append(id_on_row)

        print(str(data_run_id_lis))
        print(str(tuple(data_run_id_lis)))

        # CHANGE  data_run_id_lis to be compatible with sql cast IN ( A TUPLE )
        if len(data_run_id_lis) == 1:
            data_run_id_lis = str(data_run_id_lis).replace('[', '(').replace(']', ')')
        else:
            data_run_id_lis = str(tuple(data_run_id_lis))

        # ToDo Execute SQL To Sort
        # SP_Nxs_CoordinateSort
        sql_result, data, error = self.GetSortedRunningLog(sort_type=sort_types, sort_column=sort_column,
                                                           id_list=data_run_id_lis)
        print(data)
        if sql_result in (None, False):
            # ERROR
            titles = 'Executing Query Error'
            texts = '[Error Query GetSortedRunningLog] \n ' + str(error)
            answer = self.MessageBox(type=1, text=texts, title=titles)
            return

        # APPLY DATA TO DATA VIEW
        self.LoadDataToGroupingDv(data=data)
        pass

        # event.Skip()

    def ShowNewGroup(self, event):
        self.ShowNewGroupForm()
        self.HideExistGroupForm()

    def SaveNewGroup(self, mode):
        name = self.Txt_KoorNewGroupName.GetValue()
        # ToDO : QUERY DATA GROUPING NEW
        # SP_Nxs_AddNewCoorGroup
        self.SaveDataGrouping(mode=1,koor_id=self.Txt_KoorSelectedID.GetValue(),
                              group=name)
        self.RefreshDataviewGroupKoor()
        self.ResetFormGroupingKoor()

    def ResetNewGroup(self, event):
        self.Txt_KoorNewGroupName.SetValue('')

    def ShowGroup(self, event):
        self.ShowExistGroupForm()
        self.HideNewGroupForm()
        self.LoadDataGroupToCombobox()
    
    def LoadDataGroupToCombobox(self):
        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_GetAllExistedGroup',
                                        parameter_list=[])

        if sql_result:
            self.Cmb_KoorGroupList.Clear()
            for item in data:
                self.Cmb_KoorGroupList.AppendItems(str(item[0]))
            self.Cmb_KoorGroupList.SetValue('- Choose -')

        return (sql_result, data, error)

    def SaveGroup(self, event):
        name = self.Cmb_KoorGroupList.GetValue()
        id = self.Txt_KoorSelectedID.GetValue()
        if id == None:
            return
        #ToDO : QUERY DATA GROUPING
        # SP_Nxs_UpdateCoorGroup
        self.SaveDataGrouping(mode=2,koor_id=id,
                              group=name)

        self.RefreshDataviewGroupKoor()
        self.ResetFormGroupingKoor()

    #To Model
    def SaveDataGrouping(self, mode, koor_id, group):
        if mode == 1:
            # INSERT
            sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_AddNewCoorGroup',
                                        parameter_list=[int(koor_id), group])

            # return (sql_result, data, error)
            print(data)
            if sql_result == False:
                # ToDo MessageBox
                pass

            return (data)


        elif mode == 2:
            # UPDATE
            sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_UpdateCoorGroup',
                                        parameter_list=[int(koor_id), str(group)])

            # return (sql_result, data, error)
            print(data)
            if sql_result == False:
                # ToDo MessageBox
                pass

            return (data)


    def GetAllDataGrouping(self, mode=1):

        #ALL
        if mode == 1:
            sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_GetDataCoordinate',
                                        parameter_list=[])

            # return (sql_result, data, error)
            if sql_result == False:
                #ToDo MessageBox
                pass

            return(data)
                
    def GetSortedRunningLog(self, sort_type, sort_column, id_list):
        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_CoordinateSort',
                                        parameter_list=[id_list,sort_type,sort_column])

        # return (sql_result, data, error)
        if sql_result == False:
                #ToDo MessageBox
                pass
        return (sql_result, data, error)


    def GetSearchedCoor(self, words):
        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_SearchCoordinate',
                                        parameter_list=[words])

        # return (sql_result, data, error)
        if sql_result == False:
                #ToDo MessageBox
                pass
        return(data)

    def GetRunningColumnListGroup(self):
        sql_result, data, error = self.SQL_SP(sp_name=' SP_Nxs_GetKoorGroupColumnList',
                                        parameter_list=[])

        # return (sql_result, data, error)
        print(data)
        if sql_result == False:
                #ToDo MessageBox
                pass
        return(sql_result, data, error)





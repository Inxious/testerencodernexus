

class Th_Manuals():
    def __init__(self):

        self.ManEvent_Status = 'OFF'
        self.ManEvent_Type = ''
        while True:
            if self.ManEvent_Status == 'ON':
                if self.ManEvent_Type == 'GoEncoder':
                    MotorID = int(self.ManID)
                    if int(MotorID) == int(1):
                        is_z = True
                    else:
                        is_z = None

                    Koordinat = float(self.ManKoor)
                    self.ThreadGoEncoder(MotorID, Koordinat, z=is_z)

                elif self.ManEvent_Type == 'WaitHome':
                    MotorName = self.GetMotor(1, self.ManID)
                    self.ReadAINO(2, 1, MotorName)
                    self.UNO_Readings = "STARTED"

                    while not self.UNO_Readings == "ENDED":
                        pass

                    idlist = {1: 6, 2: 4, 3: 1, 4: 5}
                    idlist_keys = idlist.keys()
                    mode = ''
                    for item in idlist_keys:
                        if idlist.get(item) == self.ManID:
                            mode = item
                    if mode == '':
                        print ('AUTO HOME ENCODER ERROR!')
                    else:
                        self.TEndHomeEncoder(mode)

                self.ManEvent_Type = ''
                self.ManEvent_Status = 'OFF'

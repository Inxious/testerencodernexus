# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.dataview


###########################################################################
## Class Frame_OffsetManagement
###########################################################################

class Frame_OffsetManagement(wx.Frame):

    def __init__(self, parent):
        self.Frame_OffsetManagement = wx.Frame(parent, id=wx.ID_ANY, title=u"FORM INPUT OFFSET", pos=wx.DefaultPosition,
                          size=wx.Size(628, 604), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        self.Frame_OffsetManagement.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.Frame_OffsetManagement.SetBackgroundColour(wx.Colour(255, 255, 255))

        fgSizer17 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer17.SetFlexibleDirection(wx.BOTH)
        fgSizer17.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText23 = wx.StaticText(self.Frame_OffsetManagement, wx.ID_ANY, u"Koordinat Offset Management", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText23.Wrap(-1)
        self.m_staticText23.SetFont(wx.Font(16, 74, 90, 92, False, "Calibri"))

        fgSizer17.Add(self.m_staticText23, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.m_panel11 = wx.Panel(self.Frame_OffsetManagement, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer18 = wx.FlexGridSizer(1, 2, 0, 0)
        fgSizer18.SetFlexibleDirection(wx.BOTH)
        fgSizer18.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_panel15 = wx.Panel(self.m_panel11, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel15.SetBackgroundColour(wx.Colour(255, 255, 255))

        fgSizer20 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer20.SetFlexibleDirection(wx.BOTH)
        fgSizer20.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText24 = wx.StaticText(self.m_panel15, wx.ID_ANY, u"Form Input Offset", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText24.Wrap(-1)
        self.m_staticText24.SetFont(wx.Font(12, 74, 90, 92, False, "Arial"))

        fgSizer20.Add(self.m_staticText24, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_panel13 = wx.Panel(self.m_panel15, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        gbSizer4 = wx.GridBagSizer(0, 0)
        gbSizer4.SetFlexibleDirection(wx.BOTH)
        gbSizer4.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText25 = wx.StaticText(self.m_panel13, wx.ID_ANY, u"Select Pipete", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText25.Wrap(-1)
        gbSizer4.Add(self.m_staticText25, wx.GBPosition(0, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        Cmb_Offset_SelectPipeteChoices = []
        self.Cmb_Offset_SelectPipete = wx.ComboBox(self.m_panel13, wx.ID_ANY, u"- Choose -", wx.DefaultPosition,
                                                   wx.DefaultSize, Cmb_Offset_SelectPipeteChoices, 0)
        self.Cmb_Offset_SelectPipete.SetMinSize(wx.Size(220, -1))

        gbSizer4.Add(self.Cmb_Offset_SelectPipete, wx.GBPosition(0, 1), wx.GBSpan(1, 1),
                     wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_staticText26 = wx.StaticText(self.m_panel13, wx.ID_ANY, u"Select Koordinat Group", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText26.Wrap(-1)
        gbSizer4.Add(self.m_staticText26, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        Cmb_Offset_SelectGroupChoices = []
        self.Cmb_Offset_SelectGroup = wx.ComboBox(self.m_panel13, wx.ID_ANY, u"- Choose -", wx.DefaultPosition,
                                                  wx.DefaultSize, Cmb_Offset_SelectGroupChoices, 0)
        self.Cmb_Offset_SelectGroup.SetMinSize(wx.Size(220, -1))

        gbSizer4.Add(self.Cmb_Offset_SelectGroup, wx.GBPosition(1, 1), wx.GBSpan(1, 1),
                     wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_panel13.SetSizer(gbSizer4)
        self.m_panel13.Layout()
        gbSizer4.Fit(self.m_panel13)
        fgSizer20.Add(self.m_panel13, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel15.SetSizer(fgSizer20)
        self.m_panel15.Layout()
        fgSizer20.Fit(self.m_panel15)
        fgSizer18.Add(self.m_panel15, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel16 = wx.Panel(self.m_panel11, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer19 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer19.SetFlexibleDirection(wx.BOTH)
        fgSizer19.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText27 = wx.StaticText(self.m_panel16, wx.ID_ANY, u"Offset Value", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText27.Wrap(-1)
        self.m_staticText27.SetFont(wx.Font(12, 74, 90, 92, False, "Arial"))

        fgSizer19.Add(self.m_staticText27, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_panel14 = wx.Panel(self.m_panel16, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        gbSizer5 = wx.GridBagSizer(0, 0)
        gbSizer5.SetFlexibleDirection(wx.BOTH)
        gbSizer5.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText28 = wx.StaticText(self.m_panel14, wx.ID_ANY, u"X", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText28.Wrap(-1)
        gbSizer5.Add(self.m_staticText28, wx.GBPosition(0, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_Offset_X = wx.TextCtrl(self.m_panel14, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                        0)
        self.Txt_Offset_X.SetMinSize(wx.Size(70, -1))

        gbSizer5.Add(self.Txt_Offset_X, wx.GBPosition(0, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_staticText33 = wx.StaticText(self.m_panel14, wx.ID_ANY, u"A", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText33.Wrap(-1)
        gbSizer5.Add(self.m_staticText33, wx.GBPosition(0, 2), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_Offset_A = wx.TextCtrl(self.m_panel14, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                        0)
        self.Txt_Offset_A.SetMinSize(wx.Size(70, -1))

        gbSizer5.Add(self.Txt_Offset_A, wx.GBPosition(0, 3), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_staticText30 = wx.StaticText(self.m_panel14, wx.ID_ANY, u"Y", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText30.Wrap(-1)
        gbSizer5.Add(self.m_staticText30, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_Offset_Y = wx.TextCtrl(self.m_panel14, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                        0)
        self.Txt_Offset_Y.SetMinSize(wx.Size(70, -1))

        gbSizer5.Add(self.Txt_Offset_Y, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_staticText31 = wx.StaticText(self.m_panel14, wx.ID_ANY, u"Z", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText31.Wrap(-1)
        gbSizer5.Add(self.m_staticText31, wx.GBPosition(2, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_Offset_Z = wx.TextCtrl(self.m_panel14, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                        0)
        self.Txt_Offset_Z.SetMinSize(wx.Size(70, -1))

        gbSizer5.Add(self.Txt_Offset_Z, wx.GBPosition(2, 1), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_staticText35 = wx.StaticText(self.m_panel14, wx.ID_ANY, u"C", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText35.Wrap(-1)
        gbSizer5.Add(self.m_staticText35, wx.GBPosition(1, 2), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_Offset_C = wx.TextCtrl(self.m_panel14, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                        0)
        self.Txt_Offset_C.SetMinSize(wx.Size(70, -1))

        gbSizer5.Add(self.Txt_Offset_C, wx.GBPosition(1, 3), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_panel14.SetSizer(gbSizer5)
        self.m_panel14.Layout()
        gbSizer5.Fit(self.m_panel14)
        fgSizer19.Add(self.m_panel14, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel16.SetSizer(fgSizer19)
        self.m_panel16.Layout()
        fgSizer19.Fit(self.m_panel16)
        fgSizer18.Add(self.m_panel16, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel11.SetSizer(fgSizer18)
        self.m_panel11.Layout()
        fgSizer18.Fit(self.m_panel11)
        fgSizer17.Add(self.m_panel11, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel12 = wx.Panel(self.Frame_OffsetManagement, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer24 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer24.SetFlexibleDirection(wx.BOTH)
        fgSizer24.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        bSizer4 = wx.BoxSizer(wx.HORIZONTAL)

        self.Cmd_Offset_New = wx.Button(self.m_panel12, wx.ID_ANY, u"New", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer4.Add(self.Cmd_Offset_New, 0, wx.ALL, 5)

        self.Cmd_Offset_Edit = wx.Button(self.m_panel12, wx.ID_ANY, u"Edit", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer4.Add(self.Cmd_Offset_Edit, 0, wx.ALL, 5)

        self.Cmd_Offset_Del = wx.Button(self.m_panel12, wx.ID_ANY, u"Delete", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer4.Add(self.Cmd_Offset_Del, 0, wx.ALL, 5)

        self.Cmd_Offset_RefreshData = wx.Button(self.m_panel12, wx.ID_ANY, u"Refresh", wx.DefaultPosition,
                                                wx.DefaultSize, 0)
        bSizer4.Add(self.Cmd_Offset_RefreshData, 0, wx.ALL, 5)

        fgSizer24.Add(bSizer4, 1, wx.EXPAND, 5)

        self.m_panel12.SetSizer(fgSizer24)
        self.m_panel12.Layout()
        fgSizer24.Fit(self.m_panel12)
        fgSizer17.Add(self.m_panel12, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.m_panel17 = wx.Panel(self.Frame_OffsetManagement, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer14 = wx.FlexGridSizer(3, 1, 0, 0)
        fgSizer14.SetFlexibleDirection(wx.BOTH)
        fgSizer14.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        fgSizer16 = wx.FlexGridSizer(1, 4, 0, 0)
        fgSizer16.SetFlexibleDirection(wx.BOTH)
        fgSizer16.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText16 = wx.StaticText(self.m_panel17, wx.ID_ANY, u"Data Offset", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText16.Wrap(-1)
        self.m_staticText16.SetFont(wx.Font(12, 74, 90, 92, False, "Arial"))

        fgSizer16.Add(self.m_staticText16, 0, wx.ALL, 5)

        self.m_staticText22 = wx.StaticText(self.m_panel17, wx.ID_ANY, u"Sort by :", wx.DefaultPosition, wx.DefaultSize,
                                            0)
        self.m_staticText22.Wrap(-1)
        fgSizer16.Add(self.m_staticText22, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Rad_Offset_SortAsc = wx.RadioButton(self.m_panel17, wx.ID_ANY, u"Ascending", wx.DefaultPosition,
                                                 wx.DefaultSize, wx.RB_GROUP)
        self.Rad_Offset_SortAsc.SetValue(True)
        fgSizer16.Add(self.Rad_Offset_SortAsc, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Rad_Offset_SortDesc = wx.RadioButton(self.m_panel17, wx.ID_ANY, u"Descending", wx.DefaultPosition,
                                                  wx.DefaultSize, 0)
        fgSizer16.Add(self.Rad_Offset_SortDesc, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        fgSizer14.Add(fgSizer16, 1, wx.EXPAND, 5)

        fgSizer15 = wx.FlexGridSizer(1, 5, 0, 0)
        fgSizer15.SetFlexibleDirection(wx.BOTH)
        fgSizer15.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText20 = wx.StaticText(self.m_panel17, wx.ID_ANY, u"Search By Group", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_staticText20.Wrap(-1)
        fgSizer15.Add(self.m_staticText20, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_Offset_SearchByGroup = wx.TextCtrl(self.m_panel17, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                    wx.DefaultSize, 0)
        self.Txt_Offset_SearchByGroup.SetMinSize(wx.Size(250, -1))

        fgSizer15.Add(self.Txt_Offset_SearchByGroup, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Cmd_Offset_SearchGroup = wx.Button(self.m_panel17, wx.ID_ANY, u"Search", wx.DefaultPosition,
                                                wx.DefaultSize, 0)
        fgSizer15.Add(self.Cmd_Offset_SearchGroup, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        fgSizer14.Add(fgSizer15, 1, wx.EXPAND, 5)

        self.Dv_DataOffset = wx.dataview.DataViewListCtrl(self.m_panel17, wx.ID_ANY, wx.DefaultPosition,
                                                             wx.DefaultSize, 0)
        self.Dv_DataOffset.SetMinSize(wx.Size(600, 200))

        fgSizer14.Add(self.Dv_DataOffset, 0, wx.ALL, 5)

        self.m_panel17.SetSizer(fgSizer14)
        self.m_panel17.Layout()
        fgSizer14.Fit(self.m_panel17)
        fgSizer17.Add(self.m_panel17, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel18 = wx.Panel(self.Frame_OffsetManagement, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer34 = wx.FlexGridSizer(0, 2, 0, 0)
        fgSizer34.SetFlexibleDirection(wx.BOTH)
        fgSizer34.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        bSizer5 = wx.BoxSizer(wx.HORIZONTAL)

        self.Cmd_Offset_Save = wx.Button(self.m_panel18, wx.ID_ANY, u"OK", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer5.Add(self.Cmd_Offset_Save, 0, wx.ALL, 5)

        self.Cmd_Offset_Exit = wx.Button(self.m_panel18, wx.ID_ANY, u"EXIT", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer5.Add(self.Cmd_Offset_Exit, 0, wx.ALL, 5)

        fgSizer34.Add(bSizer5, 1, wx.EXPAND, 5)

        self.m_panel18.SetSizer(fgSizer34)
        self.m_panel18.Layout()
        fgSizer34.Fit(self.m_panel18)
        fgSizer17.Add(self.m_panel18, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Frame_OffsetManagement.SetSizer(fgSizer17)
        self.Frame_OffsetManagement.Layout()

        self.Frame_OffsetManagement.Centre(wx.BOTH)

        # Connect Events
        self.Cmd_Offset_New.Bind(wx.EVT_BUTTON, self.NewOffset)
        self.Cmd_Offset_Edit.Bind(wx.EVT_BUTTON, self.EditOffset)
        self.Cmd_Offset_Del.Bind(wx.EVT_BUTTON, self.DeleteOffset)
        self.Cmd_Offset_RefreshData.Bind(wx.EVT_BUTTON, self.RefreshOffsetData)
        self.Cmd_Offset_SearchGroup.Bind(wx.EVT_BUTTON, self.SearchGroupOffset)
        self.Frame_OffsetManagement.Bind(wx.dataview.EVT_DATAVIEW_COLUMN_HEADER_CLICK, self.SortOffsetHeader, id=wx.ID_ANY)
        self.Cmd_Offset_Save.Bind(wx.EVT_BUTTON, self.SaveAllOffset)
        self.Cmd_Offset_Exit.Bind(wx.EVT_BUTTON, self.ExitOffsetForm)

        self.Cmd_Offset_Exit.Hide()

        #Show Form
        self.StarterFrameOffset()
        self.Frame_OffsetManagement.Show()

    def __del__(self):
        pass

    #Add ON
    def StarterFrameOffset(self):
        self.ResetInputOffsetForm()
        self.SetColumnDataViewOffset()
        self.RefreshOffsetData()
        self.DisableOffsetForm()

    def ResetInputOffsetForm(self):
        self.Cmb_Offset_SelectPipete.SetValue('')
        self.Cmb_Offset_SelectGroup.SetValue('')
        self.Txt_Offset_X.SetValue('')
        self.Txt_Offset_Y.SetValue('')
        self.Txt_Offset_Z.SetValue('')
        self.Txt_Offset_A.SetValue('')
        self.Txt_Offset_C.SetValue('')

    def ResetSearchOffsetForm(self):
        self.Txt_Offset_SearchByGroup.SetValue('')

    def EnableOffsetForm(self):
        self.Cmb_Offset_SelectGroup.Enable()
        self.Cmb_Offset_SelectPipete.Enable()
        self.Txt_Offset_X.Enable()
        self.Txt_Offset_Y.Enable()
        self.Txt_Offset_Z.Enable()
        self.Txt_Offset_A.Enable()
        self.Txt_Offset_C.Enable()

        #Load Group List
        data_gro = self.GetAllDataExistedGroup()
        self.Cmb_Offset_SelectGroup.Clear()
        for row in data_gro:
            col = str(row[0])
            self.Cmb_Offset_SelectGroup.AppendItems(col)

        # Load Pipete List
        data_pip = self.GetAllDataExistedPipete()
        self.Cmb_Offset_SelectPipete.Clear()
        for row in data_pip:
            col = str(row[0])
            self.Cmb_Offset_SelectPipete.AppendItems(col)

    def DisableOffsetForm(self):
        self.Cmb_Offset_SelectGroup.Disable()
        self.Cmb_Offset_SelectPipete.Disable()
        self.Txt_Offset_X.Disable()
        self.Txt_Offset_Y.Disable()
        self.Txt_Offset_Z.Disable()
        self.Txt_Offset_A.Disable()
        self.Txt_Offset_C.Disable()

    def LoadDataToOffsetDv(self, data):
        # if len(data[0]) != self.Dv_DataOffset.GetColumnCount():
        #     dialog = wx.MessageDialog(parent=None,
        #                       message='Loaded Data Column have a different number',
        #                       caption='Error Loading Data',
        #                       style= wx.OK | wx.ICON_ERROR)
        #     resp = dialog.ShowModal()
        #     return

        self.Dv_DataOffset.DeleteAllItems()
        if data == None:
            return
        for row in data:
            self.Dv_DataOffset.AppendItem(list(row))

    def SaveCoorOffset(self):
        # ToDo : Process Edit and Query

        # already_exist = self.CheckOffsetDataExistence()

        already_exist = False
        if already_exist:
            #ToDo : MessageBox
            return
        else:
            #ToDo : Query Save Pipete
            pipete_sel = self.Cmb_Offset_SelectPipete.GetValue()
            group_sel = self.Cmb_Offset_SelectGroup.GetValue()

            if '' in (pipete_sel, group_sel):
                # ToDo : Alert Kosong
                pass
            else:
                try:
                    off_a = float(self.Txt_Offset_A.GetValue()) if self.Txt_Offset_A != '' else 0
                    off_c = float(self.Txt_Offset_C.GetValue()) if self.Txt_Offset_C != '' else 0
                    off_x = float(self.Txt_Offset_X.GetValue()) if self.Txt_Offset_X != '' else 0
                    off_y = float(self.Txt_Offset_Y.GetValue()) if self.Txt_Offset_Y != '' else 0
                    off_z = float(self.Txt_Offset_Z.GetValue()) if self.Txt_Offset_Z != '' else 0
                except Exception as e:
                    print(e)
                    # ToDo : Alert Error
                else:
                    # ToDo : Query Data Update
                    sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_AddOffsetMapping',
                                        parameter_list=[str(pipete_sel), str(group_sel), float(off_x), float(off_y),
                                                        float(off_z), float(off_a), float(off_c)])

                    if sql_result == False:
                        #ToDo : MessageBox
                        pass
        

    def UpdateCoorOffset(self, last_data):
        # ToDo : Process Edit and Query

        already_exist = self.CheckOffsetDataExistence()

        if already_exist:
            #ToDo : MessageBox
            return
        else:

            #Last Data ID
            id_last = last_data[0]

            #ToDo : Query Save Pipete
            pipete_sel = self.Cmb_Offset_SelectPipete.GetValue() #Name
            group_sel = self.Cmb_Offset_SelectGroup.GetValue() #Name

            if '' in (pipete_sel, group_sel):
                # ToDo : Alert Kosong
                pass
            else:
                try:
                    off_a = float(self.Txt_Offset_A.GetValue()) if self.Txt_Offset_A != '' else 0
                    off_c = float(self.Txt_Offset_C.GetValue()) if self.Txt_Offset_C != '' else 0
                    off_x = float(self.Txt_Offset_X.GetValue()) if self.Txt_Offset_X != '' else 0
                    off_y = float(self.Txt_Offset_Y.GetValue()) if self.Txt_Offset_Y != '' else 0
                    off_z = float(self.Txt_Offset_Z.GetValue()) if self.Txt_Offset_Z != '' else 0
                except Exception as e:
                    print(e)
                    # ToDo : Alert Error
                else:
                    # ToDo : QUERY UPDATE
                    sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_UpdateOffsetMapping',
                                        parameter_list=[id_last, pipete_sel, group_sel, off_x, off_y, off_z, off_a, off_c])

                    if sql_result == False:
                        #ToDo : MessageBox
                        pass

    def GetSelectedKoordinate(self):
        # self.Dv_DataKoordinat.GetSelectedRow()
        row_count = self.Dv_DataOffset.GetItemCount()
        selecteds = False
        # row_selected = None
        row_data = []
        for item_row in range(row_count):
            if self.Dv_DataOffset.IsRowSelected(row=item_row):
                selecteds = True
                row_selected = item_row
                row_data = [self.Dv_DataOffset.GetValue(row=row_selected,col=x) for x in range(self.Dv_DataOffset.GetColumnCount())]
                break

        # return (selecteds,row_selected,row_data)
        return (selecteds,row_data)

    # def SelectedData

    def DestroyOffset(self):
        self.Frame_OffsetManagement.Destroy()

    # Virtual event handlers, overide them in your derived class
    def NewOffset(self, event=None):
        print (self.Cmd_Offset_New.GetLabel())
        if self.Cmd_Offset_New.GetLabel() == 'SAVE':
            

            #ToDo : QUERY SAVE
            self.SaveCoorOffset()


            self.Cmd_Offset_New.SetLabel('New')
            self.Cmd_Offset_Edit.SetLabel('Edit')
            self.Cmd_Offset_Del.Show()
            self.Cmd_Offset_RefreshData.Show()
            self.ResetInputOffsetForm()
            self.DisableOffsetForm()
            self.RefreshOffsetData()
            return

        elif self.Cmd_Offset_New.GetLabel() == 'UPDATE':


            selected = self.OffMan_EditedData
            if len(selected) != 0:
                # ToDo : QUERY UPDATE
                self.UpdateCoorOffset(last_data=selected)




            self.Cmd_Offset_New.SetLabel('New')
            self.Cmd_Offset_Edit.SetLabel('Edit')
            self.Cmd_Offset_Del.Show()
            self.Cmd_Offset_RefreshData.Show()
            self.ResetInputOffsetForm()
            self.DisableOffsetForm()
            self.RefreshOffsetData()
            return

        
        self.Cmd_Offset_New.SetLabel('SAVE')
        self.Cmd_Offset_Edit.SetLabel('CANCEL')
        self.Cmd_Offset_Del.Hide()
        self.Cmd_Offset_RefreshData.Hide()
        self.EnableOffsetForm()
        self.ResetInputOffsetForm()



    def EditOffset(self, event=None):
        if self.Cmd_Offset_Edit.GetLabel() == 'CANCEL':
            self.Cmd_Offset_New.SetLabel('New')
            self.Cmd_Offset_Edit.SetLabel('Edit')
            self.Cmd_Offset_Del.Show()
            self.Cmd_Offset_RefreshData.Show()
            self.ResetInputOffsetForm()
            self.DisableOffsetForm()
            return

        #SET EDITED RECORD IDENTITY
        is_select , self.OffMan_EditedData = self.GetSelectedKoordinate()

        if is_select:

            self.EnableOffsetForm()
            self.Cmd_Offset_New.SetLabel('UPDATE')
            self.Cmd_Offset_Edit.SetLabel('CANCEL')
            self.Cmd_Offset_Del.Hide()
            self.Cmd_Offset_RefreshData.Hide()
            self.ResetInputOffsetForm()
            self.Cmb_Offset_SelectPipete.Disable()
            self.Cmb_Offset_SelectGroup.Disable()

            #Apply Value
            Pipete = str(self.OffMan_EditedData[1])
            Group = str(self.OffMan_EditedData[2])
            x = str(self.OffMan_EditedData[3])
            y = str(self.OffMan_EditedData[4])
            z = str(self.OffMan_EditedData[5])
            a = str(self.OffMan_EditedData[6])
            c = str(self.OffMan_EditedData[7])

            self.Cmb_Offset_SelectPipete.SetValue(Pipete)
            self.Cmb_Offset_SelectGroup.SetValue(Group)
            self.Txt_Offset_X.SetValue(x)
            self.Txt_Offset_Y.SetValue(y)
            self.Txt_Offset_Z.SetValue(z)
            self.Txt_Offset_A.SetValue(a)
            self.Txt_Offset_C.SetValue(c)



        else:
            Msgbox = wx.MessageDialog(None,
                                      'No item was Selected, Please Choose(Click) one of the item listed on Datalist Table',
                                      'There is no item to edit', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

            klick = Msgbox.ShowModal()
            return



    def DeleteOffset(self, event=None):
        is_select, selected_data  = self.GetSelectedKoordinate()
        if is_select == True:
            #ToDo QUERY DELETE
            Offset_HeadID = selected_data[0]
            sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_DeleteOffsetMapping',
                                        parameter_list=[Offset_HeadID])
            
            selected_id = selected_data[0]
            self.RefreshOffsetData()
        else:
            Msgbox = wx.MessageDialog(None,
                                      'No item was Selected, Please Choose(Click) one of the item listed on Datalist Table',
                                      'There is no item to edit', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

            klick = Msgbox.ShowModal()
            return

    def RefreshOffsetData(self, event=None):
        #ToDo : QUERY DATA NEW
        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_GetAllOffsetMappingData',
                                        parameter_list=[])
        self.LoadDataToOffsetDv(data=data)

    def SearchGroupOffset(self, event=None):
        search_word = self.Txt_Offset_SearchByGroup.GetValue()
        #ToDo: QUERT SELECT 
        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_SearchOffsetMapping',
                                        parameter_list=[search_word])
        # data = None
        self.LoadDataToOffsetDv(data=data)

    def SortOffsetHeader(self, event):
        if self.Dv_DataOffset.GetItemCount() == 0:
            return

        if self.Rad_Offset_SortAsc.GetValue():
            sort_types = 'ASC'
        elif self.Rad_Offset_SortDesc.GetValue():
            sort_types = 'DESC'

        row_count = self.Dv_DataOffset.GetItemCount()
        view_col_obj = event.GetDataViewColumn()

        # data_column_marking = {0:'running_ID',1:'user_ID',2:'platform_source',3:'crawl_source_type',
        #                        4:'crawl_active_pages',5:'search_input',6:'title_or_description_required_words',
        #                        7:'running_status',8:'record_lastupdate_datetime'}
        
        index_column = view_col_obj.GetModelColumn()
        sort_column = self.Data_DvOffset[index_column][1]
        print(view_col_obj)
        print(index_column)

        data_run_id_lis = []
        for rows in range(row_count):
            id_on_row = int(self.Dv_DataOffset.GetValue(row=rows, col=0))
            data_run_id_lis.append(id_on_row)

        print(str(data_run_id_lis))
        print(str(tuple(data_run_id_lis)))

        # CHANGE  data_run_id_lis to be compatible with sql cast IN ( A TUPLE )
        if len(data_run_id_lis) == 1:
            data_run_id_lis = str(data_run_id_lis).replace('[', '(').replace(']', ')')
        else:
            data_run_id_lis = str(tuple(data_run_id_lis))

        # ToDo Execute SQL To Sort
        # SP_Nxs_CoordinateSort
        sql_result, data, error = self.GetSortedRunningLogOffset(sort_type=sort_types, sort_column=sort_column,
                                                           id_list=data_run_id_lis)
        print(data)
        if sql_result in (None, False):
            # ERROR
            titles = 'Executing Query Error'
            texts = '[Error Query GetSortedRunningLog] \n ' + str(error)
            answer = self.MessageBox(type=1, text=texts, title=titles)
            return

        # APPLY DATA TO DATA VIEW
        self.LoadDataToOffsetDv(data=data)
        pass

        # event.Skip()

    def GetSortedRunningLogOffset(self, sort_type, sort_column, id_list):
        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_OffsetSort',
                                        parameter_list=[id_list,sort_type,sort_column])

        # return (sql_result, data, error)
        if sql_result == False:
                #ToDo MessageBox
                pass
        return (sql_result, data, error)


    def SaveAllOffset(self, event=None):
        self.DestroyOffset()

    def ExitOffsetForm(self, event=None):
        self.DestroyOffset()

    def SetColumnDataViewOffset(self):
        sql_result, data, error = self.GetRunningColumnOffset()
        print(data)
        if sql_result in (None, False):
            # ERROR
            titles = 'Executing Query Error'
            texts = '[Error Query GetRunningColumnPipete] \n ' + str(error)
            answer = self.MessageBox(type=1, text=texts, title=titles)
            return


        self.Data_DvOffset = {}
        col_num = 0
        for column_alias,column in data:
            # col = list(row.values())[0]
            # col = list(row)[0]
            # print (col)
            obj = self.Dv_DataOffset.AppendTextColumn(str(column_alias),width=len(column_alias)*7+20)
            self.Data_DvOffset.update({col_num:[column_alias,column,obj]})
            col_num += 1

    def GetRunningColumnOffset(self):
        sql_result, data, error = self.SQL_SP(sp_name=' SP_Nxs_GetOffsetMapColumnList',
                                        parameter_list=[])

        # return (sql_result, data, error)
        print(data)
        if sql_result == False:
                #ToDo MessageBox
                pass
        return(sql_result, data, error)

    def CheckOffsetDataExistence(self):
        #ToDo : Check Existence of Data in Database

        pass

    def GetAllDataExistedGroup(self):

        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_GetAllGroupExisted',
                                              parameter_list=[])

        # return (sql_result, data, error)
        print(data)
        if sql_result == False:
            # ToDo MessageBox
            pass
        return (data)

    def GetAllDataExistedPipete(self):

        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_GetAllPipeteExisted',
                                              parameter_list=[])

        # return (sql_result, data, error)
        print(data)
        if sql_result == False:
            # ToDo MessageBox
            pass
        return (data)
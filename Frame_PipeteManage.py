# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.dataview


###########################################################################
## Class Frame_PipeteManagement
###########################################################################

class Frame_PipeteManagement(wx.Frame):

    def __init__(self, parent):
        self.Frame_PipeteManage = wx.Frame(parent, id=wx.ID_ANY, title=u"FORM INPUT PIPETE", pos=wx.DefaultPosition,
                          size=wx.Size(530, 514), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        self.Frame_PipeteManage.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.Frame_PipeteManage.SetBackgroundColour(wx.Colour(255, 255, 255))

        fgSizer1 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer1.SetFlexibleDirection(wx.BOTH)
        fgSizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText1 = wx.StaticText(self.Frame_PipeteManage, wx.ID_ANY, u"MANAGE PIPETE", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1.Wrap(-1)
        self.m_staticText1.SetFont(wx.Font(14, 74, 90, 92, False, "Calibri"))

        fgSizer1.Add(self.m_staticText1, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_panel1 = wx.Panel(self.Frame_PipeteManage, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel1.SetBackgroundColour(wx.Colour(255, 255, 255))

        fgSizer2 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer2.SetFlexibleDirection(wx.BOTH)
        fgSizer2.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText2 = wx.StaticText(self.m_panel1, wx.ID_ANY, u"Input Form", wx.DefaultPosition, wx.DefaultSize,
                                           0)
        self.m_staticText2.Wrap(-1)
        self.m_staticText2.SetFont(wx.Font(9, 74, 90, 92, False, "Arial"))

        fgSizer2.Add(self.m_staticText2, 0, wx.ALL, 5)

        self.m_panel3 = wx.Panel(self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        gbSizer1 = wx.GridBagSizer(0, 0)
        gbSizer1.SetFlexibleDirection(wx.BOTH)
        gbSizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText3 = wx.StaticText(self.m_panel3, wx.ID_ANY, u"Pipete Name", wx.DefaultPosition, wx.DefaultSize,
                                           0)
        self.m_staticText3.Wrap(-1)
        gbSizer1.Add(self.m_staticText3, wx.GBPosition(0, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.Txt_PipeteName = wx.TextCtrl(self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                          0)
        self.Txt_PipeteName.SetMinSize(wx.Size(250, -1))

        gbSizer1.Add(self.Txt_PipeteName, wx.GBPosition(0, 1), wx.GBSpan(1, 3), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_staticText4 = wx.StaticText(self.m_panel3, wx.ID_ANY, u"Configuration Used", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.m_staticText4.Wrap(-1)
        gbSizer1.Add(self.m_staticText4, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        Cmb_ConfigurasiChoices = []
        self.Cmb_Configurasi = wx.ComboBox(self.m_panel3, wx.ID_ANY, u"- Choose -", wx.DefaultPosition, wx.DefaultSize,
                                           Cmb_ConfigurasiChoices, 0)
        self.Cmb_Configurasi.SetMinSize(wx.Size(300, -1))

        gbSizer1.Add(self.Cmb_Configurasi, wx.GBPosition(1, 1), wx.GBSpan(1, 3), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_staticText5 = wx.StaticText(self.m_panel3, wx.ID_ANY, u"Config Name", wx.DefaultPosition, wx.DefaultSize,
                                           0)
        self.m_staticText5.Wrap(-1)
        gbSizer1.Add(self.m_staticText5, wx.GBPosition(2, 0), wx.GBSpan(1, 1),
                     wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_staticText6 = wx.StaticText(self.m_panel3, wx.ID_ANY, u"Config ID", wx.DefaultPosition, wx.DefaultSize,
                                           0)
        self.m_staticText6.Wrap(-1)
        gbSizer1.Add(self.m_staticText6, wx.GBPosition(2, 2), wx.GBSpan(1, 1),
                     wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.Txt_ConfigID = wx.TextCtrl(self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                        wx.TE_READONLY)
        self.Txt_ConfigID.SetMinSize(wx.Size(70, -1))

        gbSizer1.Add(self.Txt_ConfigID, wx.GBPosition(2, 3), wx.GBSpan(1, 1),
                     wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Txt_ConfigName = wx.TextCtrl(self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                          wx.TE_READONLY)
        self.Txt_ConfigName.SetMinSize(wx.Size(200, -1))

        gbSizer1.Add(self.Txt_ConfigName, wx.GBPosition(2, 1), wx.GBSpan(1, 1),
                     wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.m_panel3.SetSizer(gbSizer1)
        self.m_panel3.Layout()
        gbSizer1.Fit(self.m_panel3)
        fgSizer2.Add(self.m_panel3, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL | wx.EXPAND, 5)

        self.m_panel1.SetSizer(fgSizer2)
        self.m_panel1.Layout()
        fgSizer2.Fit(self.m_panel1)
        fgSizer1.Add(self.m_panel1, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel2 = wx.Panel(self.Frame_PipeteManage, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer3 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer3.SetFlexibleDirection(wx.BOTH)
        fgSizer3.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        bSizer2 = wx.BoxSizer(wx.HORIZONTAL)

        self.Cmd_NewPipete = wx.Button(self.m_panel2, wx.ID_ANY, u"New", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer2.Add(self.Cmd_NewPipete, 0, wx.ALL, 5)

        self.Cmd_EditPipete = wx.Button(self.m_panel2, wx.ID_ANY, u"Edit", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer2.Add(self.Cmd_EditPipete, 0, wx.ALL, 5)

        self.Cmd_DeletePipete = wx.Button(self.m_panel2, wx.ID_ANY, u"Delete", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer2.Add(self.Cmd_DeletePipete, 0, wx.ALL, 5)

        self.Cmd_RefreshPipeteData = wx.Button(self.m_panel2, wx.ID_ANY, u"Refresh", wx.DefaultPosition, wx.DefaultSize,
                                               0)
        bSizer2.Add(self.Cmd_RefreshPipeteData, 0, wx.ALL, 5)

        fgSizer3.Add(bSizer2, 1, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL | wx.EXPAND, 5)

        self.m_panel2.SetSizer(fgSizer3)
        self.m_panel2.Layout()
        fgSizer3.Fit(self.m_panel2)
        fgSizer1.Add(self.m_panel2, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.m_panel4 = wx.Panel(self.Frame_PipeteManage, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel4.SetBackgroundColour(wx.Colour(255, 255, 255))

        fgSizer4 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer4.SetFlexibleDirection(wx.BOTH)
        fgSizer4.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText7 = wx.StaticText(self.m_panel4, wx.ID_ANY, u"Data Pipete", wx.DefaultPosition, wx.DefaultSize,
                                           0)
        self.m_staticText7.Wrap(-1)
        self.m_staticText7.SetFont(wx.Font(9, 74, 90, 92, False, "Arial"))

        fgSizer4.Add(self.m_staticText7, 0, wx.ALL, 5)

        self.Dv_PipeteData = wx.dataview.DataViewListCtrl(self.m_panel4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                                          0)
        self.Dv_PipeteData.SetMinSize(wx.Size(500, 150))

        fgSizer4.Add(self.Dv_PipeteData, 0, wx.ALL, 5)

        self.m_panel4.SetSizer(fgSizer4)
        self.m_panel4.Layout()
        fgSizer4.Fit(self.m_panel4)
        fgSizer1.Add(self.m_panel4, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel5 = wx.Panel(self.Frame_PipeteManage, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer5 = wx.FlexGridSizer(0, 2, 0, 0)
        fgSizer5.SetFlexibleDirection(wx.BOTH)
        fgSizer5.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        bSizer3 = wx.BoxSizer(wx.HORIZONTAL)

        self.Cmd_SavePipeteData = wx.Button(self.m_panel5, wx.ID_ANY, u"OK", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer3.Add(self.Cmd_SavePipeteData, 0, wx.ALL, 5)

        self.Cmd_ExitPipeteForm = wx.Button(self.m_panel5, wx.ID_ANY, u"EXIT", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer3.Add(self.Cmd_ExitPipeteForm, 0, wx.ALL, 5)

        fgSizer5.Add(bSizer3, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.m_panel5.SetSizer(fgSizer5)
        self.m_panel5.Layout()
        fgSizer5.Fit(self.m_panel5)
        fgSizer1.Add(self.m_panel5, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Frame_PipeteManage.SetSizer(fgSizer1)
        self.Frame_PipeteManage.Layout()

        self.Frame_PipeteManage.Centre(wx.BOTH)

        # Connect Events
        self.Cmb_Configurasi.Bind(wx.EVT_COMBOBOX, self.GetDetailedInfoConfigs)
        self.Cmd_NewPipete.Bind(wx.EVT_BUTTON, self.NewPipetee)
        self.Cmd_EditPipete.Bind(wx.EVT_BUTTON, self.EditPipetee)
        self.Cmd_DeletePipete.Bind(wx.EVT_BUTTON, self.DeletePipetee)
        self.Cmd_RefreshPipeteData.Bind(wx.EVT_BUTTON, self.RefreshPipete)
        self.Cmd_SavePipeteData.Bind(wx.EVT_BUTTON, self.SaveAllPipete)
        self.Cmd_ExitPipeteForm.Bind(wx.EVT_BUTTON, self.ExitPipeteMan)

        self.Cmd_ExitPipeteForm.Hide()

        #Show
        self.StarterPipeteManage()
        self.Frame_PipeteManage.Show()
        # self.Cmd_SavePipeteData.Hide()

    def __del__(self):
        pass

    # ADD ON
    def DestroyPipeteForm(self):
        self.Frame_PipeteManage.Destroy()

    # def StarterTablePipete(self):
    #     list_col = ['ID', 'Name', 'ConfigID']
    #     for judul in list_col:
    #         self.Dv_PipeteData.AppendTextColumn(judul, width=len(str(judul)) * 7)

    def StarterPipeteManage(self):
        self.ResetPipeteForm()
        self.DisablePipeteForm()
        self.SetColumnDataViewPipete()
        self.RefreshPipete()

    def DisablePipeteForm(self):
        self.Txt_PipeteName.Disable()
        self.Cmb_Configurasi.Disable()
        self.Txt_ConfigID.Disable()
        self.Txt_ConfigName.Disable()
        # self.Cmd_SavePipeteData.Disable()

    def EnablePipeteForm(self):
        self.Cmd_NewPipete.Disable()
        self.Cmd_EditPipete.Disable()
        self.Cmd_DeletePipete.Disable()

        # Get All Config
        data_conf = self.GetAllDataConfig()
        self.Cmb_Configurasi.Clear()
        for row in data_conf:
            col = str(row[0])
            self.Cmb_Configurasi.AppendItems(col)

        self.Txt_PipeteName.Enable()
        self.Cmb_Configurasi.Enable()
        self.Txt_ConfigID.Enable()
        self.Txt_ConfigName.Enable()
        # self.Cmd_SavePipeteData.Enable()

        self.Cmd_NewPipete.Enable()
        self.Cmd_EditPipete.Enable()
        self.Cmd_DeletePipete.Enable()




    def ReLoadDataPipete(self):
        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_GetAllPipeteData',
                                        parameter_list=[])
            
        if sql_result == False:
            #ToDo : MessageBox
            return

        self.LoadDataPipete(data=data)


    def LoadDataPipete(self, data):
        self.Dv_PipeteData.DeleteAllItems()
        if data == None:
            return
        for row in data:
            if len(list(row)) == self.Dv_PipeteData.GetColumnCount():
                self.Dv_PipeteData.AppendItem(row)


    def ResetPipeteForm(self):
        self.Txt_ConfigID.SetValue('')
        self.Txt_ConfigName.SetValue('')
        self.Txt_PipeteName.SetValue('')
        self.Cmb_Configurasi.SetValue('- Choose -')

    #ToDo : Listing Config ID and NAME
    def LoadListConfigurasi(self):
        #ToDo : Make SP SELECT CONFIG ID AND CONFIG NAME
        pass

    def CompareConfigurasi(self, name, data):
        config_id = data[name]
        return(config_id)

    def ViewDetailConfig(self, name, data):
        config_id = self.CompareConfigurasi(name=name, data=data)
        self.Txt_ConfigName.SetValue(str(name))
        self.Txt_ConfigID.SetValue(str(config_id))

    def CheckSelectedRowPipete(self):
        # Check Selected Row
        data_count = self.Dv_PipeteData.GetItemCount()
        selecteds = False
        for item_num in range(data_count):
            if self.Dv_PipeteData.IsRowSelected(row=item_num) == True:
                selecteds = True

        if selecteds == False:
            Msgbox = wx.MessageDialog(None,
                                      'No item was Selected, Please Choose(Click) one of the item listed on Datalist Table',
                                      'There is no item to edit', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

            klick = Msgbox.ShowModal()
            return(False, None)
        else:
            selected_row = self.Dv_PipeteData.GetSelectedRow()
            return(True, selected_row)

    # Virtual event handlers, overide them in your derived class
    def NewPipetee(self, event=None):
        print('======================================')
        print(self.Cmd_NewPipete.GetLabel())
        if self.Cmd_NewPipete.GetLabel() == 'SAVE':
            #ToDo : QUERY SAVE
            print('SAVING')

            self.SavingNewPipete()


            self.Cmd_NewPipete.SetLabel('New')
            self.Cmd_EditPipete.SetLabel('Edit')
            self.Cmd_EditPipete.Show()
            self.Cmd_DeletePipete.Show()
            self.DisablePipeteForm()
            self.ResetPipeteForm()
            self.RefreshPipete()
            return

        elif self.Cmd_NewPipete.GetLabel() == 'UPDATE':
            #ToDo : QUERY UPDATE

            print ('UPDATING')
            self.UpdatingPipete()

            self.Cmd_NewPipete.SetLabel('New')
            self.Cmd_EditPipete.SetLabel('Edit')
            self.Cmd_EditPipete.Show()
            self.Cmd_DeletePipete.Show()
            self.DisablePipeteForm()
            self.ResetPipeteForm()
            self.RefreshPipete()
            return

        

        #New Special Case Changes
        self.Cmd_NewPipete.SetLabel('SAVE')
        self.Cmd_EditPipete.SetLabel('CANCEL')    
        # self.Cmd_EditPipete.Hide()
        self.Cmd_DeletePipete.Hide()

        self.EnablePipeteForm()
        self.ResetPipeteForm()

    def SavingNewPipete(self):
        pipete_name = self.Txt_PipeteName.GetValue()
        config_used = self.Txt_ConfigID.GetValue()

        # already_exist = self.CheckPipeteDataExistence()
        already_exist = False
        if already_exist:
            #ToDo : MessageBox
            return
        else:
            Msgbox = wx.MessageDialog(None,
                                      'Are You Sure Want To Save?',
                                      'Saving An Item', wx.OK | wx.CANCEL | wx.ICON_WARNING | wx.STAY_ON_TOP)

            klick = Msgbox.ShowModal()

            if klick == wx.ID_OK:
                #ToDo : Query Save Pipete
                sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_SaveNewPipete',
                                            parameter_list=[str(pipete_name), int(config_used)])

                if sql_result == False:
                    Msgbox = wx.MessageDialog(None,
                                              'Failed To Save',
                                              'Save Failed!', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                else:
                    Msgbox = wx.MessageDialog(None,
                                              'Item Saved',
                                              'Save Success!', wx.OK | wx.ICON_INFORMATION | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()

    def UpdatingPipete(self):
        updated_id = self.SelectedID_Pipete
        pipete_name = self.Txt_PipeteName.GetValue()
        config_used = self.Txt_ConfigID.GetValue()
        
        already_exist = self.CheckPipeteDataExistence()

        if already_exist:
            #ToDo : MessageBox
            return
        else:
            Msgbox = wx.MessageDialog(None,
                                      'Are You Sure Want To Update?',
                                      'Updating An Item', wx.OK | wx.CANCEL | wx.ICON_WARNING | wx.STAY_ON_TOP)

            klick = Msgbox.ShowModal()

            if klick == wx.ID_OK:
                #ToDo : Query Save Pipete
                sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_UpdateDataPipete',
                                            parameter_list=[int(updated_id), str(pipete_name), int(config_used)])

                if sql_result == False:
                    Msgbox = wx.MessageDialog(None,
                                              'Failed To Update',
                                              'Update Failed!', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                else:
                    Msgbox = wx.MessageDialog(None,
                                              'Item Updated',
                                              'Update Success!', wx.OK | wx.ICON_INFORMATION | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()


    def EditPipetee(self, event=None):
        if self.Cmd_EditPipete.GetLabel() == 'CANCEL':
            
            self.SelectedID_Pipete = None

            self.Cmd_NewPipete.SetLabel('New')
            self.Cmd_EditPipete.SetLabel('Edit')
            self.Cmd_EditPipete.Show()
            self.Cmd_DeletePipete.Show()
            self.DisablePipeteForm()
            self.ResetPipeteForm()
            self.RefreshPipete()
            return

        #New Special Case Changes
        is_selected, row = self.CheckSelectedRowPipete()
        if is_selected:
            self.Cmd_NewPipete.SetLabel('UPDATE')
            self.Cmd_EditPipete.SetLabel('CANCEL')
            # self.Cmd_EditPipete.Hide()
            self.Cmd_DeletePipete.Hide()

            self.EnablePipeteForm()
            self.ResetPipeteForm()

            # Apply data to Form
            self.ApplyDataToFormPipete(selected_row=row)
            self.SelectedID_Pipete = self.Dv_PipeteData.GetValue(col=0, row=row)

    def ApplyDataToFormPipete(self, selected_row):
        # column_count = self.Dv_PipeteData.GetColumnCount()
        # data_container = []
        # for col in range(column_count):
        #     col_data = self.Dv_PipeteData.GetValue(col=col,row=selected_row)
        # self.RereshComboConfigPipete()
        self.Txt_PipeteName.SetValue(str(self.Dv_PipeteData.GetValue(col=1,row=selected_row)))
        self.Cmb_Configurasi.SetValue(str(self.Dv_PipeteData.GetValue(col=2,row=selected_row)))
        self.Txt_ConfigName.SetValue(str(self.Dv_PipeteData.GetValue(col=2,row=selected_row)))
        id = self.GetMoreConfInfo(name=self.Dv_PipeteData.GetValue(col=2,row=selected_row))
        self.Txt_ConfigID.SetValue(str(id[0][0]))

    def RereshComboConfigPipete(self):
        data = self.Get()
        self.Cmb_Configurasi.Clear()
        for items in data:
            self.Cmb_Configurasi.AppendItems(str(items))

    def DeletePipetee(self, event=None):
        is_selected, row = self.CheckSelectedRowPipete()

        if is_selected == False:
            # Msgbox = wx.MessageDialog(None, 'No item was Selected, Please Choose(Click) one of the item listed on Datalist Table',
            #                           'There is no item to edit', wx.OK |  wx.ICON_ERROR | wx.STAY_ON_TOP)
            #
            # klick = Msgbox.ShowModal()
            return

        else:
            Msgbox = wx.MessageDialog(None,
                                      'Are You Sure Want To Delete?',
                                      'Deleting An Item', wx.OK | wx.CANCEL | wx.ICON_WARNING | wx.STAY_ON_TOP)

            klick = Msgbox.ShowModal()

            if klick == wx.ID_OK:
                pipete_id = self.Dv_PipeteData.GetValue(row=row, col=0)

                #ToDo : Delete Query with PipeteID
                sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_DeleteDataPipete',
                                                parameter_list=[pipete_id])

                if sql_result == False:
                    Msgbox = wx.MessageDialog(None,
                                              'Failed To Delete',
                                              'Delete Failed!', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                else:
                    self.RefreshPipete()

                    Msgbox = wx.MessageDialog(None,
                                              'Item Was Deleted',
                                              'Delete Success!', wx.OK | wx.ICON_INFORMATION | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()



    def RefreshPipete(self, event=None):
        self.ReLoadDataPipete()

    def SaveAllPipete(self, event=None):
        self.DestroyPipeteForm()

    def ExitPipeteMan(self, event=None):
        self.DestroyPipeteForm()

    def SetColumnDataViewPipete(self):
        sql_result, data, error = self.GetRunningColumnPipete()
        print(data)
        if sql_result in (None, False):
            # ERROR
            titles = 'Executing Query Error'
            texts = '[Error Query GetRunningColumnPipete] \n ' + str(error)
            answer = self.MessageBox(type=1, text=texts, title=titles)
            return


        # self.Data_DvPipete = {}
        col_num = 0
        for row in data:
            col = list(row)[0]
            # print (col)
            obj = self.Dv_PipeteData.AppendTextColumn(str(col),width=len(col)*7+20)
            # self.Data_DvPipete.update({col_num:[col,obj]})
            col_num += 1

    def GetRunningColumnPipete(self):
        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_GetPipeteColumnList',
                                        parameter_list=[])

        #
        print(data)
        
        if sql_result == False:
                #ToDo MessageBox
                pass
        return (sql_result, data, error)
        
    def CheckPipeteDataExistence(self):
        #ToDo : Check Existence of Data in Database
        pass

    def GetAllDataConfig(self):
        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_GetAllExistedConfig',
                                        parameter_list=[])

        #
        print(data)
        
        if sql_result == False:
                #ToDo MessageBox
                pass
        return (data)

    def GetDetailedInfoConfigs(self,event=None):
        selected = str(self.Cmb_Configurasi.GetValue())

        if selected in ('','- Choose -'):
            #ToDo : Message Box
            return


        data = self.GetMoreConfInfo(name=selected)

        conf_id, conf_name = data[0]
        self.Txt_ConfigID.SetValue(str(conf_id))
        self.Txt_ConfigName.SetValue(str(conf_name))


    def GetMoreConfInfo(self, name):
        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_GetDetailConfigInfo',
                                              parameter_list=[str(name)])

        #
        print(data)

        if sql_result == False:
            # ToDo MessageBox
            pass
        return (data)


    
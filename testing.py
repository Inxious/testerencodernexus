# def Output(move):
#     ver = 0
#     hor = 0
#     for string in list(move):
#         if str(string).upper() == 'E':
#             hor += 1
#         elif str(string).upper() == 'N':
#             ver += 1
#         elif str(string).upper() == 'W':
#             hor -= 1
#         elif str(string).upper() == 'S':
#             ver -= 1
#     return(int(abs(ver)+abs(hor)))
#
# if __name__ == '__main__':
#     inp = str(input('Coordinate :'))
#     print(Output(inp))
#     while inp.lower() != 'exit':
#         inp = input('Coordinate :')
#         print(Output(inp))

# import sys
# for line in sys.stdin:
#     ver = 0
#     hor = 0
#     for string in list(line):
#         if str(string).upper() == 'E':
#             hor += 1
#         elif str(string).upper() == 'N':
#             ver += 1
#         elif str(string).upper() == 'W':
#             hor -= 1
#         elif str(string).upper() == 'S':
#             ver -= 1
#     print(int(abs(ver)+abs(hor)))


import sys
m=0
for l in sys.stdin:
    b = l.split(' ')
    try:
        int(b[-1:][0])
    except Exception:
        del b[-1:]
    b = [int(i) for i in b]
    if len(b) == 1 and int(b[0]) >= 1 and int(b[0]) <= 1000:
        m = int(l)
        continue
    else:
        if len(b) == m and m != 0 and max(b) >= 1 and max(b) <= 100:
            del b[b.index(min(b))]
            print (sum(b))

--GROUPING KOORDINAT
-- SQL QUERY GET ALL DATA KOORDINATE
CREATE 
--ALTER
PROCEDURE SP_Nxs_GetDataCoordinate
AS
BEGIN
	SELECT	k.KoordinatID,
					k.MK_Name,
					g.Group_Name,
					k.MK_Koor_X,
					k.MK_Koor_Y,
					k.MK_Koor_Z,
					k.MK_Koor_A,
					k.MK_Koor_C
	FROM M_Koordinat AS k
	LEFT JOIN M_KoordinatGroup AS g ON k.Koor_Group = g.GroupID
	WHERE k.IsActive = 1
	ORDER BY KoordinatID ASC
END

EXEC SP_Nxs_GetDataCoordinate

-- SQL QUERY SELECT SEARCHED QUERY

CREATE 
--ALTER
PROCEDURE SP_Nxs_SearchCoordinate
@Koor_Name AS VARCHAR(250)
AS BEGIN
	SELECT	k.KoordinatID,
					k.MK_Name,
					g.Group_Name,
					k.MK_Koor_X,
					k.MK_Koor_Y,
					k.MK_Koor_Z,
					k.MK_Koor_A,
					k.MK_Koor_C
	FROM M_Koordinat AS k
	LEFT JOIN M_KoordinatGroup AS g ON k.Koor_Group = g.GroupID
	
	WHERE MK_Name LIKE '%'+@Koor_Name+'%' AND
	k.IsActive = 1
	--ORDER BY MK_Name ASC
END



-- Execute SQL To Sort
--CREATE PROCEDURE SP_Nxs_CoordinateSort
--@Mode AS INTEGER
--AS
--BEGIN
--SELECT	KoordinatID,
--			MK_Name,
--			MK_Koor_X,
--			MK_Koor_Y,
--			MK_Koor_Z,
--			MK_Koor_A,
--			MK_Koor_C,
--			MK_FLIP1,
--			MK_FLIP2,
--			MK_FLIP3,
--			MK_BLOW,
--			MK_HEAT,
--			MK_Microstep
--	FROM M_Koordinat
--	ORDER BY 
--		CASE WHEN @Mode = 1 THEN KoordinatID END ASC,
--		CASE WHEN @Mode = 0 THEN KoordinatID END DESC
--END

-- QUERY DATA GROUPING
CREATE PROCEDURE SP_Nxs_GetAllExistedGroup
AS 
BEGIN
	SELECT	--GroupID,
			Group_Name
	FROM M_KoordinatGroup
	ORDER BY GroupID ASC
END


-- QUERY CHECK EXISTENCE OF GROUP NAME
--CREATE PROCEDURE SP_Nxs_CheckGroupName
--@Name_Group AS Varchar(MAX)
--AS
--BEGIN
--	SELECT Group_Name
--	FROM M_KoordinatGroup
--	WHERE Group_Name = @Name_Group
--END


-- ToDO : QUERY DATA GROUPING ( Save Data Grouping )
CREATE 
--ALTER
PROCEDURE SP_Nxs_UpdateCoorGroup
@Coor_ID AS INT,
@Name AS VARCHAR(250)
AS 
BEGIN
-- CHECK EXIST
IF EXISTS(SELECT 1 FROM M_KoordinatGroup WHERE Group_Name = @Name)
	BEGIN
	
	UPDATE M_Koordinat
	SET Koor_Group = (SELECT GroupID FROM M_KoordinatGroup WHERE Group_Name = @Name)
	WHERE KoordinatID = @Coor_ID
	
	SELECT 'SUCCESS'AS [RESULT], 'SAVED' AS [DO]
	END
ELSE
	BEGIN
	--INSERT INTO M_KoordinatGroup(Group_Name,Note)
	--VALUES (@Name,'')
	
	--UPDATE M_Koordinat
	--SET Koor_Group = (SELECT GroupID FROM M_KoordinatGroup WHERE Group_Name = @Name)
	--SET Koor_Group = (SELECT SCOPE_IDENTITY())
	--WHERE KoordinatIID = @Coor_ID
	
	--SELECT 'SUCCESS'AS [RESULT], 'SAVED' AS [DO]
	SELECT 'ERROR' AS [RESULT], 'CANCELED' AS [DO]
	END
END

-- ToDO : QUERY DATA GROUPING NEW
CREATE 
--ALTER
PROCEDURE SP_Nxs_AddNewCoorGroup
@Coor_ID AS INT,
@Name AS VARCHAR(250)

AS 
BEGIN

-- CHECK EXIST
IF EXISTS(SELECT 1 FROM M_KoordinatGroup WHERE Group_Name = @Name)
	BEGIN
	SELECT 'ERROR' AS [RESULT], 'CANCELED' AS [DO]
	END
ELSE
	BEGIN
	INSERT INTO M_KoordinatGroup(Group_Name,Note)
	VALUES (@Name,'')
	
	UPDATE M_Koordinat 
	--SET Koor_Group = (SELECT GroupID FROM M_KoordinatGroup WHERE Group_Name = @Name)
	SET Koor_Group = (SELECT SCOPE_IDENTITY())
	WHERE KoordinatID = @Coor_ID
	
	SELECT 'SUCCESS'AS [RESULT], 'SAVED' AS [DO]
	END
END

SELECT * FROM M_KoordinatGroup


CREATE 
--ALTER
PROCEDURE SP_Nxs_CoordinateSort
@ID_List AS TEXT,
@Sorte AS VARCHAR(250),
@Column AS TEXT
AS
BEGIN
	EXECUTE( 'SELECT	k.KoordinatID,
					k.MK_Name,
					g.Group_Name,
					k.MK_Koor_X,
					k.MK_Koor_Y,
					k.MK_Koor_Z,
					k.MK_Koor_A,
					k.MK_Koor_C
				FROM M_Koordinat AS k
				LEFT JOIN M_KoordinatGroup AS g ON k.Koor_Group = g.GroupID
				WHERE k.KoordinatID IN '+ @ID_List +' AND
				k.IsActive = 1
				ORDER BY '+ @Column +' '+ @Sorte)
END

 -- SP_Nxs_GetCoorColumnList
CREATE 
--ALTER
PROCEDURE SP_Nxs_GetKoorGroupColumnList
AS
BEGIN
	DECLARE @Tab TABLE ([COLUMNS_ALIAS] Varchar(MAX), [COLUMNS] Varchar(MAX))
	INSERT INTO @Tab
	VALUES
		('KoordinatID','k.KoordinatID'),
		('Nama Koordinat','k.MK_Name'),
		('Group Koordinat','g.Group_Name'),
		('Koordinat X','k.MK_Koor_X'),
		('Koordinat Y','k.MK_Koor_Y'),
		('Koordinat Z','k.MK_Koor_Z'),
		('Koordinat A','k.MK_Koor_A'),
		('Koordinat C','k.MK_Koor_C')
	SELECT * FROM @Tab
	
	
END


SELECT * FROM M_OffsetMappingHeader
	SELECT * FROM M_OffsetMappingDetail
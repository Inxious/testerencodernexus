
import time

#Threads For Board 1

class ThreadsB2():
    def  __init__(self):
        print 'THREAD 2 JALAN'

        lasterror = ''
        self.vEncoder_Connection = "OFF"
        self.vEncoder_Reading = "OFF"
        self.Encoder_ReadTypes = ''
        result = ''
        while True:

            time.sleep(0.3)
            if self.vEncoder_Connection == "ON":

                if self.vEncoder_Reading == "ON":

                    #FOR RUNNING PROCESS
                    if self.Encoder_ReadTypes == 'PROSES':
                        #Reset Vars
                        self.Encoder_Reading_Result = ''

                        # IF Per Motor Check
                        try:
                            # Check Needed Var
                            self.Encoder_Motor
                            self.Encoder_Feedback

                            if self.Encoder_List.get(self.Encoder_Motor) in (None,) or self.Encoder_Feedback in (None,):
                                raise Exception('Motor Not Listed on Encoder_List')
                        except Exception as e:
                            print (e)
                            self.vEncoder_Reading = 'OFF'
                            print('Force Stop Encoder Reading Cause > ' + str(e))
                            # ToDo Error Handler

                        # Check Serial
                        try:
                            alive = self.ser2.is_open
                            if alive == False:
                                raise Exception
                        except Exception as e:
                            print e
                            print ('Serial Encoder is Not Connected')
                            self.vEncoder_Reading  = 'OFF'
                            # ToDo : Reconnect Procedure

                        # READING PROCESS
                        vDoneRead = False
                        result = ''

                        # Feedback
                        feedback = self.Encoder_Feedback
                        # print('========================\n')
                        # print(self.Encoder_Feedback)
                        # print('========================\n')
                        marker = self.Encoder_Marker
                        partfeedback = str(feedback).split(str(marker))

                        # Time Limit
                        time_limit = 25

                        # WRITE COMMAND TO READ
                        command = 'NEXUS=' + str(self.Encoder_Command) + '~$' + '\n'
                        try:
                            self.ser2.write(command.encode())
                            print command
                        except Exception as e:
                            print (e)
                        else:
                            logs = str(str(command))
                            self.TEndUpdateLog(4, logs)


                        # Time Start
                        start_read = self.CCurTime3()

                        print ("Reading Encoder ~~")
                        result = ''
                        data = ''
                        match = ''
                        while vDoneRead != True:

                            time.sleep(0.5)

                            # Read Encoder
                            print 'read'
                            print result
                            print data
                            try:
                                data = self.ser2.readline()
                                # self.ser2.flushOutput()
                                # print data
                            except Exception as e:
                                print e
                            else:
                                # If empty
                                if data == '' or len(data) == 0:
                                    result = ''
                                    continue
                                # If read until endline
                                # elif '\n' not in data:
                                #    result += str(data)
                                #    print result
                                else:
                                    result += str(data)
                                    print result

                            for part in partfeedback:
                                located = result.find(str(part))
                                print (result)
                                print (partfeedback)
                                if located in (-1,):
                                    match = False
                                else:
                                    match = True

                            # Time Save
                            time_now = self.CCurTime3()
                            time_passed = time_now - start_read

                            # If Over The limit time
                            if float(time_passed.total_seconds()) > float(time_limit):
                                vDoneRead = True
                            # IF match
                            if match:
                                vDoneRead = True

                        print ("Reading Done ~~")
                        logs = str(str(result))
                        self.TEndUpdateLog(2, logs)
                        print (str(result))

                        # Separate Format
                        spliter = '='
                        part = result.split(spliter)
                        lenmust = 2

                        if len(part) != lenmust:
                            print('Data Wrong')
                            result = ''
                        else:
                            motors = part[0]
                            values = part[1]
                            # Remove #
                            values = list(values)
                            values.remove('#')
                            # Rearange to string
                            values = ''.join(values)
                            result = values

                        # Validate if it was the right motor
                        # ToDo : IF only NECESSARY

                        # Filter
                        if result in ('',):
                            self.Encoder_Reading_Result = ''
                            self.Encoder_Reading_Status = 'FAILED'
                        else:
                            self.Encoder_Reading_Result = result
                            self.Encoder_Reading_Status = 'SUCCESS'

                        # self.ser2.flushOutput()
                        # self.ser2.flushInput()
                        self.Encoder_ReadTypes = ''

                    else:

                        try:
                            data = str(self.ser2.readline())
                        except Exception as e:
                            print (e)
                            data = e

                        if data:
                            result += data
                            if '\n' in data:
                                if result:
                                    final = result
                                    self.TEndUpdateLog(2, final)
                                    result = ''
                else:
                    result = ''


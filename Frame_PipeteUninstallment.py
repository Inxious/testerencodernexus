# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.dataview


###########################################################################
## Class Frame_PipeteManagementUninstall
###########################################################################

class Frame_PipeteManagementUninstall(wx.Frame):

    def __init__(self, parent):
        self.Frame_PipeteUninstallment = wx.Frame( parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition,
                          size=wx.Size(533, 511), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        self.Frame_PipeteUninstallment.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.Frame_PipeteUninstallment.SetBackgroundColour(wx.Colour(255, 255, 255))

        fgSizer1 = wx.FlexGridSizer(5, 1, 0, 0)
        fgSizer1.SetFlexibleDirection(wx.BOTH)
        fgSizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText1 = wx.StaticText(self.Frame_PipeteUninstallment, wx.ID_ANY, u"MANAGE PIPETE UNINSTALLATION", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.m_staticText1.Wrap(-1)
        self.m_staticText1.SetFont(wx.Font(14, 74, 90, 92, False, "Calibri"))

        fgSizer1.Add(self.m_staticText1, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_panel1 = wx.Panel(self.Frame_PipeteUninstallment, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel1.SetBackgroundColour(wx.Colour(255, 255, 255))

        fgSizer2 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer2.SetFlexibleDirection(wx.BOTH)
        fgSizer2.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText2 = wx.StaticText(self.m_panel1, wx.ID_ANY, u"Input Form", wx.DefaultPosition, wx.DefaultSize,
                                           0)
        self.m_staticText2.Wrap(-1)
        self.m_staticText2.SetFont(wx.Font(9, 74, 90, 92, False, "Arial"))

        fgSizer2.Add(self.m_staticText2, 0, wx.ALL, 5)

        self.m_panel3 = wx.Panel(self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        gbSizer1 = wx.GridBagSizer(0, 0)
        gbSizer1.SetFlexibleDirection(wx.BOTH)
        gbSizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText3 = wx.StaticText(self.m_panel3, wx.ID_ANY, u"Pipete Name", wx.DefaultPosition, wx.DefaultSize,
                                           0)
        self.m_staticText3.Wrap(-1)
        gbSizer1.Add(self.m_staticText3, wx.GBPosition(0, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        Cmb_PipeteListUninstallChoices = []
        self.Cmb_PipeteListUninstall = wx.ComboBox(self.m_panel3, wx.ID_ANY, u"- Choose -", wx.DefaultPosition,
                                                   wx.DefaultSize, Cmb_PipeteListUninstallChoices, 0)
        self.Cmb_PipeteListUninstall.SetMinSize(wx.Size(300, -1))

        gbSizer1.Add(self.Cmb_PipeteListUninstall, wx.GBPosition(0, 1), wx.GBSpan(1, 3), wx.ALL, 5)

        self.m_staticText4 = wx.StaticText(self.m_panel3, wx.ID_ANY, u"Configuration Used", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.m_staticText4.Wrap(-1)
        gbSizer1.Add(self.m_staticText4, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        Cmb_ConfigurasiUninstallChoices = []
        self.Cmb_ConfigurasiUninstall = wx.ComboBox(self.m_panel3, wx.ID_ANY, u"- Choose -", wx.DefaultPosition,
                                                    wx.DefaultSize, Cmb_ConfigurasiUninstallChoices, 0)
        self.Cmb_ConfigurasiUninstall.SetMinSize(wx.Size(300, -1))

        gbSizer1.Add(self.Cmb_ConfigurasiUninstall, wx.GBPosition(1, 1), wx.GBSpan(1, 3),
                     wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_staticText5 = wx.StaticText(self.m_panel3, wx.ID_ANY, u"Config Name", wx.DefaultPosition, wx.DefaultSize,
                                           0)
        self.m_staticText5.Wrap(-1)
        gbSizer1.Add(self.m_staticText5, wx.GBPosition(2, 0), wx.GBSpan(1, 1),
                     wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.m_staticText6 = wx.StaticText(self.m_panel3, wx.ID_ANY, u"Config ID", wx.DefaultPosition, wx.DefaultSize,
                                           0)
        self.m_staticText6.Wrap(-1)
        gbSizer1.Add(self.m_staticText6, wx.GBPosition(2, 2), wx.GBSpan(1, 1),
                     wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_RIGHT, 5)

        self.Txt_ConfigIDUninstall = wx.TextCtrl(self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                 wx.DefaultSize, wx.TE_READONLY)
        self.Txt_ConfigIDUninstall.SetMinSize(wx.Size(70, -1))

        gbSizer1.Add(self.Txt_ConfigIDUninstall, wx.GBPosition(2, 3), wx.GBSpan(1, 1),
                     wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Txt_ConfigNameUninstall = wx.TextCtrl(self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                   wx.DefaultSize, wx.TE_READONLY)
        self.Txt_ConfigNameUninstall.SetMinSize(wx.Size(200, -1))

        gbSizer1.Add(self.Txt_ConfigNameUninstall, wx.GBPosition(2, 1), wx.GBSpan(1, 1),
                     wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.m_panel3.SetSizer(gbSizer1)
        self.m_panel3.Layout()
        gbSizer1.Fit(self.m_panel3)
        fgSizer2.Add(self.m_panel3, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL | wx.EXPAND, 5)

        self.m_panel1.SetSizer(fgSizer2)
        self.m_panel1.Layout()
        fgSizer2.Fit(self.m_panel1)
        fgSizer1.Add(self.m_panel1, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel2 = wx.Panel(self.Frame_PipeteUninstallment, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer3 = wx.FlexGridSizer(1, 1, 0, 0)
        fgSizer3.SetFlexibleDirection(wx.BOTH)
        fgSizer3.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        bSizer2 = wx.BoxSizer(wx.HORIZONTAL)

        self.Cmd_SetPipeteUninstall = wx.Button(self.m_panel2, wx.ID_ANY, u"Set", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer2.Add(self.Cmd_SetPipeteUninstall, 0, wx.ALL, 5)

        self.Cmd_EditPipeteUninstall = wx.Button(self.m_panel2, wx.ID_ANY, u"Edit", wx.DefaultPosition, wx.DefaultSize,
                                                 0)
        bSizer2.Add(self.Cmd_EditPipeteUninstall, 0, wx.ALL, 5)

        self.Cmd_DeletePipeteUninstall = wx.Button(self.m_panel2, wx.ID_ANY, u"Delete", wx.DefaultPosition,
                                                   wx.DefaultSize, 0)
        bSizer2.Add(self.Cmd_DeletePipeteUninstall, 0, wx.ALL, 5)

        self.Cmd_RefreshPipeteDataUninstall = wx.Button(self.m_panel2, wx.ID_ANY, u"Refresh", wx.DefaultPosition,
                                                        wx.DefaultSize, 0)
        bSizer2.Add(self.Cmd_RefreshPipeteDataUninstall, 0, wx.ALL, 5)

        fgSizer3.Add(bSizer2, 1, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL | wx.EXPAND, 5)

        self.m_panel2.SetSizer(fgSizer3)
        self.m_panel2.Layout()
        fgSizer3.Fit(self.m_panel2)
        fgSizer1.Add(self.m_panel2, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.m_panel4 = wx.Panel(self.Frame_PipeteUninstallment, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel4.SetBackgroundColour(wx.Colour(255, 255, 255))

        fgSizer4 = wx.FlexGridSizer(2, 1, 0, 0)
        fgSizer4.SetFlexibleDirection(wx.BOTH)
        fgSizer4.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText7 = wx.StaticText(self.m_panel4, wx.ID_ANY, u"Data Pipete", wx.DefaultPosition, wx.DefaultSize,
                                           0)
        self.m_staticText7.Wrap(-1)
        self.m_staticText7.SetFont(wx.Font(9, 74, 90, 92, False, "Arial"))

        fgSizer4.Add(self.m_staticText7, 0, wx.ALL, 5)

        self.Dv_PipeteDataUninstall = wx.dataview.DataViewListCtrl(self.m_panel4, wx.ID_ANY, wx.DefaultPosition,
                                                                   wx.DefaultSize, 0)
        self.Dv_PipeteDataUninstall.SetMinSize(wx.Size(500, 150))

        fgSizer4.Add(self.Dv_PipeteDataUninstall, 0, wx.ALL, 5)

        self.m_panel4.SetSizer(fgSizer4)
        self.m_panel4.Layout()
        fgSizer4.Fit(self.m_panel4)
        fgSizer1.Add(self.m_panel4, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel5 = wx.Panel(self.Frame_PipeteUninstallment, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        fgSizer5 = wx.FlexGridSizer(0, 2, 0, 0)
        fgSizer5.SetFlexibleDirection(wx.BOTH)
        fgSizer5.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        bSizer3 = wx.BoxSizer(wx.HORIZONTAL)

        self.Cmd_SavePipeteDataUninstall = wx.Button(self.m_panel5, wx.ID_ANY, u"OK", wx.DefaultPosition,
                                                     wx.DefaultSize, 0)
        bSizer3.Add(self.Cmd_SavePipeteDataUninstall, 0, wx.ALL, 5)

        fgSizer5.Add(bSizer3, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.m_panel5.SetSizer(fgSizer5)
        self.m_panel5.Layout()
        fgSizer5.Fit(self.m_panel5)
        fgSizer1.Add(self.m_panel5, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.Frame_PipeteUninstallment.SetSizer(fgSizer1)
        self.Frame_PipeteUninstallment.Layout()

        self.Frame_PipeteUninstallment.Centre(wx.BOTH)

        # Connect Events
        self.Cmb_ConfigurasiUninstall.Bind(wx.EVT_COMBOBOX, self.GetDetailedInfoConfigsUninstall)
        self.Cmd_SetPipeteUninstall.Bind(wx.EVT_BUTTON, self.SetPipeteUninstall)
        self.Cmd_EditPipeteUninstall.Bind(wx.EVT_BUTTON, self.EditPipeteUninstall)
        self.Cmd_DeletePipeteUninstall.Bind(wx.EVT_BUTTON, self.DeletePipeteUninstall)
        self.Cmd_RefreshPipeteDataUninstall.Bind(wx.EVT_BUTTON, self.RefreshPipeteUninstall)
        self.Cmd_SavePipeteDataUninstall.Bind(wx.EVT_BUTTON, self.SaveAllPipeteUninstall)

        self.StarterPipeteManageUninstall()
        self.Frame_PipeteUninstallment.Show()

    def __del__(self):
        pass

    #Virtual event handlers, overide them in your derived class
    # def GetDetailedInfoConfigsUninstall(self, event ):
    #     self.GetDeta

    # def SetPipeteUninstall( self, event ):
    #     self.SetPipe
    #     event.Skip()

    # def EditPipeteUninstall( self, event ):
    #     self.Edit
    #     event.Skip()

    # def DeletePipeteUninstall( self, event ):
    #     self.Dele
    #     event.Skip()

    # def RefreshPipeteUninstall( self, event ):
    #     self.Refres
    #     event.Skip()

    # def SaveAllPipeteUninstall( self, event ):
    #     self.SaveAL
    #     event.Skip()

    def StarterPipeteManageUninstall(self):
        self.ResetPipeteFormUninstall()
        self.DisablePipeteFormUninstall()
        self.SetColumnDataViewPipeteUninstall()
        self.RefreshPipeteUninstall()

    def DisablePipeteFormUninstall(self):
        self.Cmb_PipeteListUninstall.Disable()
        self.Cmb_ConfigurasiUninstall.Disable()
        self.Txt_ConfigIDUninstall.Disable()
        self.Txt_ConfigNameUninstall.Disable()
        # self.Cmd_SavePipeteDataUninstall.Disable()

    def EnablePipeteFormUninstall(self):
        self.Cmd_SetPipeteUninstall.Disable()
        self.Cmd_EditPipeteUninstall.Disable()
        self.Cmd_DeletePipeteUninstall.Disable()

        # Get All Config
        data_conf = self.GetAllDataConfigUninstall()
        self.Cmb_ConfigurasiUninstall.Clear()
        for row in data_conf:
            col = str(row[0])
            self.Cmb_ConfigurasiUninstall.AppendItems(col)

        data_pipete = self.GetAllDataPipeteListUninstall()
        self.LoadAllDataPipeteUninstall(data=data_pipete)

        self.Cmb_PipeteListUninstall.Enable()
        self.Cmb_ConfigurasiUninstall.Enable()
        self.Txt_ConfigIDUninstall.Enable()
        self.Txt_ConfigNameUninstall.Enable()
        # self.Cmd_SavePipeteDataUninstall.Enable()

        self.Cmd_SetPipeteUninstall.Enable()
        self.Cmd_EditPipeteUninstall.Enable()
        self.Cmd_DeletePipeteUninstall.Enable()




    def ReLoadDataPipeteUninstall(self):
        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_GetAllPipeteDataUninstall',
                                        parameter_list=[])
            
        if sql_result == False:
            #ToDo : MessageBox
            return

        self.LoadDataPipeteUninstall(data=data)


    def LoadDataPipeteUninstall(self, data):
        self.Dv_PipeteDataUninstall.DeleteAllItems()
        if data == None:
            return
        for row in data:
            if len(list(row)) == self.Dv_PipeteDataUninstall.GetColumnCount():
                self.Dv_PipeteDataUninstall.AppendItem(row)


    def ResetPipeteFormUninstall(self):
        self.Txt_ConfigIDUninstall.SetValue('')
        self.Txt_ConfigNameUninstall.SetValue('')
        self.Cmb_PipeteListUninstall.SetValue('')
        self.Cmb_ConfigurasiUninstall.SetValue('- Choose -')

    #ToDo : Listing Config ID and NAME
    # def LoadListConfigurasiUninstall(self):
    #     #ToDo : Make SP SELECT CONFIG ID AND CONFIG NAME
    #     pass

    def CompareConfigurasiUninstall(self, name, data):
        config_id = data[name]
        return(config_id)

    def ViewDetailConfigUninstall(self, name, data):
        config_id = self.CompareConfigurasiUninstall(name=name, data=data)
        self.Txt_ConfigNameUninstall.SetValue(str(name))
        self.Txt_ConfigIDUninstall.SetValue(str(config_id))

    def CheckSelectedRowPipeteUninstall(self):
        # Check Selected Row
        data_count = self.Dv_PipeteDataUninstall.GetItemCount()
        selecteds = False
        for item_num in range(data_count):
            if self.Dv_PipeteDataUninstall.IsRowSelected(row=item_num) == True:
                selecteds = True

        if selecteds == False:
            Msgbox = wx.MessageDialog(None,
                                      'No item was Selected, Please Choose(Click) one of the item listed on Datalist Table',
                                      'There is no item to edit', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

            klick = Msgbox.ShowModal()
            return(False, None)
        else:
            selected_row = self.Dv_PipeteDataUninstall.GetSelectedRow()
            return(True, selected_row)

    # Virtual event handlers, overide them in your derived class
    def SetPipeteUninstall(self, event=None):
        print('======================================')
        print(self.Cmd_SetPipeteUninstall.GetLabel())
        if self.Cmd_SetPipeteUninstall.GetLabel() == 'SAVE':
            #ToDo : QUERY SAVE
            print('SAVING')

            self.SavingNewPipeteUninstall()


            self.Cmd_SetPipeteUninstall.SetLabel('Set')
            self.Cmd_EditPipeteUninstall.SetLabel('Edit')
            self.Cmd_EditPipeteUninstall.Show()
            self.Cmd_DeletePipeteUninstall.Show()
            self.DisablePipeteFormUninstall()
            self.ResetPipeteFormUninstall()
            self.RefreshPipeteUninstall()
            return

        elif self.Cmd_SetPipeteUninstall.GetLabel() == 'UPDATE':
            #ToDo : QUERY UPDATE

            print ('UPDATING')
            self.UpdatingPipeteUninstall()

            self.Cmd_SetPipeteUninstall.SetLabel('Set')
            self.Cmd_EditPipeteUninstall.SetLabel('Edit')
            self.Cmd_EditPipeteUninstall.Show()
            self.Cmd_DeletePipeteUninstall.Show()
            self.DisablePipeteFormUninstall()
            self.ResetPipeteFormUninstall()
            self.RefreshPipeteUninstall()
            return

        

        #New Special Case Changes
        self.Cmd_SetPipeteUninstall.SetLabel('SAVE')
        self.Cmd_EditPipeteUninstall.SetLabel('CANCEL')    
        # self.Cmd_EditPipeteUninstall.Hide()
        self.Cmd_DeletePipeteUninstall.Hide()

        self.EnablePipeteFormUninstall()
        self.ResetPipeteFormUninstall()

    def SavingNewPipeteUninstall(self):
        pipete_name = self.Cmb_PipeteListUninstall.GetValue()
        config_used = self.Txt_ConfigIDUninstall.GetValue()

        # already_exist = self.CheckPipeteDataExistence()
        already_exist = False
        if already_exist:
            #ToDo : MessageBox
            return
        else:
            Msgbox = wx.MessageDialog(None,
                                      'Are You Sure Want To Save?',
                                      'Saving An Item', wx.OK | wx.CANCEL | wx.ICON_WARNING | wx.STAY_ON_TOP)

            klick = Msgbox.ShowModal()

            if klick == wx.ID_OK:
                #ToDo : Query Save Pipete
                sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_SaveNewPipeteUninstall',
                                            parameter_list=[str(pipete_name), int(config_used)])

                if sql_result == False:
                    Msgbox = wx.MessageDialog(None,
                                              'Failed To Save',
                                              'Save Failed!', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                else:
                    Msgbox = wx.MessageDialog(None,
                                              'Item Saved',
                                              'Save Success!', wx.OK | wx.ICON_INFORMATION | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()

    def UpdatingPipeteUninstall(self):
        updated_id = self.SelectedID_PipeteUninstall
        pipete_name = self.Cmb_PipeteListUninstall.GetValue()
        config_used = self.Txt_ConfigIDUninstall.GetValue()
        
        already_exist = self.CheckPipeteDataExistenceUninstall()

        if already_exist:
            #ToDo : MessageBox
            return
        else:
            Msgbox = wx.MessageDialog(None,
                                      'Are You Sure Want To Update?',
                                      'Updating An Item', wx.OK | wx.CANCEL | wx.ICON_WARNING | wx.STAY_ON_TOP)

            klick = Msgbox.ShowModal()

            if klick == wx.ID_OK:
                #ToDo : Query Save Pipete
                sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_UpdateDataPipeteUninstall',
                                            parameter_list=[int(updated_id), str(pipete_name), int(config_used)])

                if sql_result == False:
                    Msgbox = wx.MessageDialog(None,
                                              'Failed To Update',
                                              'Update Failed!', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                else:
                    Msgbox = wx.MessageDialog(None,
                                              'Item Updated',
                                              'Update Success!', wx.OK | wx.ICON_INFORMATION | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()


    def EditPipeteUninstall(self, event=None):
        if self.Cmd_EditPipeteUninstall.GetLabel() == 'CANCEL':
            
            self.SelectedID_PipeteUninstall = None

            self.Cmd_SetPipeteUninstall.SetLabel('Set')
            self.Cmd_EditPipeteUninstall.SetLabel('Edit')
            self.Cmd_EditPipeteUninstall.Show()
            self.Cmd_DeletePipeteUninstall.Show()
            self.DisablePipeteFormUninstall()
            self.ResetPipeteFormUninstall()
            self.RefreshPipeteUninstall()
            return

        #New Special Case Changes
        is_selected, row = self.CheckSelectedRowPipeteUninstall()
        if is_selected:
            self.Cmd_SetPipeteUninstall.SetLabel('UPDATE')
            self.Cmd_EditPipeteUninstall.SetLabel('CANCEL')
            # self.Cmd_EditPipeteUninstall.Hide()
            self.Cmd_DeletePipeteUninstall.Hide()

            self.EnablePipeteFormUninstall()
            self.ResetPipeteFormUninstall()

            # Apply data to Form
            self.ApplyDataToFormPipeteUninstall(selected_row=row)
            self.SelectedID_PipeteUninstall = self.Dv_PipeteDataUninstall.GetValue(col=0, row=row)

    def ApplyDataToFormPipeteUninstall(self, selected_row):
        # column_count = self.Dv_PipeteDataUninstall.GetColumnCount()
        # data_container = []
        # for col in range(column_count):
        #     col_data = self.Dv_PipeteDataUninstall.GetValue(col=col,row=selected_row)
        # self.RereshComboConfigPipete()
        self.Cmb_PipeteListUninstall.SetValue(str(self.Dv_PipeteDataUninstall.GetValue(col=1,row=selected_row)))
        self.Cmb_ConfigurasiUninstall.SetValue(str(self.Dv_PipeteDataUninstall.GetValue(col=2,row=selected_row)))
        self.Txt_ConfigNameUninstall.SetValue(str(self.Dv_PipeteDataUninstall.GetValue(col=2,row=selected_row)))
        id = self.GetMoreConfInfoUninstall(name=self.Dv_PipeteDataUninstall.GetValue(col=2,row=selected_row))
        self.Txt_ConfigIDUninstall.SetValue(str(id[0][0]))

    def RereshComboConfigPipeteUninstall(self):
        data = self.Get()
        self.Cmb_ConfigurasiUninstall.Clear()
        for items in data:
            self.Cmb_ConfigurasiUninstall.AppendItems(str(items))

    def DeletePipeteUninstall(self, event=None):
        is_selected, row = self.CheckSelectedRowPipeteUninstall()

        if is_selected == False:
            Msgbox = wx.MessageDialog(None, 'No item was Selected, Please Choose(Click) one of the item listed on Datalist Table',
                                      'There is no item to edit', wx.OK |  wx.ICON_ERROR | wx.STAY_ON_TOP)

            klick = Msgbox.ShowModal()
            return

        else:
            Msgbox = wx.MessageDialog(None,
                                      'Are You Sure Want To Delete?',
                                      'Deleting An Item', wx.OK | wx.CANCEL | wx.ICON_WARNING | wx.STAY_ON_TOP)

            klick = Msgbox.ShowModal()

            if klick == wx.ID_OK:
                pipete_id = self.Dv_PipeteDataUninstall.GetValue(row=row, col=0)

                #ToDo : Delete Query with PipeteID
                sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_DeleteDataPipeteUninstall',
                                                parameter_list=[pipete_id])

                if sql_result == False:
                    Msgbox = wx.MessageDialog(None,
                                              'Failed To Delete',
                                              'Delete Failed!', wx.OK | wx.ICON_ERROR | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()
                else:
                    self.RefreshPipeteUninstall()

                    Msgbox = wx.MessageDialog(None,
                                              'Item Was Deleted',
                                              'Delete Success!', wx.OK | wx.ICON_INFORMATION | wx.STAY_ON_TOP)

                    klick = Msgbox.ShowModal()



    def RefreshPipeteUninstall(self, event=None):
        self.ReLoadDataPipeteUninstall()

    def SaveAllPipeteUninstall(self, event=None):
        self.DestroyPipeteFormUninstall()

    def ExitPipeteManUninstall(self, event=None):
        self.DestroyPipeteFormUninstall()

    def DestroyPipeteFormUninstall(self):
        self.Frame_PipeteUninstallment.Destroy()

    def SetColumnDataViewPipeteUninstall(self):
        sql_result, data, error = self.GetRunningColumnPipeteUninstall()
        print(data)
        if sql_result in (None, False):
            # ERROR
            titles = 'Executing Query Error'
            texts = '[Error Query GetRunningColumnPipete] \n ' + str(error)
            answer = self.MessageBox(type=1, text=texts, title=titles)
            return


        # self.Data_DvPipete = {}
        col_num = 0
        for row in data:
            col = list(row)[0]
            # print (col)
            obj = self.Dv_PipeteDataUninstall.AppendTextColumn(str(col),width=len(col)*7+20)
            # self.Data_DvPipete.update({col_num:[col,obj]})
            col_num += 1

    def GetRunningColumnPipeteUninstall(self):
        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_GetPipeteColumnListUninstall',
                                        parameter_list=[])

        #
        print(data)
        
        if sql_result == False:
                #ToDo MessageBox
                pass
        return (sql_result, data, error)
        
    def CheckPipeteDataExistenceUninstall(self):
        #ToDo : Check Existence of Data in Database
        pass

    def GetAllDataConfigUninstall(self):
        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_GetAllExistedConfig',
                                        parameter_list=[])

        #
        print(data)
        
        if sql_result == False:
                #ToDo MessageBox
                pass
        return (data)

    def GetDetailedInfoConfigsUninstall(self,event=None):
        selected = str(self.Cmb_ConfigurasiUninstall.GetValue())

        if selected in ('','- Choose -'):
            #ToDo : Message Box
            return


        data = self.GetMoreConfInfoUninstall(name=selected)

        conf_id, conf_name = data[0]
        self.Txt_ConfigIDUninstall.SetValue(str(conf_id))
        self.Txt_ConfigNameUninstall.SetValue(str(conf_name))


    def GetMoreConfInfoUninstall(self, name):
        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_GetDetailConfigInfo',
                                              parameter_list=[str(name)])

        #
        print(data)

        if sql_result == False:
            # ToDo MessageBox
            pass
        return (data)

    def LoadAllDataPipeteUninstall(self,data):
        self.Cmb_PipeteListUninstall.Clear()
        if data != None:
            for row in data:
                col = str(row[0])
                self.Cmb_PipeteListUninstall.AppendItems(col)


    def GetAllDataPipeteListUninstall(self):
        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_GetAllPipeteExisted',
                                              parameter_list=[])

        #
        print(data)

        if sql_result == False:
            # ToDo MessageBox
            pass
        return (data)

    def GetDetailedInfoConfigsUninstall(self,event=None):
        selected = str(self.Cmb_ConfigurasiUninstall.GetValue())

        if selected in ('','- Choose -'):
            #ToDo : Message Box
            return


        data = self.GetMoreConfInfo(name=selected)

        conf_id, conf_name = data[0]
        self.Txt_ConfigIDUninstall.SetValue(str(conf_id))
        self.Txt_ConfigNameUninstall.SetValue(str(conf_name))
	


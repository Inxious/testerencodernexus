

import serial
import wx
import time,sys,os
import threading
import datetime
import ConfigParser

#APPEND CURR DIRECTORY TO SYSd
sys.path.append(os.getcwd())

import Controller2
import View
import Threads_Board1
import Threads_Board2
import Thread_Manual
import Frame_EncoderControl
import Frame_Rumus
import Frm_SpeedTable
import Frame_PersSetting

#Add 2
import Frame_GroupingKoordinat
import Frame_OffsetManagement
import Frame_PipeteManage
import Frame_PipeteUninstallment

from serialscan import serial_scan


class Controller(Thread_Manual.Th_Manuals,Threads_Board1.ThreadsB1,Threads_Board2.ThreadsB2,Frame_EncoderControl.MyFrame1,
                 Controller2.Controller2,Frm_SpeedTable.Frm_SpeedTable,Frame_PersSetting.FramesEncodePers,Frame_Rumus.Frame_RumSet,
                 View.View,Frame_GroupingKoordinat.Frame_GroupingKoordinat,Frame_OffsetManagement.Frame_OffsetManagement,Frame_PipeteManage.Frame_PipeteManagement,
                 Frame_PipeteUninstallment.Frame_PipeteManagementUninstall):

    def __init__(self):

        # 6 = X
        # 4 = Y
        # 1 = Z
        # 5 = C

        # # IMPORT SETTING
        # # LOAD DATABASE SETTING FROM CONFIG.txt
        config = ConfigParser.ConfigParser()
        config.readfp(open(r'config.txt'))

        pers_x = config.get('Persamaan Encoder', 'X')
        pers_y = config.get('Persamaan Encoder', 'Y')
        pers_z = config.get('Persamaan Encoder', 'Z')
        pers_c = config.get('Persamaan Encoder', 'C')

        self._PersamaanEncoder = {6:float(pers_x),4:float(pers_y),1:float(pers_z),5:float(pers_c)}

        #Threading
        T_B1 = threading.Thread(target=Threads_Board1.ThreadsB1.__init__, args=(self,),
                                name="Thread Board 1")
        T_B2 = threading.Thread(target=Threads_Board2.ThreadsB2.__init__, args=(self,),
                                name="Thread Board 2")
        T_Man = threading.Thread(target=Thread_Manual.Th_Manuals.__init__, args=(self,),
                                name="Thread Manual")

        T_B1.daemon = True
        T_B2.daemon = True
        T_Man.daemon = True

        T_B1.start()
        T_B2.start()
        T_Man.start()

        # # IMPORT SETTING
        # # LOAD DATABASE SETTING FROM CONFIG.txt
        # config = ConfigParser.ConfigParser()
        # config.readfp(open(r'config.txt'))

        #Frame
        Controller2.Controller2.__init__(self)
        View.View.__init__(self)
        Frame_EncoderControl.MyFrame1.__init__(self,None)
        # Frm_SpeedTable.Frm_SpeedTable.__init__(self,None)
        self.Offset_Encoder = {}
        x = config.get('Encoder Settings', 'Offset_X')
        y = config.get('Encoder Settings', 'Offset_Y')
        z = config.get('Encoder Settings', 'Offset_Z')
        c = config.get('Encoder Settings', 'Offset_C')
        self.Offset_Encoder.update({1:float(z),4:float(y),5:float(c),6:float(x)})

        # NEW RUMUS
        x_using = float(config.get('CalculateEncoder Data', 'X_USING'))
        y_using = float(config.get('CalculateEncoder Data', 'Y_USING'))
        z_using = float(config.get('CalculateEncoder Data', 'Z_USING'))
        c_using = float(config.get('CalculateEncoder Data', 'C_USING'))

        x_const = float(config.get('CalculateEncoder Data', 'X_Constant'))
        y_const = float(config.get('CalculateEncoder Data', 'Y_Constant'))
        z_const = float(config.get('CalculateEncoder Data', 'Z_Constant'))
        c_const = float(config.get('CalculateEncoder Data', 'C_Constant'))

        x_plus = float(config.get('CalculateEncoder Data', 'X_Plus'))
        y_plus = float(config.get('CalculateEncoder Data', 'Y_Plus'))
        z_plus = float(config.get('CalculateEncoder Data', 'Z_Plus'))
        c_plus = float(config.get('CalculateEncoder Data', 'C_Plus'))

        # self._RumData['C']['use']
        x_dat = {'use':x_using,
                 'constant':x_const,
                 'plus':x_plus}
        y_dat = {'use': y_using,
                 'constant': y_const,
                 'plus': y_plus}
        z_dat = {'use': z_using,
                 'constant': z_const,
                 'plus': z_plus}
        c_dat = {'use': c_using,
                 'constant': c_const,
                 'plus': c_plus}

        self._RumData = {'X':x_dat,
                         'Y':y_dat,
                         'Z':z_dat,
                         'C':c_dat}



        self.UnderGapIncrement = float(config.get('Encoder Settings', 'UnderGap_Increment'))
        self.UnderGapLimit = float(config.get('Encoder Settings', 'UnderGap_Limit'))
        self.AutoHome = bool(int(config.get('Encoder Settings', 'Auto_Homing')))
        self.EncoderOffset_Type = config.get('Encoder Settings', 'Offset_Type')
        self.EncoderRecovery = bool(int(config.get('Encoder Settings', 'Recovery_Mode')))
        self.EncoderRecovery_Loop = float(config.get('Encoder Settings', 'Recovery_Loop'))
        self.CStarter(0)




    def CStarter(self, mode):
        if mode != 2:
            self.XNow = 0
            self.YNow = 0
            self.ZNow = 0
            self.ANow = 0
            # self.BNow = 0
            self.MagNow = 0
            self.CNow = 0
            self.RNow = 0
        self.F1Now = str("")
        self.F2Now = str("")
        self.F3Now= str("")
        self.loop2 = ""
        self.MaxLimitTemp = 38
        self.MinLimitTemp = 36
        self.Camera_Connection = "OFF"
        self.ProcedureStart = False
        self.Rotataing = False
        self.SpeedChanged = False
        self.MotorIDBefore = 'None'
        self.SpeedBefore = 'None'
        self.AccelBefore = 'None'
        self.SerBefore = 'None'
        self.Proces_Selected = ''

        self.GUID_Proces = ''
        self.GUID_Config = ''
        self.GUID_Motor = ''
        self.ID_Proces = ''
        self.ID_Config = ''
        self.ID_ConfigDetail = ''
        self.ID_Motor = ''

        self.X_Speed = ""
        self.Y_Speed = ""
        self.Z_Speed = ""
        self.A_Speed = ""
        # self.B_Speed = ""
        self.C_Speed = ""
        self.R_Speed = ""

        self.X_Accel = ""
        self.Y_Accel = ""
        self.Z_Accel = ""
        self.A_Accel = ""
        # self.B_Accel = ""
        self.C_Accel = ""
        self.R_Accel = ""

        #Thread STARTER
        self.ManEvent_Status = "OFF"
        if mode == 0:
            self.vMotor_Connection = "OFF"
            self.vEncoder_Connection = "OFF"
        elif mode == 1:
            self.vMotor_Connection = "OFF"
        elif mode == 2:
            self.vEncoder_Connection = "OFF"


        self.Current_PipeteAttached = None

    def CPortScan(self):
        hasil = serial_scan()
        return (hasil)

    # ALSE CURRENT TIME BUT OBJECT
    def CCurTime3(self):
        return (datetime.datetime.now())

    def CSerialCon(self, board, com, brate, timeout):
        # Ard DUE (Motor)
        if board == 1:
            self.vMotor_Connection = "OFF"
            self.ser1 = serial.Serial()
            self.ser1.port = com
            self.ser1.baudrate = brate
            self.ser1.timeout = timeout
            if self.ser1.is_open == True:
                self.ser1.close()
            try:
                self.ser1.open()
            except Exception as e:
                print (e)
                return (False)
            print("GOOD")
            time.sleep(2)
            self.CStarter(1)
            self.vMotor_Connection = "ON"
            self.Ser1_Active = True
            self.vMotor_Reading = "ON"
            return (True)

        elif board == 2:
            self.vEncoder_Connection = "OFF"
            self.ser2 = serial.Serial()
            self.ser2.port = com
            self.ser2.baudrate = brate
            self.ser2.timeout = timeout
            if self.ser2.is_open == True:
                self.ser2.close()

            try:
                self.ser2.open()
            except Exception as e:
                print (e)
                return (False)

            print("GOOD")
            time.sleep(2)
            self.CStarter(2)
            self.vEncoder_Connection = "ON"
            self.Ser2_Active = True
            self.vEncoder_Reading = "ON"
            return (True)


    def CSerialDisconn(self, board):

        if board == 1:
            if self.ser1.is_open == True:
                self.vMotor_Reading = "OFF"
                self.vMotor_Connection = "OFF"
                self.ser1.close()
                self.Ser1_Active = False
                return (True)


        elif board == 2:
            if self.ser2.is_open == True:
                self.vEncoder_Reading = "OFF"
                self.vEncoder_Connection = "OFF"
                self.ser2.close()
                self.Ser2_Active = False
                return (True)

    def CSendCommand(self, board, data):
        if board == 1:

            try:
                self.ser1
            except Exception as e:

                print (e)
                print (" Serial Motor Belum Aktif ")
                self.TEndUpdateLog(1," Serial Motor Belum Aktif ")
                return

            if self.ser1.is_open:
                try:
                    self.ser1.write(data + '\n')
                    print ('WRITE >> ' + str(data))
                except Exception as e:
                    print (e)
                    print (" Serial Motor Belum Aktif ")
                    self.TEndUpdateLog(1, " Serial Motor Belum Aktif ")
            else:
                print (" Serial Motor Belum Aktif ")
                self.TEndUpdateLog(1, " Serial Motor Belum Aktif ")

        elif board == 2:

            try:
                self.ser2
            except Exception as e:
                print (e)
                print (" Serial Encoder Belum Aktif ")
                self.TEndUpdateLog(2, " Serial Encoder Belum Aktif ")
                return

            if self.ser2.is_open:
                try:
                    self.ser2.write(data + '\n')
                    print ('WRITE >> ' + str(data))
                except Exception as e:
                    print (e)
                    print (" Serial Encoder Belum Aktif ")
                    self.TEndUpdateLog(2, " Serial Encoder Belum Aktif ")
            else:
                print (" Serial Encoder Belum Aktif ")
                self.TEndUpdateLog(2, " Serial Encoder Belum Aktif ")

    # UI
    def SetSpeedos(self):
        Frm_SpeedTable.Frm_SpeedTable.__init__(self, None)

    # UI
    def SetPersdos(self):
        Frame_PersSetting.FramesEncodePers.__init__(self, None)

    #UI
    def SetRumdos(self):
        Frame_Rumus.Frame_RumSet.__init__(self, None)

    # UI Add 2
    def SetKoorGroupos(self, event):
        Frame_GroupingKoordinat.Frame_GroupingKoordinat.__init__(self, None)

    # UI Add 2
    def SetOffsetos(self, event):
        Frame_OffsetManagement.Frame_OffsetManagement.__init__(self, None)

    # UI Add 2
    def SetMapPipetos(self, event):
        Frame_PipeteManage.Frame_PipeteManagement.__init__(self, None)

    # UI Add 2
    def SetMapPipetosUninstall(self, event):
        Frame_PipeteUninstallment.Frame_PipeteManagementUninstall.__init__(self, None)


    def GetOffsetMotorGroup(self, koor_id, motor_used):
        if self.Current_PipeteAttached == None:
            return([[0]])
        else:
            #ToDo : Get Offset of Motor
            sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_GetSetOffsetKoordinate',
                                        parameter_list=[int(koor_id),str(motor_used),int(self.Current_PipeteAttached)])

            if sql_result == False:
                #ToDo : MessageBox
                pass
            
            print(data)
            return(data)
    def GetOKoordinateGroup(self, koor_id):
        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_GetKoordinatGroup',
                                              parameter_list=[int(koor_id)])

        if sql_result == False:
            # ToDo : MessageBox
            pass

        print(data)
        return (data)


    def CheckPipeteAttachment(self, config_id):

        sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_GetPipeteAttached',
                                              parameter_list=[int(config_id)])
        print(data)
        if sql_result == False:
            # ToDo : MessageBox
            pass
        else:
            if data not in (None,[]) and len(data) != 0:
                print('ATTACHED PIPETE')
                pipete_name = data[0][1]
                final = "PIPETE INFO | ATTACH | " + str(pipete_name)
                self.TEndUpdateLog(2, final)
                self.Current_PipeteAttached = data[0][0]
                self.SetHeadString(name='PIPETE USED = ' + str(pipete_name))

        return (data)

    def CheckPipeteDeattachment(self, config_id):
        if self.Current_PipeteAttached == None:
            pass
        else:
            sql_result, data, error = self.SQL_SP(sp_name='SP_Nxs_GetPipeteAttachedEnd',
                                                 parameter_list=[int(config_id)])
            if sql_result == False:
                # ToDo : MessageBox
                pass
            else:
                if data not in (None, []) and len(data) != 0:
                    print('DETACHED PIPETE')
                    pipete_name = data[0][1]
                    final = "PIPETE INFO | DETACH | " + str(pipete_name)
                    self.TEndUpdateLog(2, final)
                    self.Current_PipeteAttached = None
                    self.SetHeadString(name='PIPETE USED = NONE')

            return (data)


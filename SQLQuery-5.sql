-- PIPETE MANAGE UNINSTALL

-- SAVE NEW PIPETE
CREATE PROCEDURE SP_Nxs_SaveNewPipeteUninstall
@Name AS VARCHAR(250),
@Config_ID AS INTEGER
AS
BEGIN
	
	UPDATE M_Pipete_Mapping
	SET Pipete_UninstallConfig = @Config_ID
	WHERE Pipete_Name = @Name

END

EXEC SP_Nxs_GetAllPipeteDataUninstall

-- UPDATE DATA PIPETE
CREATE PROCEDURE SP_Nxs_UpdateDataPipeteUninstall
@PipeteID AS INTEGER,
@Name AS VARCHAR(250),
@Config_ID AS INTEGER
AS
BEGIN
	
	UPDATE M_Pipete_Mapping
	SET Pipete_UninstallConfig = @Config_ID
	WHERE Pipete_Name = @Name AND Pipete_ID = @PipeteID

END

-- DELETE DATA PIPETE
CREATE PROCEDURE SP_Nxs_DeleteDataPipeteUninstall
@PipeteID AS INTEGER
AS 
BEGIN

	UPDATE M_Pipete_Mapping
	SET Pipete_UninstallConfig = NULL
	WHERE Pipete_ID = @PipeteID

END


-- GET ALL DATA PIPETE
CREATE 
--ALTER
PROCEDURE SP_Nxs_GetAllPipeteDataUninstall
AS
BEGIN 

	SELECT p.Pipete_ID, p.Pipete_Name, c.MC_Action_Name --Pipete_ConfigSet
	FROM M_Pipete_Mapping AS p
	INNER JOIN M_ConfigurasiHeader AS c ON p.Pipete_UninstallConfig = c.MC_HeaderID 
	WHERE p.Pipete_UninstallConfig IS NOT NULL  

END

select * from M_Pipete_Mapping
select * from M_ConfigurasiHeader

 -- SP_Nxs_GetCoorColumnList
CREATE 
--ALTER
PROCEDURE SP_Nxs_GetPipeteColumnListUninstall
AS
BEGIN
	DECLARE @Tab TABLE ([COLUMNS_ALIAS] Varchar(MAX), [COLUMNS] Varchar(MAX))
	INSERT INTO @Tab
	VALUES
		('Pipete ID','p.Pipete_ID'),
		('Pipete Name','p.Pipete_Name'),
		('Pipete Uninstall Config','c.MC_Action_Name ')
		
	SELECT * FROM @Tab
END


-- GET ALL CONFIG EXIST
CREATE PROCEDURE SP_Nxs_GetAllExistedConfig
AS
BEGIN

	SELECT MC_Action_Name FROM M_ConfigurasiHeader
	WHERE IsActive = 1
	ORDER BY MC_Action_Name ASC

END

-- GET ALL CONFIG PIPETE
CREATE PROCEDURE SP_Nxs_GetAllExistedConfig
AS
BEGIN

	SELECT MC_Action_Name FROM M_ConfigurasiHeader
	WHERE IsActive = 1
	ORDER BY MC_Action_Name ASC

END



-- GET DETAILED INFO OF CONFIG
CREATE PROCEDURE SP_Nxs_GetDetailConfigInfo
@Config_Name AS VARCHAR(MAX)
AS
BEGIN
	
	SELECT MC_HeaderID, MC_Action_Name
	FROM M_ConfigurasiHeader
	WHERE MC_Action_Name = @Config_Name
END

EXEC  SP_Nxs_GetPipeteColumnList;


-- GET ALL DATA PIPETE UNINSTALL
--CREATE PROCEDURE SP_Nxs_GetAllPipeteExistedUninstall
--AS
--BEGIN

--	SELECT Pipete_Name
--	FROM M_Pipete_Mapping
--	WHERE Pipe
--	ORDER BY Pipete_Name ASC

--END

SELECT * FROM M_Pipete_Mapping
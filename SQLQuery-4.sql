-- USAGES


--SELECT * FROM M_Motor
--ALTER TABLE M_Koordinat ADD Koor_Group CHAR(50) NULL;

-- CHECK OFFSET MOTOR 
CREATE PROCEDURE SP_Nxs_GetSetOffsetKoordinate
@Koor_ID  AS INTEGER,
@Motor AS VARCHAR(250),
@Pipete_ID AS INTEGER
AS
BEGIN 
	DECLARE @Koor_Group AS INTEGER
	SET @Koor_Group = (SELECT Koor_Group FROM M_Koordinat WHERE KoordinatID = @Koor_ID)

	DECLARE @HeadID AS INTEGER
	SET @HeadID = (SELECT Ofmap_HeaderID FROM M_OffsetMappingHeader
					WHERE Pipete_ID = @Pipete_ID AND Koor_Group = @Koor_Group)
	
	SELECT Offset_Value
	FROM M_OffsetMappingDetail
	WHERE Ofmap_HeaderID = @HeadID AND Motor_Used = @Motor
	
	
END

-- GET CURRENT KOORDINAT GROUP
CREATE PROCEDURE SP_Nxs_GetKoordinatGroup
@Coor_ID AS INTEGER
AS
BEGIN

	SELECT g.Group_Name
	FROM M_Koordinat AS k
	RIGHT JOIN M_KoordinatGroup AS g ON k.Koor_Group = g.GroupID
	WHERE k.KoordinatID = @Coor_ID

END


-- GET CURRENT PIPETE ATTACHMENT
CREATE 
--ALTER
PROCEDURE SP_Nxs_GetPipeteAttached
@Config_ID AS INTEGER
AS
BEGIN

	SELECT Pipete_ID, Pipete_Name
	FROM M_Pipete_Mapping
	WHERE Pipete_ConfigSet = @Config_ID

END

-- GET CURRENT PIPETE DEATTACHMENT
CREATE 
--ALTER
PROCEDURE SP_Nxs_GetPipeteAttachedEnd
@Config_ID AS INTEGER
AS
BEGIN

	SELECT Pipete_ID, Pipete_Name
	FROM M_Pipete_Mapping
	WHERE Pipete_UninstallConfig = @Config_ID

END

SELECT * FROM M_Motor
	SELECT * FROM M_Koordinat
	SELECT * FROM M_OffsetMappingHeader
	SELECT * FROM M_OffsetMappingDetail
SELECT * FROM M_Koordinat
SELECT * FROM M_Pipete_Mapping